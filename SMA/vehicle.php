<?php
include_once 'session_auth.php';

$_page = 'vtl';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="images/favicon.png">
<title>My Limo Project | Vehicle Type Setup</title>

<!--Core CSS -->
<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-reset.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

<!--dynamic table-->
<link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />

<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />
<link href="css/bootstrap-switch.css" rel="stylesheet">
<style type="text/css">
.bootstrap-switch-wrapper {
	width: 70px !important;
}
/*.bootstrap-switch-container .bootstrap-switch-handle-on
{
	
		width:30px !important;
	
	}*/
	
.bootstrap-switch.bootstrap-switch-large .bootstrap-switch-handle-on, .bootstrap-switch.bootstrap-switch-large .bootstrap-switch-handle-off, .bootstrap-switch.bootstrap-switch-large .bootstrap-switch-label {
	padding: 6px 2px;
	font-size: 18px;
	line-height: 1.3333333;
}
</style>
</head>

<body>
<section id="container" >

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
  <section id="main-content">
    <section class="wrapper"> 
      <!-- page start-->
      
      <div class="row">
        <div class="col-sm-12">
          <section class="panel">
            <header class="panel-heading"> Vehicle Type </header>
            
            <!--========================================================= --> 
            <!-- switch start here --> 
            <!--<b>OverRide Limoanywhere vehicle</b>
            <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-id-switch-size bootstrap-switch-large bootstrap-switch-animate bootstrap-switch-off">
              <input type="radio" name="radio2"   data-radio-all-off="true" class="switch-radio2" data-size="small" >
            </div>--> 
            
            <!-- switch end here --> 
            <!--========================================================= -->
            
            <div id="pad-wrapper">
              <div class="row">
                <div class="col-sm-12">
                  <h4 class="page-title"></h4>
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Code</th>
                        <th> <span class="line"></span> Capacity</th>
                        <th><span class="line"></span>Name</th>
                        <th><span class="line"></span>Luggage</th>
                        <th><span class="line"></span></th>
                      </tr>
                    </thead>
                    <tbody class="sortable-table ui-sortable" id="setCarDetail">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
      
      <!-- page end--> 
    </section>
  </section>
  <!--main content end--> 
  <!--right sidebar start--><!--right sidebar end--> 
  
</section>


<!--Loading indicator-->
<div style="position: fixed; top: 0px; z-index: 10000; text-align: center; width: 100%; height: 100%; display: none;" id="refresh_overlay">
  <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%; display:table-row">
        <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px" >
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto"><br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <div class="loadingGIF"> <img style="height:70px;width:70px;" src="images/loading.gif" alt="Page loading indicator"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Loading Indicator Ends--> 
<!-- Placed js at the end of the document so the pages load faster --> 

<!--Core js--> 

<script src="js/lib/jquery.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/logout.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 

<!--Easy Pie Chart--> 
<script src="assets/easypiechart/jquery.easypiechart.js"></script> 
<!--Sparkline Chart--> 

<!--jQuery Flot Chart--> 


<!--dynamic table--> 
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script> 
<!--common script init for all pages--> 
<script src="js/scripts.js"></script> 
<script src="js/bootstrap-switch.js"></script> 
<script src="pageJs/vehicle_checked.js"></script> 
<script src="pageJs/vehicle.js"></script> 
<script>
 $('.submit_popup').on("click",function(){
			    $('#signup_form_overlay').show();
			  });
 $('#popup_close_vrc,#cancel_trans').on("click",function(){
		  $('#signup_form_overlay').hide();
		  });

$("input[type=\"checkbox\"], input[type=\"radio\"]").not("[data-switch-no-init]").bootstrapSwitch();


  </script>
</body>
</html>
