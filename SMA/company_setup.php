<?php
include_once 'session_auth.php';

$_page = 'cpd';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Company Profile Setup</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="assets/jvector-map/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="css/clndr.css" rel="stylesheet">
    <!--clock css-->
    <link href="assets/css3clock/css/style.css" rel="stylesheet">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="assets/morris-chart/morris.css">
    <link rel="stylesheet" href="build/css/intlTelInput.css">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
        .form-control {
            border: 1px solid #e2e2e4;
            box-shadow: none;
            color: #847F7F;
        }

        #setting_mode .notActive {
            color: #074759;
            background-color: #fff;
        }

        #setting_mode .active {
            color: #fff;
            background-color: #074759 !important;
        }

        #map_mode .notActive {
            color: #074759;
            background-color: #fff;
        }

        #map_mode .active {
            color: #fff;
            background-color: #074759 !important;
        }
    </style>
</head>
<body>

<!--header start-->
<?php include_once './global/header.php'; ?>
<!--header end-->

<!--sidebar start-->
<?php include_once './global/sideNav.php'; ?>
<!-- sidebar menu end-->

<section id="main-content">
    <section class="wrapper">
        <div class="panel-body">
            <form id="companySetupForm" class="form-horizontal bucket-form" method="get">
                <br>
                <h3>User Profile</h3>
                <div class="form-group">
                    <label class="col-sm-3 control-label">User Auth Key</label>
                    <div class="col-sm-6">
                        <label class="col-sm-3 control-label" id="userAuthKey"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">NAME</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="userId">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">EMAIL</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="emailId" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">PHONE NUMBER</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="mbNumber" name="mbNumber"
                               onkeyup="validPhoneNumber(this)">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">COMPANY NAME</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control " id="companyName">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">PASSWORD</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control " id="password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Limoanywhere</label>
                    <div class="col-sm-8 col-md-8 col-xs-7">
                        <div class="input-group">
                            <div id="setting_mode" class="btn-group">
                                <a class="btn btn-primary btn-sm active" data-toggle="on_off" data-title="on">ON</a>
                                <a class="btn btn-primary btn-sm notActive" data-toggle="on_off"
                                   data-title="off">OFF</a>
                            </div>
                            <input type="hidden" name="on_off" id="on_off" value="on">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Show Routing Map</label>
                    <div class="col-sm-8 col-md-8 col-xs-7">
                        <div class="input-group">
                            <div id="map_mode" class="btn-group">
                                <a class="btn btn-primary btn-sm active" data-toggle="yes_no" data-title="1">Yes</a>
                                <a class="btn btn-primary btn-sm notActive" data-toggle="yes_no" data-title="0">No</a>
                            </div>
                            <input type="hidden" name="yes_no" id="yes_no" value="1">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Pagination Mode</label>
                    <div class="col-sm-6">
                        <select name="paginate_mode" class="form-control" id="paginate_mode">
                            <option value="classical">Classical</option>
                            <option value="load_more">Load more</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Pagination Column</label>
                    <div class="col-sm-6">
                        <select name="paginate_column" class="form-control" id="paginate_column">
                            <option value="4">4 column</option>
                            <option value="2">2 column</option>
                            <option value="1">1 column</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Paginate data per page</label>
                    <div class="col-sm-6">
                        <input type="number" min="1" class="form-control " id="paginate_per_page" name="paginate_per_page">
                    </div>
                </div>
                <input type="button" class="btn btn-primary" id="updateUserProfile" value="UPDATE PROFILE"
                       style=" margin-left: 37%; width: 23%;">
            </form>
        </div>
    </section>
</section>

<!--Loading indicator-->
<div style="position: fixed; top: 0px; z-index: 10000; text-align: center; width: 100%; height: 100%; display: none;"
     id="refresh_overlay">
    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%; display:table-row">
                <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto"><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="loadingGIF"><img style="height:70px;width:70px;" src="images/loading.gif"
                                                         alt="Page loading indicator"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
<script src="js/lib/jquery.js"></script>
<script src="pageJs/dashboard.js"></script>
<script src="pageJs/logout.js"></script>
<!-- <script src="assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>  -->
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>
<!--[if lte IE 8]>
<script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="assets/skycons/skycons.js"></script>
<script src="assets/jquery.scrollTo/jquery.scrollTo.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="assets/calendar/clndr.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="assets/calendar/moment-2.2.1.js"></script>
<script src="js/calendar/evnt.calendar.init.js"></script>
<script src="assets/jvector-map/jquery-jvectormap-1.2.2.min.js"></script>
<script src="assets/jvector-map/jquery-jvectormap-us-lcc-en.js"></script>
<script src="assets/gauge/gauge.js"></script>
<!--clock init-->
<script src="assets/css3clock/js/script.js"></script>
<!--Easy Pie Chart-->
<script src="assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="assets/sparkline/jquery.sparkline.js"></script>
<!--Morris Chart-->
<script src="assets/morris-chart/morris.js"></script>
<script src="assets/morris-chart/raphael-min.js"></script>
<!--jQuery Flot Chart-->
<script src="assets/flot-chart/jquery.flot.js"></script>
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="assets/flot-chart/jquery.flot.resize.js"></script>
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="assets/flot-chart/jquery.flot.animator.min.js"></script>
<script src="assets/flot-chart/jquery.flot.growraf.js"></script>
<script src="js/custom-select/jquery.customSelect.min.js"></script>
<!--common script init for all pages-->
<script src="build/js/intlTelInput.js"></script>
<script src="js/scripts.js"></script>
<script src="pageJs/UserProfile.js"></script>
<script>
    intlTelInputValid = 1;

    function validPhoneNumber(trigger) {
        var trigger = $(trigger);
        trigger.closest('.col-sm-6').find('.phoneValidation').remove();
        var isValidNumber = trigger.intlTelInput("isValidNumber");
        if (!isValidNumber) {
            trigger.closest('.intl-tel-input').after('<div class="phoneValidation"><span style="font-size: 12px;color: #ff4268;">Invalid Number. Please insert valid one.</span></div>');
            intlTelInputValid = 0;
        } else {
            intlTelInputValid = 1;
            var countrySet = trigger.intlTelInput("getSelectedCountryData");
            trigger.intlTelInput("destroy");
            trigger.intlTelInput({
                utilsScript: 'build/js//utils.js',
                autoPlaceholder: true,
                initialCountry: countrySet.iso2,
                preferredCountries: ['us', 'fr', 'gb']
            });
        }

    }

</script>
<script>
    $('#setting_mode a').on('click', function () {
        var setting_sel = $(this).data('title');
        var setting_tog = $(this).data('toggle');

        $('#' + setting_tog).prop('value', setting_sel);

        $('a[data-toggle="' + setting_tog + '"]').not('[data-title="' + setting_sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + setting_tog + '"][data-title="' + setting_sel + '"]').removeClass('notActive').addClass('active');
    });
    $('#map_mode a').on('click', function () {
        var setting_sel = $(this).data('title');
        var setting_tog = $(this).data('toggle');

        $('#' + setting_tog).prop('value', setting_sel);

        $('a[data-toggle="' + setting_tog + '"]').not('[data-title="' + setting_sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + setting_tog + '"][data-title="' + setting_sel + '"]').removeClass('notActive').addClass('active');
    });
</script>
</body>
</html>