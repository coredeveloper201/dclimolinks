<?php
include_once 'session_auth.php';

$_page = 'csds';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="images/favicon.png">
<title>My Limo Project | Edit Airport Database</title>
<!--Core CSS -->
<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet">
<style type="text/css">
input[type=file] {
	left: 89%;
	min-height: 33px;
	opacity: 0;
	position: absolute;
	text-align: right;
	top: 0;
	width: 12%;
}
.close_popup {
	width: 16px;
	margin-left: 97%;
	margin-top: -2%;
	cursor: pointer;
	height: 16px;
}
.btn-default {
	color: #fff;
	background-color: #1FB5AD;
	border-color: #1FB5AD;
}
.btn-default:hover {
	color: #fff;
	background-color: #2FA9A2;
	border-color: #2FA9A2;
}
.btn-primary {
	color: #fff;
	background-color: #1FB5AD;
	border-color: #1FB5AD;
}
.btn-primary:hover {
	color: #fff;
	background-color: #2FA9A2;
	border-color: #2FA9A2;
}
.multiselect-container li {
	text-align: left;
}
div.box {
	margin-top: 1%;
	padding-top: 2%;
	border: 1px solid #C1C1C1;
	background-color: #E5E5FF;
}
.fntweight {
	font-weight: 700;
	text-align: left;
}
.table-action {
	text-align: right;
	font-size: 14px
}
.loadingGIF {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	width: 100%;
	padding-left: 30%;
	padding-top: 10%;
}
.alignCENTER {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}
th {
	text-align: center;
}
.cursor_pointer{
    cursor: pointer;
}
</style>
</head>
<body>
<section id="container" >

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
  <section id="main-content">
    <section class="wrapper"> 
      <!-- page start-->
      
      <div class="row">
        <div class="col-sm-12">
          <section class="panel"> 
            
            <!-- Add popup code start here -->
            
            <div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display:none;" id="CruisePopupStart">
              <div style="position:relative; background:black; opacity:0.5; top: 0px; left:0px; width:100%; height:100%"> </div>
              <div style="position:absolute; top: 0px; left:-17%; width:136%; height:100%; margin-left:auto; margin-right:auto;">
                <div style="width:100%;display:table; height:100%;">
                  <div style="width:100%;display:table-row">
                    <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                      <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; " >
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;"> <br>
                          <br>
                          <br>
                          <br>
                          <br>
                          <br>
                          <div style="width:80%"> 
                            <!--  code of popup end-->
                            
                            <div class="container">
                              <form  id="cruiseform">
                                <div id="modal" class="popupContainer" style="display:block;  top: 23px; position: absolute; width: 30%; left: 33%; top: 30px;
  background: #FFF; border-radius:4px; box-shadow:0 10px 15px rgba(0, 0, 0, 0.61);">
                                  <header class="popupHeader" style="  background: #F4F4F2; position: relative;padding: 10px 20px 0px 20px;border-bottom: 1px solid #DDD;font-weight: bold;font-family: 'Source Sans Pro', sans-serif;font-size: 14px;color: #666;font-size: 16px;text-transform: capitalize; text-align: center;height:9%; margin-top:4px;"> <span class="header_title">Add Cruise Ship</span> <img src="images/remove.png" alt="" id="close_rate_postal_popup" class="close_popup" style="margin-left: 97%; margin-top: -9%; width:16px; height:16px; cursor:pointer;"> <span id="errmsg_add_airport_rate" style="color:red;"></span> </header>
                                  <section class="popupBody" style="height: 246px;">
                                    <div class="form-group">
                                      <div class="col-xs-12 show_postal"  style="margin-bottom: 8%; font-size: 14px;">
                                        <div style="height:auto; padding: 0 20px;">
                                          <div class="row" style="margin-bottom:2%;">
                                            <div class="col-xs-6 col-sm-6 col-md-6">
                                              <p class="fntweight">Cruise Liner Name</p>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 text-center">
                                              <input type="text"  name="name" style="margin-left: -12%">
                                            </div>
                                          </div>
                                          <div class="row" style="margin-bottom:2%;">
                                            <div class="col-xs-6 col-sm-6 col-md-6">
                                              <p class="fntweight">Cruise Ship Name</p>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 text-center">
                                              <input type="text"  name="cruise_code" style="margin-left: -12%">
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                  <section class="popup_footer  text-center">
                                    <input class="btn btn-primary" id="selected_vehicle_rat_btn" style="width:25% !important;margin: 0px 0px 25px;"  type="submit" value="Update">
                                  </section>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <!--  Add popup code end here --> 
            
            <!-- popup code start here -->
            
            <div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display:none;" id="showAirportData">
              <div style="position:relative; background:black; opacity:0.5; top: 0px; left:0px; width:100%; height:100%"> </div>
              <div style="position:absolute; top: 0px; left:-17%; width:136%; height:100%; margin-left:auto; margin-right:auto;">
                <div style="width:100%;display:table; height:100%;">
                  <div style="width:100%;display:table-row">
                    <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                      <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; " >
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;"> <br>
                          <br>
                          <br>
                          <br>
                          <br>
                          <br>
                          <div style="width:80%"> 
                            <!--  code of popup end-->
                            
                            <div class="container">
                              <form  id="cruiseInformation">
                                <div id="modal" class="popupContainer" style="display:block;  top: 23px; position: absolute; width: 30%; left: 33%; top: 30px;
  background: #FFF; border-radius:4px; box-shadow:0 10px 15px rgba(0, 0, 0, 0.61);">
                                  <header class="popupHeader" style="  background: #F4F4F2; position: relative;padding: 10px 20px 0px 20px;border-bottom: 1px solid #DDD;font-weight: bold;font-family: 'Source Sans Pro', sans-serif;font-size: 14px;color: #666;font-size: 16px;text-transform: capitalize; text-align: center;height:9%; margin-top:4px;"> <span class="header_title">Edit Cruise Ship </span> <img src="images/remove.png" alt="" id="close_rate_postal_popup" class="close_popup" style="margin-left: 97%; margin-top: -9%; width:16px; height:16px; cursor:pointer;"> <span id="errmsg_add_airport_rate" style="color:red;"></span> </header>
                                  <section class="popupBody" style="height: 246px;">
                                    <div class="form-group">
                                      <div class="col-xs-12 show_postal"  style="margin-bottom: 8%; font-size: 14px;">
                                        <div style="height:auto; padding: 0 20px;">
                                          <div class="row" style="margin-bottom:2%;">
                                            <div class="col-xs-6 col-sm-6 col-md-6">
                                              <p class="fntweight">Cruise Liner Name</p>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 text-center">
                                              <input type="text" id="edit_cruise_name" name="edit_cruise_name" style="margin-left: -12%">
                                            </div>
                                          </div>
                                          <div class="row" style="margin-bottom:2%;">
                                            <div class="col-xs-6 col-sm-6 col-md-6">
                                              <p class="fntweight">Cruise Ship Name</p>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 text-center">
                                              <input type="text" id="edit_cruise_code" name="edit_cruise_code" style="margin-left: -12%">
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                  <section class="popup_footer  text-center">
                                    <input class="btn btn-primary" id="selected_vehicle_rat_btn" style="width:25% !important;margin: 0px 0px 25px;"  type="submit" value="Update">
                                  </section>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <!--  popup code end here -->
            
            <div id="pad-wrapper">
              <div class="row">
                <div class="col-sm-12 alignCENTER">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box" style="padding-top:20px; padding-bottom: 40px; margin-bottom:20px; margin-top: -10px;">
                  <h4 class="page-title" style="margin-bottom:30px;">Cruise Ship Database Management</h4>
                  <button class="btn btn-primary" id="addCruise">Add Cruise Ship</button>

                  <button class="btn btn-primary" id="csvFileId">Import CSV File</button>
                  <form id="getformdata" >
                 <!-- <i class="glyphicon glyphicon-folder-open"></i> <span class="hidden-xs1" id="csvFileId">Upload CSV File</span> -->
                    <input id="importCruise" name="importCruise" type="file" multiple="" class="">
                  
                </form>

                  <!-- <button class="btn btn-primary" id="showAddForm"></button> -->
                  </div>
                  <table class="table table-condensed ng-scope sieve">
                    <thead>
                      <tr>
                        <th class="cursor_pointer" onclick="sortThisTable('id')">ID#</th>
                        <th class="cursor_pointer" onclick="sortThisTable('name')"><span class="line">Cruise Liner Family Name</span></th>
                        <th class="cursor_pointer" onclick="sortThisTable('cruise_code')"><span class="line">Cruise Ship Name</span></th>
                        <th><span class="line">Action</span></th>
                      </tr>
                    </thead>
                    <tbody class="sortable-table ui-sortable" id="view_airport_information_db_table">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
      
      <!-- page end--> 
    </section>
  </section>
  <!--main content end--></section>
<!--Loading indicator-->

<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;" id="refresh_overlay">
  <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%;display:table-row">
        <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; " >
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;"> <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <div class="loadingGIF"> <img style="height:70px;width:70px;" src="images/loading.gif" alt="Page loading indicator"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Loading Indicator Ends--> 

<!--Core js--> 

<script src="js/lib/jquery.js"></script> 
<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/logout.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 

<!--Easy Pie Chart--> 
<script src="assets/easypiechart/jquery.easypiechart.js"></script> 
<!--Sparkline Chart--> 

<!--dynamic table--> 
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script> 
<!--common script init for all pages--> 
<!-- code start here for search box--> 
<script src="pageJs/searchbox.js"></script> 
<!-- code end here for search box--> 

<script src="js/scripts.js"></script> 
<script src="pageJs/cruise.js"></script> 
<script src="js/dynamic_table/dynamic_table_init.js"></script> 
<script src="js/bootstrap-multiselect.js"></script> 
<script type="text/javascript">
    $("table.sieve").sieve();
   
</script>
</body>
</html>
