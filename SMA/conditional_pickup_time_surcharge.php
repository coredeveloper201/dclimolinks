<?php
include_once 'session_auth.php';

$_page = 'cpts';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Conditional Early Morning/After Hours pickup Surcharge Setup</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>

    <!--dynamic table-->
    <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet"/>
    <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css"/>

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/limo_car.css" rel="stylesheet">
    <link href="css/bootstrap-multiselect.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" media="screen" href="css/timepicker/bootstrap-datetimepicker.min.css"/>

    <script src="js/lib/jquery.js"></script>
    <script src="pageJs/validation.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="JQ_timepicker/jquery_addon_css.css">
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
        ul.vehicle-list {
            padding: 0;
            margin: 0;
            list-style: none;
            margin-top: 15px
        }

        ul.vehicle-list li a {
            display: block;
            border-bottom: 1px dotted #CFCFCF;
            padding: 10px;
            text-decoration: none
        }

        ul.vehicle-list li:first-child a {
            border-top: 1px dotted #CFCFCF
        }

        ul.vehicle-list li a:hover {
            background-color: #DBF9FF
        }

        ul.vehicle-list li span {
            display: block
        }

        ul.vehicle-list li span.vehicle-name {
            font-weight: 600;
            color: #8F8F8F
        }

        ul.vehicle-list li span.vehicle-tier-count {
            color: #8C8C8C
        }

        .top-links a {
            font-size: 12px
        }

        .top-links {
            text-align: right
        }

        div.rate-group-selector {
            display: inline-block
        }

        div.vehicle-list .panel-default > .panel-heading {
            background-color: transparent;
            padding: 15px
        }

        div.vehicle-list .panel-default {
            border-color: transparent
        }

        div.vehicle-list .panel {
            border: none;
            box-shadow: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            -o-box-shadow-none: none
        }

        #vehicle-list h4.panel-title {
            font-style: normal;
            font-size: 14px
        }

        div.vehicle-list .panel-group .panel + .panel {
            margin-top: 0px
        }

        .input-xs {
            height: 22px;
            padding: 5px 5px;
            font-size: 12px;
            line-height: 1.5;
            border-radius: 3px
        }

        div.vehicle-list .table thead th {
            padding-bottom: 5px !important;
            text-transform: none;
            color: #898989;
            font-weight: normal
        }

        div.vehicle-list .table > tbody > tr > td, div.vehicle-list .table thead {
            border-top: none
        }

        div.vehicle-list .table-condensed > tbody > tr > td {
            padding: 2px
        }

        .table-action {
            text-align: right;
            font-size: 14px
        }

        .editable-text {
            width: 30%
        }

        .set { /* position:relative; *//* width:100%; *//* height:auto; *//* background-color:#f5f5f5 */
        }

        .set > a {
            display: block;
            padding: 20px 15px;
            text-decoration: none;
            color: #555;
            font-weight: 600;
            border-bottom: 1px solid #ddd;
            -webkit-transition: all 0.2s linear;
            -moz-transition: all 0.2s linear;
            transition: all 0.2s linear
        }

        .set > a:hover {
            background-color: cornsilk
        }

        .set > a i {
            position: relative;
            float: right;
            margin-top: 4px;
            color: #666
        }

        .set > a.active {
            background-color: #1fb5ad;
            color: #fff
        }

        .set > a.active i {
            color: #fff
        }

        .acc-content {
            position: relative;
            width: 100%;
            height: auto;
            background-color: #fff;
        }

        .form-control {
            color: black;
        }

        .serviceType_css {
            margin-top: 8%;
        }

        .serviceRate_css {
            margin-top: 3%;
        }

        #add_point_rate_to option {
            max-height: 10px !important;
        }

        select[multiple] {
            height: 35px;
        }

        .form-control1 {
            color: black;
        }

        .loadingGIF {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width: 100%;
            padding-left: 30%;
            padding-top: 10%;
        }

        .alignCENTER {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width: 100%;
        }
    </style>
</head>

<body>
<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-md-6">
                    <h4 class="page-title "> Conditional Date/Time Surcharge SetUp </h4>

                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-info" style="height:360px;">
                        <!-- form start -->
                        <form onsubmit="return false;">
                            <div class="box-body row">
                                <div class="col-xs-12" style="text-align:center">
                                    <h4><strong><span class="page-title ">Set Up  Conditional Date/Time Surcharge</span>
                                    </strong></h4>
                                    <br>
                                </div><!-- col-xs-12-->
                            </div>

                            <div class=" row">

                                <div class="col-sm-1" style="text-align:center;"></div>
                                <div class="col-xs-2" style="text-align:center;"><b> <br> After Hours/Early Morning
                                    Pickups</b>

                                </div>

                                <div class="col-xs-2" style="text-align:center;width:19%"><b>Amount</b>
                                    <input type="text" class="form-control " id="amount" placeholder="$"></div>

                                <div class="col-xs-2" style="text-align:center;"><b>Start Time</b>
                                    <input type="text" class="form-control time-picker" id="start_time"></div>

                                <div class="col-xs-2" style="text-align:center;"><b>End Time</b>
                                    <input type="text" class="form-control time-picker" id="end_time"></div>


                                <br>
                                <br>
                                <br>
                            </div>
                            <br/>
                            <br/>
                            <!-- Remaining Miles Row-->
                            <div class="row">
                                <div class="col-sm-1" style="text-align:center;"></div>
                                <div class="col-xs-2" style="text-align:center;"><b> </b>

                                </div>
                                <div class="col-sm-2" style="text-align:center; width:19%"><b>Applies to Service
                                    Type</b>

                                    <select class="service_type" id="service_type" multiple="multiple"
                                            name="service_type" required>

                                        <option>--Select Service--</option>
                                    </select>

                                </div>

                                <div class="col-xs-2" style="text-align:center;width:17%"><b>Applies to Vehicle
                                    Type </b>
                                    <select id="apply_vehicle_type" multiple="multiple">
                                        <option>--Select--</option>
                                    </select>
                                </div>

                                <div class="col-xs-2" style="text-align:center;"><b>Associate with SMA</b>
                                    <select id="apply_sma" multiple="multiple">
                                        <option>Select SMA</option>
                                    </select>
                                </div>

                                <br>
                                <br>
                            </div>
                            <!-- Apply to vehicle type-->
                            <div class="row">

                                <br>
                                <br>
                                <br>
                            </div>
                            <!-- Associate with SMA -->

                            <br>
                            <div class="row">
                                <div class="col-xs-12 " style="text-align:center"><b></b>
                                    <button type="button" id="save_rate" class="btn btn-primary" style="width:10%;">
                                        Save
                                    </button>
                                    <button type="button" id="back_button" class="btn btn-primary"
                                            style="display:none;"> Back
                                    </button>


                                </div>
                            </div><!-- row save matrix button-->


                            <!-- /.box-body -->

                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div data-ng-controller="SharedData" class="m-t-md ng-scope">
                        <!--######################################### Conditional Time Pickup Surcharge Data Display ####################################################-->
                        <div>
                            <div class="row">
                                <div class="col-xs-12"></div>
                            </div>
                            <div class="row mb-sm ng-scope">
                                <div class="col-sm-12"></div>
                            </div>
                            <div style="overflow:scroll;min-height:500px;">
                                <table class="table table-condensed ng-scope sieve">
                                    <thead>
                                    <tr>
                                        <th style="text-align:center">Amount($)</th>

                                        <th style="text-align:center">Start Time</th>
                                        <th style="text-align:center">End Time</th>
                                        <th style="text-align: center;padding-right: 2%;">Apply To Vehicle</th>

                                        <th style="text-align: center;padding-right: 2%;">Service Type</th>

                                        <th style="text-align: center;padding-right: 2%;">Associate with SMA</th>
                                        <th style="text-align:center">Action</th>
                                        <th class="table-action"><span class="table-action"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody id="rate_matrix_list">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!--##################finish###########################-->

            <!-- page end-->
        </section>
    </section>

</section>

<!--#######################code to add popup on sma list##########################################################-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display:none;"
     id="postal_code_modal">
    <div style="position:relative; background:black; opacity:0.5; top: 0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top: 0px; left:-17%; width:136%; height:100%" class="alignCENTER">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%;display:table-row">
                <div style="width:100%;display:table-cell; vertical-align:middle" class="alignCENTER">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; "
                         id="asseriesUpdateOverlay">
                        <div style="display:table-cell; vertical-align:center;" class="alignCENTER">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div style="width:80%">
                                <!--  code of popup start-->
                                <div class="container">
                                    <div id="modal" class="popupContainer" style="display:block;  top: 23px; position: absolute;
  width: 21%;
  
  left: 39%;
  top: 30px;
  background: #FFF;">
                                        <header class="popupHeader" style="  background: #F4F4F2;
  position: relative;
  padding: 10px 20px;
  border-bottom: 1px solid #DDD;
  font-weight: bold;
  font-family: 'Source Sans Pro', sans-serif;
  font-size: 14px;
  color: #666;
  font-size: 16px;
  text-transform: uppercase;
  text-align: left;
  height:9%"><span class="header_title">Postal Codes </span> <img src="images/remove.png" alt="" id="close_postal_popup"
                                                                  class="close_popup"> <span
                                                id="errmsg_add_airport_rate" style="color:red;"></span></header>
                                        <section class="popupBody">
                                            <div class="form-group serviceType_css">
                                                <div class="col-xs-9 show_postal"
                                                     style="margin-bottom: 8%; font-size: 17px;">

                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->

<!--Loading indicaator-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;"
     id="refresh_overlay">
    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%;display:table-row">
                <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; ">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="loadingGIF"><img style="height:70px;width:70px;" src="images/loading.gif"
                                                         alt="Page loading indicator"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="pageJs/dashboard.js"></script>
<script src="pageJs/logout.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="js/moment.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>


<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="assets/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="pageJs/searchbox.js"></script>
<script src="pageJs/conddate_surcharge.js"></script>
<!--<script src="assets/flot-chart/jquery.flot.js"></script> 
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script> 
<script src="assets/flot-chart/jquery.flot.resize.js"></script> 
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script> -->

<!--dynamic table-->
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>


<!--dynamic table initialization -->
<script src="js/dynamic_table/dynamic_table_init.js"></script>

<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="JQ_timepicker/jquery_addon_js.js"></script>
<script src="JQ_timepicker/slideaccess.js"></script>

<script>
    $("table.sieve").sieve();
    $(function () {

        $('#start_time, #end_time').timepicker({timeFormat: "hh:mm tt"});

    });
    setTimeout(function () {

            //getState(cityAndState);
            $("#apply_vehicle_type").multiselect('destroy');
            $('#apply_vehicle_type').multiselect({
                maxHeight: 200,
                buttonWidth: '155px',
                includeSelectAllOption: true
            });
            $("#value").multiselect('destroy');
            $('#value').multiselect({
                maxHeight: 200,
                buttonWidth: '155px',
                includeSelectAllOption: true
            });
            $("#apply_sma").multiselect('destroy');
            $('#apply_sma').multiselect({
                maxHeight: 200,
                buttonWidth: '155px',
                includeSelectAllOption: true
            });
        }
        , 900);
</script>

<!--<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/getairports.js"></script> 
<script src="pageJs/getState.js"></script> 
<script src="pageJs/getSeaport.js"></script> 
<script src="pageJs/getStatePopup.js"></script> 
<script src="pageJs/getVehicle.js"></script> 
<script src="pageJs/getService.js"></script> 
<script src="pageJs/peakhour.js"></script> 
<script src="pageJs/extrachild.js"></script> -->

<!-- Loading Indicator Ends-->
</body>
</html>
