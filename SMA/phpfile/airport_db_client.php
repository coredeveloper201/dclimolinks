<?php

include_once 'airport_db_server.php';

$action = $_REQUEST['action'];
$response=array();
switch ($action) {
		
	case "getCountrySeaportDb":
		$response=getCountrySeaportDb();
		echo json_encode($response);
		break;

	case "getSeaportCityAndCode":
		$response=getSeaportCityAndCode();
		echo json_encode($response);
		break;

	case "getStateAirportDb":
		$response=getStateAirportDb();
		echo json_encode($response);
		break;

	case "getCountryAirportDb":
		$response=getCountryAirportDb();
		echo json_encode($response);
		break;

	case "getStateSeaportDb":
		$response=getStateSeaportDb();
		echo json_encode($response);
		break;

	case "getCountryTrainDb":
		$response=getCountryTrainDb();
		echo json_encode($response);
		break;

	case "getStateTrainDb":
		$response=getStateTrainDb();
		echo json_encode($response);
		break;

	case "airport_activeFormSubmit":
		$response=airport_activeFormSubmit();
		echo json_encode($response);
		break;

	case "train_activeFormSubmit":
		$response=train_activeFormSubmit();
		echo json_encode($response);
		break;

	case "seaport_activeFormSubmit":
		$response=seaport_activeFormSubmit();
		echo json_encode($response);
		break;

	case "isAirportCheck":
		$response=isAirportCheck();
		echo json_encode($response);
		break;
		
	case "isTrainCheck":
		$response=isTrainCheck();
		echo json_encode($response);
		break;

	case "isSeaportCheck":
		$response=isSeaportCheck();
		echo json_encode($response);
		break;
		
	case "getTrainCityAndCode":
		$response=getTrainCityAndCode();
		echo json_encode($response);
		break;

	case "getAiportCityAndCode":
		$response=getAiportCityAndCode();
		echo json_encode($response);
		break;

	case "setImportDatabaseFIleTrain":
		$response=setImportDatabaseFIleTrain();
		echo json_encode($response);
		break;

	case "setManualTrain":
		$response=setManualTrain();
		echo json_encode($response);
		break;

	case "setImportDatabaseFIle":
		$response=setImportDatabaseFIle();
		echo json_encode($response);
		break;

	case "setManualAirport":
		$response=setManualAirport();
		echo json_encode($response);
		break;

	case "setImportDatabaseFIleSeaport":		
		$response=setImportDatabaseFIleSeaport();
		echo json_encode($response);
		break;

	case "setManualSeaport":		
		$response=setManualSeaport();
		echo json_encode($response);
		break;
		
	case "getImportDatabaseFIleSeaPort":
		$response=getImportDatabaseFIleSeaPort();
		echo json_encode($response);
		break;


	case "getImportDatabaseFIleTrain":
		$response=getImportDatabaseFIleTrain();
		echo json_encode($response);
		break;


	case "getImportDatabaseFIle":
		$response=getImportDatabaseFIle();
		echo json_encode($response);
		break;


	case "getSmaCountry":
		$response=getSmaCountry();
		echo json_encode($response);
		break;

	case "getSmaState":
		$response=getSmaState();
		echo json_encode($response);
		break;

	case "getSmaCity":
		$response=getSmaCity();
		echo json_encode($response);
		break;
	
}

