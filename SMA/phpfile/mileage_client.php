<?php

include_once 'mileage_server.php';

$action = $_REQUEST['action'];
$response=array();
switch ($action) {
	case "getRateMatrix":
	$response=getRateMatrix();
	echo json_encode($response);
	break;
		
	case "setMilesRate":		
	$response=setMilesRate();
	echo json_encode($response);
	break;	
		
	case "getRateMatrixList":		
	$response=getRateMatrixList();
	echo json_encode($response);
	break;
		
	case "editRateMatrix":	
	$response=editRateMatrix();
	echo json_encode($response);
	break;
		
	case "deleteMilesRate":		
	$response=deleteMilesRate();
	echo json_encode($response);
	break;
	
	case "checkUniqueMatrix":		
	$response=checkUniqueMatrix();
	echo json_encode($response);
	break;
}//switch


