<?php 

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');

	/*****************************************************************
	Method:             setCondSurcharge()
	InputParameter:     vehicle_code,user_id,sma_id
	Return:             set Cond Surcharge
	*****************************************************************/
		function setCondSurcharge()
	{	
 		if(isset($_REQUEST['sma_id'])&&(isset($_REQUEST['vehicle_code'])  )&&(isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
	   	{
  			$userId=$_REQUEST['user_id'];
		  
		   	$VehicleCode=explode(',',$_REQUEST['vehicle_code']);
		   	$addSma=explode(',',$_REQUEST['sma_id']);
			$query ="insert into cond_surcharge(term_park,domestic_flight,int_flight,airport_fhv,fbo_pickup,seaport_fhv,seaport_pickup,concert_pickup,sport_event_pickup,train_pickup,user_id) value('".$_REQUEST['term_park']."','".$_REQUEST['domestic_flight']."','".$_REQUEST['int_flight']."','".$_REQUEST['airport_fhv']."','".$_REQUEST['fbo_pickup']."','".$_REQUEST['seaport_fhv']."','".$_REQUEST['seaport_pickup']."','".$_REQUEST['concert_pickup']."','".$_REQUEST['sport_event_pickup']."','".$_REQUEST['train_pickup']."','".$userId."')";
            $cs_id = operations($query);
	  		for($i=0;$i<count($VehicleCode);$i++)
	  		{
		  		$Vehquery="insert into cs_vehicle(cs_id,vehicle_code,user_id) value('".$cs_id."','".$VehicleCode[$i]."','".$userId."')";	
		  		$resource1 = operations($Vehquery);
	  		}
		  	for($j=0;$j<count($addSma);$j++)
	  		{
				$Smaquery="insert into cs_sma(cs_id,sma_id,user_id) value('".$cs_id."','".$addSma[$j]."','".$userId."')";	
		  		$resource2 = operations($Smaquery);
		 	}
			$conditional_data=json_decode($_REQUEST['conditional_arra']);		
			if(isset($conditional_data) && !empty($conditional_data))
			{			
				for($i=0;$i<count($conditional_data);$i++)
				{
					$Condquery="insert into conditional_surcharge_extra(parent_id,schrg_fee_name,schrg_fee) value('".$cs_id."','".$conditional_data[$i]->cond_fees."','".$conditional_data[$i]->cond_value."')";	
					$resource3 = operations($Condquery);				
				}
			}
		   
		   	$result=global_message(200,1008,$cs_id);		   
	   	}
	   	else
	   	{
		    $result=global_message(201,1003);
   		}	
		return $result;	
	}

	/*****************************************************************
	Method:             setCondSurcharge()
	InputParameter:     vehicle_code,user_id,sma_id
	Return:             set Cond Surcharge
	*****************************************************************/
	function getRateMatrixList()
	{
		if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
  		{
			$query="Select * from cond_surcharge where user_id='".$_REQUEST['user_id']."'";
			$resource= operations($query);
			$contents = array();

			for($i=0; $i<count($resource); $i++)
			{
				$vehicle_code=''; 
				$sma_name='';
				$sma_id='';
				$value='';
				$Vehquery="Select vehicle_code from cs_vehicle where cs_id='".$resource[$i]['id']."'";
				$resource1= operations($Vehquery);
				for($j=0; $j<count($resource1); $j++)
				{
					$vehicle_code .=$resource1[$j]['vehicle_code'].',';
				}
				$Smaquery="Select sma_id,sma_name from cs_sma,sma where sma.id=cs_sma.sma_id AND cs_sma.cs_id='".$resource[$i]['id']."'";
				$resource2= operations($Smaquery);
				for($k=0; $k<count($resource2); $k++)
				{
					$sma_name .=$resource2[$k]['sma_name'].',';
					$sma_id .=$resource2[$k]['sma_id'].',';
				}
				$conditionalQuery="select * from conditional_surcharge_extra where parent_id='".$resource[$i]['id']."' order by schrg_fee_name";
				 $resource3= operations($conditionalQuery);
				$conditional_arr=array();
				for($i_c=0;$i_c<count($resource3);$i_c++)
				{
					$conditional_arr[] = array('fees_name' => $resource3[$i_c]['schrg_fee_name'] ,'fees_value' => $resource3[$i_c]['schrg_fee']);
				}
				$contents[$i]['id']=$resource[$i]['id'];
				$contents[$i]['term_park']=$resource[$i]['term_park'];
				$contents[$i]['domestic_flight']=$resource[$i]['domestic_flight'];
				$contents[$i]['int_flight']=$resource[$i]['int_flight'];
				$contents[$i]['airport_fhv']=$resource[$i]['airport_fhv'];
				$contents[$i]['fbo_pickup']=$resource[$i]['fbo_pickup'];
				$contents[$i]['seaport_fhv']=$resource[$i]['seaport_fhv'];
				$contents[$i]['seaport_pickup']=$resource[$i]['seaport_pickup'];
				$contents[$i]['concert_pickup']=$resource[$i]['concert_pickup'];
				$contents[$i]['sport_event_pickup']=$resource[$i]['sport_event_pickup'];
				$contents[$i]['train_pickup']=$resource[$i]['train_pickup'];
				$contents[$i]['sma_id'] = $sma_id;
				$contents[$i]['conditional_arr'] = $conditional_arr;
				$contents[$i]['sma_name'] = $sma_name;
				$contents[$i]['vehicle_code']=$vehicle_code;
			}
			if(count($contents)>0 && gettype($contents)!="boolean")
		   	{
			   $result=global_message(200,1007,$contents);			   
		   	}
		   	else
		   	{
			   $result=global_message(200,1006);
		   	}		  
	  
	  
  		}
 		else
		{
	  		$result=global_message(201,1003);
  		}
  			return  $result;
	}


	/*****************************************************************
	Method:             deleteCondSurcharge()
	InputParameter:     cs_id
	Return:             delete Cond Surcharge
	*****************************************************************/
	function deleteCondSurcharge()
	{
		if((isset($_REQUEST['cs_id']) && !empty($_REQUEST['cs_id'])))
	   	{
		  $rowId=$_REQUEST['cs_id'];
		 
			$query="delete from cond_surcharge where id='".$rowId."'";
	    	$resource = operations($query);
			$queryDelete1="delete  from cs_sma where cs_id='".$rowId."'";
			$resource2 = operations($queryDelete1);
			$queryDelete2="delete  from cs_vehicle where cs_id='".$rowId."'";
			$resource3 = operations($queryDelete2);
			$queryDelete3="delete  from conditional_surcharge_extra where parent_id='".$rowId."'";
			$resource4 = operations($queryDelete3);					
			$result=global_message(200,1010);   
		}
	  	else
	  	{
		   $result=global_message(201,1003);
	  	}
		return $result;
	}

