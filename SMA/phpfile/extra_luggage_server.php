<?php

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');


function saveInfo()
{
    if (isset($_REQUEST['combinations']) && (isset($_REQUEST['action']))) {
        $combinations = (isset($_REQUEST['combinations']) && !empty($_REQUEST['combinations'])) ? $_REQUEST['combinations'] : '[]';

        $query = "select * from extra_luggage where type='combinations' ";
        $bd_id = operations($query);
        if(count($bd_id) == 0){
            $query = "insert into extra_luggage (type,content) value('combinations','" . $combinations . "')";
            $bd_id = operations($query);
        } else {
            $query = "update extra_luggage set content = '" . $combinations . "' where type='combinations'";
            $bd_id = operations($query);
        }



        $result = global_message(200, 1008, $bd_id);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}


function savePrice()
{
    if (isset($_REQUEST['extra_price']) && (isset($_REQUEST['action']))) {
        $combinations = (isset($_REQUEST['extra_price']) && !empty($_REQUEST['extra_price'])) ? $_REQUEST['extra_price'] : '[]';

        $query = "select * from extra_luggage where type='extra_price' ";
        $bd_id = operations($query);
        if(count($bd_id) == 0){
            $query = "insert into extra_luggage (type,content) value('extra_price','" . $combinations . "')";
            $bd_id = operations($query);
        } else {
            $query = "update extra_luggage set content = '" . $combinations . "' where type='extra_price'";
            $bd_id = operations($query);
        }



        $result = global_message(200, 1008, $bd_id);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

function saveDisclaimer()
{
    if (isset($_REQUEST['disclaimer']) && (isset($_REQUEST['action']))) {
        $disclaimer = (isset($_REQUEST['disclaimer']) && !empty($_REQUEST['disclaimer'])) ? $_REQUEST['disclaimer'] : '';

        $query = "select * from extra_luggage where type='disclaimer' ";
        $bd_id = operations($query);
        if(count($bd_id) == 0){
            $query = "insert into extra_luggage (type,content) value('disclaimer','" . $disclaimer . "')";
            $bd_id = operations($query);
        } else {
            $query = "update extra_luggage set content = '" . $disclaimer . "' where type='disclaimer'";
            $bd_id = operations($query);
        }



        $result = global_message(200, 1008, $bd_id);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}


function get_extra_luggage_info()
{

    $query = "Select * from extra_luggage";
    $resource = operations($query);
    $result = global_message(200, 1007, $resource);
    return $result;

}


$action = $_REQUEST['action'];
$response = array();
switch ($action) {

    case "saveInfo":
        $response = saveInfo();
        echo json_encode($response);
        break;

    case "savePrice":

        $response = savePrice();
        echo json_encode($response);
        break;

    case "getInfo":

        $response = get_extra_luggage_info();
        echo json_encode($response);
        break;

    case "saveDisclaimer":

        $response = saveDisclaimer();
        echo json_encode($response);
        break;




}