		
	/*---------------------------------------------
        Template Name: Mylimoproject
        Page Name: car Seat Surcharge
        Author: Mylimoproject
    ---------------------------------------------*/
    var _SERVICEPATH="phpfile/service.php";
	var _SERVICEPATH="phpfile/service.php";
	var _SERVICEPATHServer="phpfile/client.php";
	var _SERVICEPATHSma="phpfile/sma_client.php";
	var _SERVICEPATHCarSeat="phpfile/car_seat_client.php";
	var ADDMILEAGE =0;
	/*load the data on page load*/
	$(function(){
        /*click on back button*/
	    $('#back_button').on("click",function(){
			location.reload(true);
		});
		$("#seat_1").keypress(function (e) {
		
			if  (e.which != 46 && e.which != 45 && e.which != 46 &&
		
			!(e.which >= 48 && e.which <= 57))  {
		
			    return false;
		
			}
		
		  })
		
		$("#seat_2").keypress(function (e) {
		
			 //if the letter is not digit then display error and don't type anything
		
		    if  (e.which != 46 && e.which != 45 && e.which != 46 &&
		
			  !(e.which >= 48 && e.which <= 57))  {
		
			    return false;
		
			}
		
		});	 
		
		$("#seat_3").keypress(function (e) {
		
			//if the letter is not digit then display error and don't type anything
		
		    if  (e.which != 46 && e.which != 45 && e.which != 46 &&
		
			  !(e.which >= 48 && e.which <= 57))  {
		
					   return false;
			}
		
		});
		
	
	    $("#seat_4").keypress(function (e) {
			//if the letter is not digit then display error and don't type anything
		
	        if(e.which != 46 && e.which != 45 && e.which != 46 &&
		
			  !(e.which >= 48 && e.which <= 57))  {
		
				return false;
		
			}
		
		});
		
		  
		/*load the function on page load*/
		getSMA();
		getVehicleType();	
		getRateMatrixList();
		getServiceTypes();
		
	    /***************************
		@Method Name	:On clicking edit button of the list
		@Desc			:Populate All the details of the Rate matrix 
		*****************************/
		
		$('.rate_matrix_onedit').on("click",function(){
		
			getSequence=$(this).attr("seq");
		
			editRateMatrix(getSequence);
		
		});
		
	});
		
    /*load data on page load*/	   	
	isUserApiKey();
	/*---------------------------------------------
        Function Name: isUserApiKey()
        Input Parameter:user_id
        return:json data
    ---------------------------------------------*/		
	function isUserApiKey()
	{	
		
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;	
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}	
		$.ajax({
			url: _SERVICEPATHServer,
			type: 'POST',
			data: "action=getApiKey&user_id="+userInfoObj[0].id,
			success: function(response) {		
			    var responseObj=response;
			    if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);			
				}
				if(responseObj.code=="1017")
				{		
				    window.localStorage.setItem("apiInformation",JSON.stringify(responseObj.data[0]));		  		  
					var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
					if(typeof(getLocalStoragevalueUserInformation)=="string")
				    {
						getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
					}
			
					$('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
					$('#add_sma_button').on('click',function(){
						$('#add_sma').show();								
					})	
					$("#popup_close_add_sma").off();
						$('#popup_close_add_sma').on("click",function(){
						$('#add_sma').hide();
					});	
				}
				else
				{			
					window.location.href="connectwithlimo.html";			
				}			
		
			}
		
		});
		
	}
		
		
   /*---------------------------------------------
       Function Name: getSMA()
       Input Parameter:user_id
       return:json data
    ---------------------------------------------*/		
	function getSMA()
	{
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}
		$.ajax({
			url: _SERVICEPATHSma,
			type: 'POST',
			data: "action=getSMA&user_id="+userInfoObj[0].id,
			success: function(response) {
				var responseObj=response;
				var responseHTML='';
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}	
				var responseHTML='';
				for(var i=0; i<responseObj.data.length; i++)
				{	
					if(responseObj.data[i].id!=null){          
							responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].sma_name+'</option>';	
					}
				}
				$('#apply_sma').html(responseHTML);
				setTimeout(function(){				
					$("#apply_sma").multiselect('destroy');
					$('#apply_sma').multiselect({
						maxHeight: 200,
						buttonWidth: '155px',
						includeSelectAllOption: true
					});
				},400);
						
		    },
			error:function(){
					alert("Some Error");}
			});
		}
		
			
    /*---------------------------------------------
       Function Name: getSMA()
       Input Parameter:user_id
       return:json data
    ---------------------------------------------*/					
	function getVehicleType()
	{
		
		var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
		if(typeof(getLocalStoragevalue)=="string")
		{
			getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		}

		var getuserId=window.localStorage.getItem("companyInfo");
		    if(typeof(getuserId)=="string")
			{
			   getuserId=JSON.parse(getuserId);
			}
		$.ajax({
			url: _SERVICEPATH,
			type: 'POST',
			data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&user_id="+getuserId[0].id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
			success: function(response) {
			    var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);					
				}
				responseHTML='';
				for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++)
				{
					responseHTML +='<option value="'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</option>';
				}
				$('#apply_vehicle_type').html(responseHTML);
					setTimeout(function(){
						$("#apply_vehicle_type").multiselect('destroy');
						$('#apply_vehicle_type').multiselect({
						    maxHeight: 200,
						    buttonWidth: '155px',
						    includeSelectAllOption: true
					});
				},400);
			}
		
		});
		
	}
		
	/*click on save rate button*/	
	$('#save_rate').on("click",function(){
		
		$('#refresh_overlay').css("display","block");
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}
		var service_type = $('#service_type option:selected');
		var service_typeObject = [];
		$(service_type).each(function(index, service_type){
			service_typeObject .push($(this).val());
		});
		var selectedVehicle = $('#apply_vehicle_type option:selected');
		var selectedVehicleObject = [];
		$(selectedVehicle).each(function(index, selectedVehicle){
			selectedVehicleObject .push($(this).val());
		});
	    var selectedSma = $('#apply_sma option:selected');
		var selectedSmaObject = [];
	    $(selectedSma).each(function(index, selectedSma){
			selectedSmaObject .push($(this).val());
		});
		var seat_1=$('#seat_1').val();
	    var seat_2=$('#seat_2').val();
		var seat_3=$('#seat_3').val();
		var seat_4=$('#seat_4').val();
		if(selectedVehicleObject=='')
		{
			$('#refresh_overlay').css("display","none");
			alert("Please Select At Least One Vehicle Code");
		}
		if(service_typeObject=='')
		{
			$('#refresh_overlay').css("display","none");
			alert("Please Select At Least One Vehicle Code");
		}
		else if(selectedSmaObject=='')
		{  
		    $('#refresh_overlay').css("display","none");
		    alert("Please Select At Least One SMA ");
		}
		else
		{				
			if($('#save_rate').html()=='Update')
			{
				$('#save_rate').html('Processing...');
				$('#save_rate').attr("disable","true");
				var carseat_id=$('#save_rate').attr('seq')
				$.ajax({
					url: _SERVICEPATHCarSeat,
					type: 'POST',
					data: "action=deleteCarSeatSurcharge&carseat_id="+carseat_id,
					success: function(response) {
						if(typeof(response)=="string")
						{
						    responseObj=JSON.parse(response);
						}
						if(responseObj.code==1010)
						{
	                        $.ajax({
								url: _SERVICEPATHCarSeat,
								type: 'POST',
							    data: "action=setCarSeatSurcharge&user_id="+userInfoObj[0].id+"&service_typeObject="+service_typeObject+"&vehicle_code="+selectedVehicleObject+"&seat_1="+seat_1+"&seat_2="+seat_2+"&seat_3="+seat_3+"&seat_4="+seat_4+"&sma_id="+selectedSmaObject,
								success: function(response){
								    $('#refresh_overlay').css("display","none");
									if(typeof(response)=="string")
									{
										responseObj=JSON.parse(response);										
									}
									if(responseObj.code==1008)
									{
										alert("Car Seat Surcharge Updated Successfully");
										location.reload(true);
									}
								    else
									{
			
										$('#save_rate').html('Update');
										$('#save_rate').attr("disable","false");
										alert("Some Error");
									}
			
								},
								error:function(){
			
									$('#refresh_overlay').css("display","none");
									$('#save_rate').html('Update');
								    $('#save_rate').attr("disable","false");
									alert("Some Error");
								}
			
						    });
						}
						else
						{
							alert("Some Server Error while Updating.");
							$('#save_rate').html('Update');
							$('#save_rate').attr("disable","false");
						}
		
					},
					error:function(){
						$('#refresh_overlay').css("display","none");
						$('#save_rate').html('Update');
					    $('#save_rate').attr("disable","false");
						alert("Some Error");
					}
			    });
		
			}
			else
			{
			
				$('#save_rate').html('Processing...');
				$('#save_rate').attr("disable","true");
				$.ajax({
					url: _SERVICEPATHCarSeat,
					type: 'POST',
					data: "action=setCarSeatSurcharge&user_id="+userInfoObj[0].id+"&vehicle_code="+selectedVehicleObject+"&service_typeObject="+service_typeObject+"&seat_1="+seat_1+"&seat_2="+seat_2+"&seat_3="+seat_3+"&seat_4="+seat_4+"&sma_id="+selectedSmaObject,
					success: function(response) {
						$('#refresh_overlay').css("display","none");
						if(typeof(response)=="string")
						{
						    responseObj=JSON.parse(response);
						}
					    if(responseObj.code==1008)
						{
							alert("Car Seat Surcharge Added Successfully");
							location.reload(true);
						}
						else
						{
							$('#save_rate').html('Save');
							$('#save_rate').attr("disable","false");
							alert("Some Error");
						}
			
				    },
					error:function(){

						$('#refresh_overlay').css("display","none");
						$('#save_rate').html('Save');
						$('#save_rate').attr("disable","false");
					    alert("Some Error");
					}
			
				});
			
			}
			
	    }
		
    });
		
		
			
	 /**********************************************
	    Method:             getServiceTypes()
		Return:Rate         List Of service type Name
		parameter:          user_id
	 *************************************************/
	function getServiceTypes()
    {
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}
		var serviceTypeData = [];
			$.ajax({
				url : "phpfile/service_type.php",
				type : 'post',
				data : 'action=GetServiceTypes&user_id='+userInfoObj[0].id,
				dataType : 'json',
				success : function(data){
					
					if(data.ResponseText == 'OK'){
						var ResponseHtml='';
						$.each(data.ServiceTypes.ServiceType, function( index, result){
							ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
						});
					    $('#service_type').html(ResponseHtml);
					}
					$("#service_type").multiselect('destroy');
					$('#service_type').multiselect({
						maxHeight: 200,
						buttonWidth: '178px',
						includeSelectAllOption: true
					});
			  }
			});
		}
		
	/*******************************************************
		@MethodName			:getRateMatrix
		@Description		:Get list of all Rate matrixes
		@param				:user_id
		@return			    :list of rate matrix
	********************************************************/
	function getRateMatrixList()
	{
		$('#refresh_overlay').css("display","block");
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}
		$.ajax({
			url:_SERVICEPATHCarSeat,
			type:'POST',
			data:"action=getRateMatrixList&user_id="+userInfoObj[0].id,
			success:function(response)
			{
				var responseObj=response;
				var responseHTML='';
				var responseVeh='';
				var responseSma='';
				var vehicle_code='';
				var sma_name='';
				var vehicle_code_array='';
				var service_typeArray=[];
				var sma_name_array='';
				var sma_id='';
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}
				if(responseObj.data!=null && responseObj.data!="null" && responseObj.data!=undefined && responseObj.data!="undefined" &&
								 responseObj.data!="")
				{
				    for(var i=0; i<responseObj.data.length; i++)
					{
						responseVeh=0;
						responseSma=0;
						vehicle_code = responseObj.data[i].vehicle_code.replace(/,\s*$/, "");
						vehicle_code_array=vehicle_code.split(',');
						service_type = responseObj.data[i].service_type.replace(/,\s*$/, "");
					    service_typeArray=vehicle_code.split(',');
						sma_name = responseObj.data[i].sma_name.replace(/,\s*$/, "");
						sma_name_array=sma_name.split(',');
						var s=0;
						for(s=0;s<vehicle_code_array.length;s++)
						{
							responseVeh +='<option selected disabled>'+vehicle_code_array[s]+'</option>';
						}
						var t=0;
						for(t=0;t<sma_name_array.length;t++)
						{
							responseSma +='<option selected disabled>'+sma_name_array[t]+'</option>';
						}
						sma_id=responseObj.data[i].sma_id.replace(/,\s*$/, "");
						responseHTML +='<tr seq="'+responseObj.data[i].id+'"><td style="text-align:center" id="rate_seat1_'+responseObj.data[i].id+'" >'+responseObj.data[i].seat_1+'</td><td style="text-align:center" id="rate_seat2_'+responseObj.data[i].id+'" >'+responseObj.data[i].seat_2+'</td><td style="text-align:center" id="rate_seat3_'+responseObj.data[i].id+'" >'+responseObj.data[i].seat_3+'</td><td style="text-align:center" id="rate_seat4_'+responseObj.data[i].id+'" >'+responseObj.data[i].seat_4+'</td><td style="text-align:center;width: 21%; text-align: justify;" id="rate_matrix_veh_'+responseObj.data[i].id+'" veh_code="'+vehicle_code +'" service_type="'+service_type+'"><select class="refresh_multi_select" multiple>'+responseVeh+'</select></td><td style="text-align:center;width: 21%; text-align: justify;" id="rate_matrix_sma_'+responseObj.data[i].id+'" sma_id="'+sma_id +'" ><select class="refresh_multi_select" multiple>'+responseSma+'</select></td><td style="text-align:center"><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs rate_matrix_onedit" id=edit_rate_'+responseObj.data[i]['id']+' onclick="editRateMatrix('+responseObj.data[i].id+',1)">Edit</a> <a class="btn btn-xs rate_matrix_ondelete"  seq='+responseObj.data[i]['id']+' onclick="deleteCarSeatSurcharge('+responseObj.data[i].id+')">Delete</a> </div></td></tr>';
					} 
				}else
				{
					responseHTML='<tr><td colspan="1">Data not Found.</td></tr>';
			    }
					$('#rate_matrix_list').html(responseHTML)
					setTimeout(function(){
						$(".refresh_multi_select").multiselect('destroy');
						$('.refresh_multi_select').multiselect({
							maxHeight: 200,
							buttonWidth: '155px',
							includeSelectAllOption: true
						});
		
					},800);
		        $('#refresh_overlay').css("display","none");         
			},
			error:function(){
				alert("Some Error");
				$('#refresh_overlay').css("display","none");					
			}
		
		});
	}
		
		
	/*******************************************************
		@MethodName			:editRateMatrix
		@Description		:edit the rate matrix list
		@param				:getSequence
		@return			    :edited data of rate
	********************************************************/	
	function editRateMatrix(getSequence,status)
	{
		
	    $('#refresh_overlay').css("display","block");
		$('#save_rate').html("Update");
		$('#save_rate').attr("seq",getSequence);
		$('#back_button').css("display","inline-block");
		var rate_matrix_veh=$('#rate_matrix_veh_'+getSequence).attr("veh_code");
			rate_matrix_veh=rate_matrix_veh.split(',');
		$("#apply_vehicle_type").val(rate_matrix_veh);
		$("#apply_vehicle_type").multiselect('refresh');
		var service_type=$('#rate_matrix_veh_'+getSequence).attr("service_type");
			service_type=service_type.split(',');
		$("#service_type").val(service_type);
		$("#service_type").multiselect('refresh');
		var rate_matrix_sma=$('#rate_matrix_sma_'+getSequence).attr("sma_id");
			rate_matrix_sma=rate_matrix_sma.split(',');
		$("#apply_sma").val(rate_matrix_sma);
		$("#apply_sma").multiselect('refresh');
		$('#seat_1').val($('#rate_seat1_'+getSequence).html());
		$('#seat_2').val($('#rate_seat2_'+getSequence).html());
		$('#seat_3').val($('#rate_seat3_'+getSequence).html());
		$('#seat_4').val($('#rate_seat4_'+getSequence).html());
		setTimeout(function(){
		    $("html, body").animate({ scrollTop: 0 }, "slow");	
			$('#refresh_overlay').css("display","none");
		},200);
	}
		
	 	
   /*---------------------------------------------
       Function Name: deleteCarSeatSurcharge()
       Input Parameter: getSequence
       return:json data
    ---------------------------------------------*/	
	function deleteCarSeatSurcharge(getSequence){

		var res = confirm("Do You Really Want To Delete This?");
		if (res == true) {
		    $('#refresh_overlay').css("display","block");
			$.ajax({
				url: _SERVICEPATHCarSeat,
				type: 'POST',
			    data: "action=deleteCarSeatSurcharge&carseat_id="+getSequence,
				success: function(response) {
					$('#refresh_overlay').css("display","none");
					getRateMatrixList();
				},
				error:function(){
					alert("Some Error");
					$('#refresh_overlay').css("display","none");
				}
		
			});
		}
		else
		{
		
      	}
	}
		
    /*******************************************************
		@Method			:checknegative
		@Description	:restrict entering negative numbers
		@Param			:none
		@RETURN			:none
	********************************************************/
	function checknegative() {
		
		if ($('#percent_increase').val() < 0) {
			$('#percent_increase').val(0) ;
			$('#percent_increase').focus();
			alert('Negative Values Not allowed');
			return false;
		}
		
	}
		
	/* Page Script */
	setTimeout(function(){

	    $("#apply_vehicle_type").multiselect('destroy');
		$('#apply_vehicle_type').multiselect({
			maxHeight: 200,
			buttonWidth: '155px',
			includeSelectAllOption: true
		});
		$("#apply_sma").multiselect('destroy');
			$('#apply_sma').multiselect({
				maxHeight: 200,
				buttonWidth: '155px',
				includeSelectAllOption: true
			});					
	},900);			