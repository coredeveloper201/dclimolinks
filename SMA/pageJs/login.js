var _SERVICEPATH = "phpfile/client.php";



$(function() {

    localStorage.removeItem("apiInformation");
    localStorage.removeItem("companyInfo");

    $('#forgetBtn').on("click", function() {
        var emailId = $('#emailId').val();
        var getData={"email":emailId };
        $('#refresh_overlay').show();

         $.ajax({

        url: _SERVICEPATH,

        type: 'POST',

        data: "action=forgetPwd&email=" + emailId,

        success: function(response) {
        	console.log("response...",response);
            $('#refresh_overlay').hide();

           alert("Password and User name sent to your registered email");

        }
    });







    });
    $('#userLogIn').on("click", function() {
        logIn();
    });
});

function logIn(e) {
    e.preventDefault();
    var getUserName = $('#user_email').val();
    var getUserPassword = $('#user_password').val();
    $.ajax({

        url: _SERVICEPATH,

        type: 'POST',

        data: "action=logIn&email=" + getUserName + "&password=" + getUserPassword,

        success: function(response) {
            responseObj = JSON.parse(response);
            if (responseObj.code === 1004){

                $('#loginSuccessMsg').show();
                setTimeout(function() {
                    $('#loginSuccessMsg').hide();
                }, 1000);


                var set_limo = responseObj.data[0].limo_setup;
                localStorage.setItem('limo_mode', set_limo);
                window.localStorage.setItem("companyInfo", JSON.stringify(responseObj.data));
                localStorage.setItem('limo_mode', set_limo);

                // window.location.href = "index.html";
                window.location.href = "./";


            } else {
                $('#loginInfoMsg').show();
                setTimeout(function() {
                    $('#loginInfoMsg').hide();
                }, 1000)

            }
        }

    });
}
function resetPassword(e) {
    e.preventDefault();
    var password = $('#user_password').val();
    var con_password = $('#user_password_conf').val();
    var reset = $('#reset').val();
    $.ajax({
        url: _SERVICEPATH,
        type: 'POST',
        data: {
            action: 'resetPass',
            password: password,
            con_password: con_password,
            reset: reset
        },
        success: function(response) {
            var res = JSON.parse(response);
            alert(res.msg);
            if(res.status === 2000){
                window.location.href = 'login.php';
            }
        }

    });
}