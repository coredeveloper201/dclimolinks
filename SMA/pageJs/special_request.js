/*---------------------------------------------
   Template Name: Mylimoproject
   Page Name: Special Request
   Author: Mylimoproject
---------------------------------------------*/

/*set the php url path for web service*/
var _SERVICEPATH="phpfile/service.php";
var _SERVICEPATHServer="phpfile/client.php";
var _SERVICEPATHSma="phpfile/sma_client.php";
var _SERVICEPATHSpRequest="phpfile/splreq_client.php";
var ADDMILEAGE =0;

/*call the load function on page load*/
$(function(){

    /*click on add button of millage rate*/
	$('#add_mileage_rate').on("click",function(){		
		new_mileage_rate();
    });	

    /*copy the existing rate matrix*/
	$('#copy_rate_matrix').on("click",function(){

		if($('#select_matrix').val()=='')
		{
			alert("Please Select Rate Matrix");
		}
		else
		{
			editRateMatrix($('#select_matrix').val(),0);
		}

    });

	/*click on back button */
	$('#back_button').on("click",function(){

		location.reload(true);
	});

	 /*key up function on the package amount*/
     $("#package_amount").keypress(function (e) {

     //if the letter is not digit then display error and don't type anything
        if(e.which != 46 && e.which != 45 && e.which != 46 && !(e.which >= 48 && e.which <= 57)){
            return false;
        }
    });

  
    $("input[name='copy_existing']").click(function(){

	    if($("input[name='copy_existing']:checked").val()=="yes")
		{
			$('#select_matrix_div').css("visibility","visible");
			$('#copy_rate_matrix').css("visibility","visible");
		}
	    else
	    {
			$('#select_matrix_div').css("visibility","hidden");
			$('#copy_rate_matrix').css("visibility","hidden");
		}
	});

	/*call the function on page load*/	  
     getSMA();
	 //getRateMatrix();
	 getVehicleType();	
	 getRateMatrixList();
 	 getServiceTypes();


	$('.rate_matrix_onedit').on("click",function(){

		getSequence=$(this).attr("seq");
		editRateMatrix(getSequence);
	});

});

/*call the isUserApiKey on page load*/
isUserApiKey();

    /*---------------------------------------------
       Function Name: getAiportCityAndCode()
       Input Parameter: rowId,categoryId
       return:json data
    ---------------------------------------------*/
	function isUserApiKey()
	{	
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;	
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}	
		$.ajax({
			url: _SERVICEPATHServer,
			type: 'POST',
			data: "action=getApiKey&user_id="+userInfoObj[0].id,
			success: function(response) {		
			    var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);			
				}
				if(responseObj.code=="1017")
				{		
				    window.localStorage.setItem("apiInformation",JSON.stringify(responseObj.data[0]));		  		  
			  	    var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");

					if(typeof(getLocalStoragevalueUserInformation)=="string")
				    {
				   	    getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);

				    }

				    $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
					$('#add_sma_button').on('click',function(){
						$('#add_sma').show();								

					})	
					$("#popup_close_add_sma").off();
					$('#popup_close_add_sma').on("click",function(){
					    $('#add_sma').hide();

			        });	
				}
				else
				{			
					window.location.href="connectwithlimo.html";			
				}			

			}

	    });

	}


    /*---------------------------------------------
        Function Name: getSMA()
        Input Parameter: user_id
        return:json data
    ---------------------------------------------*/
	function getSMA()
    {
	    var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
	    {
		    userInfoObj=JSON.parse(userInfo);
	    }
		$.ajax({
		    url: _SERVICEPATHSma,
		    type: 'POST',
		    data: "action=getSMA&user_id="+userInfoObj[0].id,
		    success: function(response) {
			    var responseObj=response;
				var responseHTML='';
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
	            }	
				var responseHTML='';
                if(responseObj.data[0].sma_name!=null){
				    for(var i=0; i<responseObj.data.length; i++)
				    {	
					    responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].sma_name+'</option>';	
				    }
                }
				$('#apply_sma').html(responseHTML);

				setTimeout(function(){				

				$("#apply_sma").multiselect('destroy');
					$('#apply_sma').multiselect({
					    maxHeight: 200,
					    buttonWidth: '155px',
					    includeSelectAllOption: true
					});
			    },400);

		    },
		    error:function(){

			    alert("Some Error");
			}

		});

    }

	
   /*---------------------------------------------
       Function Name: getAiportCityAndCode()
       Input Parameter: rowId,categoryId
       return:json data
    ---------------------------------------------*/
	function getServiceTypes()
	{
	    var userInfo=window.localStorage.getItem("companyInfo");
	    var userInfoObj=userInfo;
	    if(typeof(userInfo)=="string")
	    {
	        userInfoObj=JSON.parse(userInfo);
	    }
	    var serviceTypeData = [];
	    $.ajax({
	        url : "phpfile/service_type.php",
	        type : 'post',
	        data : 'action=GetServiceTypes&user_id='+userInfoObj[0].id,
	        dataType : 'json',
	        success : function(data){
	            if(data.ResponseText == 'OK'){
	                var ResponseHtml='';
	                $.each(data.ServiceTypes.ServiceType, function( index, result){
	                     ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
	                });
	                 
	                $('#service_type').html(ResponseHtml);
  	            }
	            $("#service_type").multiselect('destroy');
	            $('#service_type').multiselect({
	                maxHeight: 200,
	                bttonWidth: '178px',
	                includeSelectAllOption: true

	            });
	        }         
	    });
	}


	function getRateMatrix()
	{
	    var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
	    if(typeof(userInfo)=="string")
	    {
		    userInfoObj=JSON.parse(userInfo);
	    }
	    $.ajax({
		    url: _SERVICEPATHSpRequest,
		    type: 'POST',
		    data: "action=getRateMatrix&user_id="+userInfoObj[0].id,
		    success: function(response) {
			    var responseObj=response;
				var responseHTML='';
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}	
				var responseHTML='<option value="">--Select--</option>';
				for(var i=0; i<responseObj.data.length; i++)
				{	
					responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].name+'</option>';	
				}
				$('#select_matrix').html(responseHTML);
		    },
		    error:function(){
			    alert("Some Error");
			}
		});
	}
		

	/*---------------------------------------------
        Function Name: getVehicleType()
        Input Parameter: user_id
        return:json data
    ---------------------------------------------*/
	function getVehicleType()
	{
	    var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
	    if(typeof(getLocalStoragevalue)=="string")
	    {
	        getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
	
	    }

	    var getuserId=window.localStorage.getItem("companyInfo");
	    if(typeof(getuserId)=="string")
		{
		   getuserId=JSON.parse(getuserId);
		}

        $.ajax({
		    url: _SERVICEPATH,
		    type: 'POST',
		    data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&user_id="+getuserId[0].id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
		    success: function(response) {
 			    var responseHTML='';
			    var responseObj=response;
		    	if(typeof(response)=="string")
			    {
				    responseObj=JSON.parse(response);					

			    }
			    responseHTML='';
			    for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++)
			    {
		            responseHTML +='<option value="'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</option>';
			    }
			
			    $('#apply_vehicle_type').html(responseHTML);
			    setTimeout(function(){
				    $("#apply_vehicle_type").multiselect('destroy');
            	    $('#apply_vehicle_type').multiselect({
                        maxHeight: 200,
                        buttonWidth: '155px',
                        includeSelectAllOption: true
                     });
				},400);
		    }

	    });
	}

    /*---------------------------------------------
       Function Name: getAiportCityAndCode()
       Input Parameter: rowId,categoryId
       return:json data
    ---------------------------------------------*/ 
	$('#save_rate').on("click",function(){
	    $('#refresh_overlay').css("display","block");	
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
	    {
	        userInfoObj=JSON.parse(userInfo);
	    }
		var Value = $('#value option:selected');
        var ValueObject = [];
        var getValueConstant=1;
        $('.special_rate_name').each(function(index, Value){
        	if($(this).val()!='')
        	{
        		ValueObject .push($(this).val());	
        	}
        	else
        	{
        		getValueConstant=2;
        	}
        });
        
		var selectedVehicle = $('#apply_vehicle_type option:selected');
        var selectedVehicleObject = [];
        $(selectedVehicle).each(function(index, selectedVehicle){
            selectedVehicleObject .push($(this).val());
        });
        var service_type = $('#service_type option:selected');
        var service_typeObj = [];
        $(service_type).each(function(index, service_type){
            service_typeObj .push($(this).val());
        });
		var selectedSma = $('#apply_sma option:selected');
        var selectedSmaObject = [];
        $(selectedSma).each(function(index, selectedSma){
            selectedSmaObject .push($(this).val());
        });
		var package_code=$('#package_name').val();
		var amount=$('#package_amount').val();
		var packageDescription=$('#packageDescription').val();
	    if(getValueConstant==2)
		{
            $('#refresh_overlay').css("display","none");
			alert("Please Select Atleast One Value");
		}
		else if(service_typeObj=='')
		{
		    $('#refresh_overlay').css("display","none");
			alert("Please Enter Package Code");
		}
		else if(package_code=='')
		{
            $('#refresh_overlay').css("display","none");
			alert("Please Enter Package Code");
		}
		else if(amount=='')
		{
            $('#refresh_overlay').css("display","none");
			alert("Please Enter Amount ");
		}
		else if(selectedVehicleObject=='')
		{
            $('#refresh_overlay').css("display","none");
			alert("Please Select At Least One Vehicle Code");
		}
		else if(selectedSmaObject=='')
		{
            $('#refresh_overlay').css("display","none");
			alert("Please Select At Least One SMA ");
		}
		else
		{
		   if($('#save_rate').html()=='Update')
			{
			    $('#save_rate').html('Processing...');
			    $('#save_rate').attr("disable","true");
				var sr_id=$('#save_rate').attr('seq')
				$.ajax({
					url: _SERVICEPATHSpRequest,
					type: 'POST',
				    data: "action=deleteSpecialRequest&sr_id="+sr_id,
					success: function(response) {
                        $('#refresh_overlay').css("display","none");
						if(typeof(response)=="string")
				        {
 					        responseObj=JSON.parse(response);
				        }
						if(responseObj.code==1010)
						{
							$.ajax({
								url: _SERVICEPATHSpRequest,
								type: 'POST',
								data: "action=setSpecialRequest&user_id="+userInfoObj[0].id+"&package_code="+package_code+"&vehicle_code="+selectedVehicleObject+"&service_typeObj="+service_typeObj+"&amount="+amount+"&value="+ValueObject+"&sma_id="+selectedSmaObject+"&packageDescription="+packageDescription,
							    success: function(response){
											
									$('#refresh_overlay').css("display","none");	
									alert("Special Request Updated Successfully");
									location.reload(true);
								},
								error:function(){
		                            $('#refresh_overlay').css("display","none");
								    $('#save_rate').html('Update');
					                $('#save_rate').attr("disable","false");
						            alert("Some Error");
							    }

										
							});
						}
					    else
						{
		                    $('#refresh_overlay').css("display","none");
							alert("Some Server Error while Updating.");
							$('#save_rate').html('Update');
					    	$('#save_rate').attr("disable","false");
						}

					},
					error:function(){
		                $('#refresh_overlay').css("display","none");
						$('#save_rate').html('Update');
					    $('#save_rate').attr("disable","false");
						alert("Some Error");
		            }

				});

			}
		    else
			{
		                
			    $('#save_rate').html('Processing...');
			    $('#save_rate').attr("disable","true");
				$.ajax({
					url: _SERVICEPATHSpRequest,
					type: 'POST',
					data: "action=setSpecialRequest&user_id="+userInfoObj[0].id+"&package_code="+package_code+"&vehicle_code="+selectedVehicleObject+"&service_typeObj="+service_typeObj+"&amount="+amount+"&value="+ValueObject+"&sma_id="+selectedSmaObject+"&packageDescription="+packageDescription,
					success: function(response) {
						$('#refresh_overlay').css("display","none");
					    alert("Special Request Added Successfully");
						location.reload(true);
					},
					error:function(){
		                $('#refresh_overlay').css("display","none");
					    $('#save_rate').html('Save');
					    $('#save_rate').attr("disable","false");
						alert("Some Error");
					}

		        });

			}
		}

	});

	
    /*---------------------------------------------
        Function Name: getRateMatrixList()
        Input Parameter: user_id
        return:json data
    ---------------------------------------------*/
	function getRateMatrixList()
	{ 
		$('#refresh_overlay').css("display","block"); 
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}
		$.ajax({
			url:_SERVICEPATHSpRequest,
			type:'POST',
			data:"action=getRateMatrixList&user_id="+userInfoObj[0].id,
			success:function(response)
			{
				var responseObj=response;
				var responseHTML='';
				var responseVeh='';
				var responseSma='';
				var responseValue='';
				var vehicle_code='';
				var sr_value='';
				var sma_name='';
				var vehicle_code_array='';
				var sma_name_array='';
				var value_array='';
				var sma_id='';
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}
				if(responseObj.data!=null && responseObj.data!="null" && responseObj.data!=undefined && responseObj.data!="undefined" &&
					 responseObj.data!="")
				{
					for(var i=0; i<responseObj.data.length; i++)
					{
						responseVeh=0;
					 	responseSma=0;
						responseValue=0;
						vehicle_code = responseObj.data[i].vehicle_code.replace(/,\s*$/, "");
						vehicle_code_array=vehicle_code.split(',');
						sma_name = responseObj.data[i].sma_name.replace(/,\s*$/, "");
						sma_name_array=sma_name.split(',');
						sr_value = responseObj.data[i].value.replace(/,\s*$/, "");
						value_array=sr_value.split(',');
						sr_service = responseObj.data[i].service_type.replace(/,\s*$/, "");
					    var s=0;
						for(s=0;s<vehicle_code_array.length;s++)
						{
							responseVeh +='<option selected disabled>'+vehicle_code_array[s]+'</option>';
						}
						var t=0;
						for(t=0;t<sma_name_array.length;t++)
						{
							responseSma +='<option selected disabled>'+sma_name_array[t]+'</option>';
						}
						var u=0;
						for(u=0;u<value_array.length;u++)
						{
							if(value_array[u]!='')
							{
							    responseValue +='<option selected disabled>'+value_array[u]+'</option>';
							}
							
						}
						sma_id=responseObj.data[i].sma_id.replace(/,\s*$/, "");
					    if(responseObj.data[i].packageDescription=='')
						{
							responseObj.data[i].packageDescription=' ';
						}
					 	responseHTML +='<tr seq="'+responseObj.data[i].id+'"><td style="text-align:center" id="rate_matrix_'+responseObj.data[i].id+'">'+responseObj.data[i].package_code+'</td><td style="text-align:center; text-align: justify;" id="rate_matrix_val_'+responseObj.data[i].id+'" sr_val="'+sr_value +'" sr_service="'+sr_service +'"><select class="refresh_multi_select" multiple>'+responseValue+'</select></td><td style="text-align:center" id="sr_amount_'+responseObj.data[i].id+'">'+responseObj.data[i].amount+'</td><td style="text-align:center;width: 21%; text-align: justify;" id="rate_matrix_veh_'+responseObj.data[i].id+'" veh_code="'+vehicle_code +'"><select class="refresh_multi_select" multiple>'+responseVeh+'</select></td><td style="text-align:center;width: 21%; text-align: justify;" id="rate_matrix_sma_'+responseObj.data[i].id+'" sma_id="'+sma_id +'" ><select class="refresh_multi_select" multiple>'+responseSma+'</select></td><td style="text-align:center"><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs rate_matrix_onedit" id=edit_rate_'+responseObj.data[i]['id']+' packageDescription="'+responseObj.data[i].packageDescription+'" inc_percent="'+responseObj.data[i].miles_increase_percent+'" onclick="editRateMatrix('+responseObj.data[i].id+',1)">Edit</a> <a class="btn btn-xs rate_matrix_ondelete"  seq='+responseObj.data[i]['id']+' onclick="deleteRateMatrix('+responseObj.data[i].id+')">Delete</a> </div></td></tr>';

					} 
				}else{
					 	responseHTML='<tr><td colspan="1">Data not Found.</td></tr>';
			    }

				$('#rate_matrix_list').html(responseHTML);
				$('#refresh_overlay').css("display","none");
			    setTimeout(function(){
					$(".refresh_multi_select").multiselect('destroy');
					$('.refresh_multi_select').multiselect({
						maxHeight: 200,
						buttonWidth: '155px',
					    includeSelectAllOption: true
					});

				},800);
			},
			error:function(){
					alert("Some Error");
					$('#refresh_overlay').css("display","none");					
			}

		});

	}


    /*---------------------------------------------
        Function Name: getRateMatrixList()
        Input Parameter: user_id
        return:json data
    ---------------------------------------------*/
	function editRateMatrix(getSequence,status)
	{
		$('#refresh_overlay').css("display","block");
		$('#save_rate').html("Update");
		$('#save_rate').attr("seq",getSequence);

		var getPackageDescription=$('#edit_rate_'+getSequence).attr("packagedescription")
		$('#packageDescription').val(getPackageDescription);
		$('#back_button').css("display","inline-block");

		var package_code=$('#rate_matrix_'+getSequence).html();
		var amount=$('#sr_amount_'+getSequence).html();

		var rate_matrix_veh=$('#rate_matrix_veh_'+getSequence).attr("veh_code");
		rate_matrix_veh=rate_matrix_veh.split(',');

		$("#apply_vehicle_type").val(rate_matrix_veh);
		$("#apply_vehicle_type").multiselect('refresh');

		var rate_matrix_sma=$('#rate_matrix_sma_'+getSequence).attr("sma_id");
		rate_matrix_sma=rate_matrix_sma.split(',');

		$("#apply_sma").val(rate_matrix_sma);
		$("#apply_sma").multiselect('refresh');

		var spclreq_val=$('#rate_matrix_val_'+getSequence).attr("sr_val");
		spclreq_val=spclreq_val.split(',');

		var sr_service=$('#rate_matrix_val_'+getSequence).attr("sr_service");
		sr_service=sr_service.split(',');

		$("#service_type").val(sr_service);
		$("#service_type").multiselect('refresh');

	    $('.addRequest').html('');
		var getSeq=1;
		$.each(spclreq_val,function(index,dataValue){
			if(dataValue!='')
			{
				$('.addRequest').append('<div class="col-xs-3 addRequestDiv_'+getSeq+'" style="width: 26%;margin-top: 1%;"> <b style="margin-left:6%;">Special Request Name: </b><input type="text" class="form-control special_rate_name"  value="'+dataValue+'" style="width: 67%;display: inline-block;"><button class="btn btn-primary addRequestDiv" seq='+getSeq+'>X</button></div>');		
				getSeq++;
			}
			$('.addRequestDiv').off();
			$('.addRequestDiv').on("click",function(){
			    var getseq=$(this).attr("seq");
			    $('.addRequestDiv_'+getseq).remove();
		    });
		});
		$("#value").val(spclreq_val);
		$("#value").multiselect('refresh');
		$("#package_amount").val(amount);
		$("#package_name").val(package_code);
		setTimeout(function(){
			$("html, body").animate({ scrollTop: 0 }, "slow");	
			$('#refresh_overlay').css("display","none");
		},200);
	}

	/*---------------------------------------------
        Function Name: getRateMatrixList()
        Input Parameter: user_id
        return:json data
    ---------------------------------------------*/
	function deleteRateMatrix(getSequence){
		var res = confirm("Do You Really Want To Delete this Matrix?");
	        if (res == true) {
				$('#refresh_overlay').css("display","block");
			    $.ajax({
					url: _SERVICEPATHSpRequest,
					type: 'POST',
					data: "action=deleteSpecialRequest&sr_id="+getSequence,
					success: function(response) {
					    $('#refresh_overlay').css("display","none");
							getRateMatrixList();
						},
					error:function(){
							alert("Some Error");
							$('#refresh_overlay').css("display","none");
					}

				});
			}
			else
			{

		}

	}

    /*---------------------------------------------
      Function Name: checknegative()
      Input Parameter:check number is negative or not
      return:json data
    ---------------------------------------------*/	
	function checknegative() {

	    if ($('#percent_increase').val() < 0) {
	        $('#percent_increase').val(0) ;
	        $('#percent_increase').focus();
	            alert('Negative Values Not allowed');
	            return false;
	    }

	}

	/*call the set time out function*/
	setTimeout(function(){
				
    $("#apply_vehicle_type").multiselect('destroy');
		$('#apply_vehicle_type').multiselect({
			maxHeight: 200,
			buttonWidth: '155px',
			includeSelectAllOption: true
		});
    $("#value").multiselect('destroy');
		$('#value').multiselect({
		    maxHeight: 200,
			buttonWidth: '155px',
			includeSelectAllOption: true
		});
	$("#apply_sma").multiselect('destroy');
		$('#apply_sma').multiselect({
		    maxHeight: 200,
			buttonWidth: '155px',
			includeSelectAllOption: true
		});					
	}
	,900);
				