<?php
include_once 'session_auth.php';

$_page = 'cds';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Checkout Disclaimer Setup</title>
    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="assets/jvector-map/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="css/clndr.css" rel="stylesheet">
    <!--clock css-->
    <link href="assets/css3clock/css/style.css" rel="stylesheet">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="assets/morris-chart/morris.css">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
    .form-control {
        border: 1px solid #e2e2e4;
        box-shadow: none;
        color: #847F7F;
    }

    .table-action {
        text-align: right;
        font-size: 14px
    }

    .loadingGIF {
        margin-left: auto;
        margin-right: auto;
        text-align: center;
        width: 100%;
        padding-left: 30%;
        padding-top: 10%;
    }
    </style>
</head>

<body>

<!--header start-->
<?php include_once './global/header.php'; ?>
<!--header end-->

<!--sidebar start-->
<?php include_once './global/sideNav.php'; ?>
<!-- sidebar menu end-->


<section id="main-content">
        <section class="wrapper">
            <div class="panel-body">
                <div class="notify-form">
                    <!-- Q&R Notify eMail Section Starts here -->
                    <div class="col-sm-12" style="text-align:center; padding-bottom:20px;">
                        <br/>
                        <h4><strong>Payment/Checkout Disclaimer Setup</strong></h4>
                    </div>
                    <div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;">
                        <label>Credit Card Disclaimer:</label>
                    </div>
                    <div class="col-sm-8" style="text-align:left; margin-left:-5px; padding-bottom:10px;">
                        <!--  <div id="editor" name="CCardDisclamer" class="CCardDisclamer">
                        </div> -->

                        <input type="text" class=" form-control " id="CCardDisclamer" placeholder="Reservation Agreement/Terms of Service">
                       <!--  <textarea name="editor" id="editor" class="editor" rows="10" cols="80">
                        </textarea> -->
                    </div>
                    <!--  <div class="col-sm-8" style="text-align:left; margin-left:-5px; padding-bottom:10px;">
                        <input type="text" class=" form-control" id="CCardDisclamer" placeholder="Disclaimer on credit card section">
                    </div> -->
                    <div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;">
                        <label>Reservation Agreement:</label>
                    </div>
                    <div class="col-sm-8" style="text-align:left; margin-left:-5px; padding-bottom:10px;">

                          <textarea name="editor" id="editor" class="editor" rows="10" cols="80"></textarea>

                        <!-- <input type="text" class=" form-control " id="reservationAgreement" placeholder="Reservation Agreement/Terms of Service"> -->
                    </div>
                    <div class="col-sm-4" style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;">
                        <label>Cash Payment Disclaimer:</label>
                    </div>
                    <div class="col-sm-8" style="text-align:left; margin-left:-5px; padding-bottom:10px;">
                        <input type="text" class=" form-control " id="cashPaymentDisclaimer" placeholder="Cash Payment Disclaimer goes here">
                    </div>
                    <!-- Disclaimer Section Ends here -->
                    <div class="col-sm-12" style="text-align:center; padding:20px;">
                        <input type="button" value="Submit" class="btn btn-primary" id="updateDisclaimerSetup" />
                    </div>
                </div>
            </div>
        </section>
    </section>
    <!--Loading indicator-->
    <div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;" id="refresh_overlay;">
        <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%;"> </div>
        <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
            <div style="width:100%;display:table; height:100%;">
                <div style="width:100%; display:table-row">
                    <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                        <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px;">
                            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="loadingGIF">
                                    <img style="height:70px;width:70px;" src="images/loading.gif" alt="loading indicator">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Loading Indicator Ends-->
    <!-- Placed js at the end of the document so the pages load faster -->
    <!--Core js-->
    <script src="js/lib/jquery.js"></script>
    <script src="pageJs/dashboard.js"></script>
    <script src="pageJs/logout.js"></script>
    <script src="assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
    <script src="bs3/js/bootstrap.min.js"></script>
    <script src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/scrollTo/jquery.scrollTo.min.js"></script>
    <script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.js"></script>
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
    <script src="assets/skycons/skycons.js"></script>
    <script src="assets/jquery.scrollTo/jquery.scrollTo.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="assets/calendar/clndr.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <script src="assets/calendar/moment-2.2.1.js"></script>
    <script src="js/calendar/evnt.calendar.init.js"></script>
    <script src="assets/jvector-map/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="assets/jvector-map/jquery-jvectormap-us-lcc-en.js"></script>
    <script src="assets/gauge/gauge.js"></script>
    <!--clock init-->
    <script src="assets/css3clock/js/script.js"></script>
    <!--Easy Pie Chart-->
    <script src="assets/easypiechart/jquery.easypiechart.js"></script>
    <!--Sparkline Chart-->
    <script src="assets/sparkline/jquery.sparkline.js"></script>
    <!--Morris Chart-->
    <script src="assets/morris-chart/morris.js"></script>
    <script src="assets/morris-chart/raphael-min.js"></script>
    <!-- Text editor plugin url start here -->
    <script src="ckeditor/ckeditor.js"></script>
    <script src="ckeditor/samples/js/sample.js"></script>
    <!-- Text editor plugin url end here -->
    <!--jQuery Flot Chart-->
    <script src="assets/flot-chart/jquery.flot.js"></script>
    <script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script>
    <script src="assets/flot-chart/jquery.flot.resize.js"></script>
    <script src="assets/flot-chart/jquery.flot.pie.resize.js"></script>
    <script src="assets/flot-chart/jquery.flot.animator.min.js"></script>
    <script src="assets/flot-chart/jquery.flot.growraf.js"></script>
    <script src="js/custom-select/jquery.customSelect.min.js"></script>
    <!--common script init for all pages-->
    <script src="js/scripts.js"></script>
    <script src="pageJs/checkout_disclaimer.js"></script>
    <!--script for this page-->
    <script>
    initSample();
    </script>
</body>

</html>