$(document).ready(function () {
    $("#submit_button").click(function () {
        if ($("#pick_hourly").val() != '' && $("#drop_hourly").val() != '') {
            var dataPickUp = $("#pick_hourly").val();
            var dataDrop = $("#drop_hourly").val();
            var jurneyDate = $("#selected_date").val();
            var jurneyTime = $("#selected_time").val();

            var bookingInfo = {
                pickuplocation: dataPickUp,
                dropupaddress: dataDrop,
                jurneyDate: jurneyDate,
                jurneyTime: jurneyTime
            };
            localStorage.bookingInfo = JSON.stringify(bookingInfo);

            $.ajax({
                type: 'POST',
                url: '/QandR/frontend/phpfile/distance.php',
                data: {dataPickUp, dataDrop},
                success: function (result) {
                    
                    if (result <= 100) {
                        window.location.replace("//localhost/limo/frontend/select-vehicle.html");
                    } else {
                        alert("Distanse should not be more than 100 miles! Select another locations");
                    }
                },
                error: function (result) {
                    alert("whoops ! Something is wrong");
                }
            });
        } else {
            alert("Please provide pickup and drop off locations");
        }
        return false;
    });

});

