//var _SERVICEPATHServer="//crimsonbeans.com/cbprojects/airportpickup/SMA/phpfile/service_client.php";
//code for google map start===========
var _SERVICEPATHServer = _SERVICEPATHSERVICECLIENT;
//alert(_SERVICEPATHServer);

function check_map_visibility() {
    if(parseInt(localStorage.show_routing_map) === 1 ){
        return true;
    }

    return false
}

function window_size_changed(){
    //console.log('map is shown : ' , document.getElementById('mapShown').value);
    var mapShown = document.getElementById('mapShown').value;
    var mapType = document.getElementById('mapType').value;

    if(document.getElementById('mapShown').value == 'yes'){
        //console.log('size : ' , screen.width);
        if(screen.width >= 768){
            if(mapShown == 'yes' && mapType == 'desktop'){
                //console.log('keep the map desktop map')
            } else if(mapShown == 'yes' && mapType == 'mobile'){
                //console.log('hide the mobile map and show the desktop map');
                console.log('called 4');
                maoGenerate('yes');
            }
        }
        else if(screen.width < 768){
            if(mapShown == 'yes' && mapType == 'mobile'){
                //console.log('keep the mobile map')
            } else if(mapShown == 'yes' && mapType == 'desktop'){
                //console.log('hide the desktop map and show the mobile map');
                console.log('called 5');
                maoGenerate('yes');
            }
        }
    }
}

var autoIcon = '';
var Demo = {
    // HTML Nodes
    mapContainer: document.getElementById('map-container'),
    dirContainer: document.getElementById('dir-container'),
    fromInput: document.getElementById('from-input'),
    toInput: document.getElementById('to-input'),
    travelModeInput: document.getElementById('travel-mode-input'),
    unitInput: document.getElementById('unit-input'),

    // API Objects
    dirService: new google.maps.DirectionsService(),
    dirRenderer: new google.maps.DirectionsRenderer(),
    map: null,

    showDirections: function (dirResult, dirStatus) {
        if (dirStatus !== google.maps.DirectionsStatus.OK) {
            return;
        }

        // Show directions
        Demo.dirRenderer.setMap(Demo.map);
        Demo.dirRenderer.setPanel(Demo.dirContainer);
        Demo.dirRenderer.setDirections(dirResult);
    },

    getSelectedTravelMode: function () {
        var value = "DRIVING";
        return value;
    },

    getSelectedUnitSystem: function () {
        return 0;
    },
    getDirections: function (startpoint, endpoint , stop_points , initial = false) {

        if (stop_points.length > 0) {

            //console.log('with stop address');


            var waypts = [];
            $.each(stop_points, function(i, v){
                var addrs = v;
                $.ajax({
                    // url: "https://dclimolinks.com/QandRAuth/frontend/phpfile/get_latlong.php",
                    url: "https://maps.google.com/maps/api/geocode/json?address="+addrs+"&key=AIzaSyAaslQNU81hqdVhDCQt9ipOls8uAKdyL9g",
                    type: 'GET',
                    // data: 'address=' + addrs,
                    success: function (res) {
                        var lat = res.results[0].geometry.location.lat;
                        var lon = res.results[0].geometry.location.lng;

                        // var str_array = data.split(',');
                        var str_array = [lat,lon];

                        var stop = new google.maps.LatLng(str_array[0], str_array[1]);
                        waypts.push({
                            location: stop,
                            stopover: true
                        });

                        setTimeout(function () {
                            var fromStr = startpoint;
                            var toStr = endpoint;
                            var dirRequest = {
                                origin: fromStr,
                                destination: toStr,
                                waypoints: waypts,
                                optimizeWaypoints: true,
                                travelMode: Demo.getSelectedTravelMode(),
                                unitSystem: Demo.getSelectedUnitSystem(),
                                provideRouteAlternatives: true
                            };
                            Demo.dirService.route(dirRequest, Demo.showDirections);
                        }, 500);

                    }
                });
            });



        } else {
            //console.log('without stop address ' , arguments);


            setTimeout(function () {
                //console.log(' generate map for ',  startpoint);
                var fromStr = startpoint;
                var toStr = endpoint;
                var dirRequest = {
                    origin: fromStr,
                    destination: toStr,
                    optimizeWaypoints: true,
                    travelMode: Demo.getSelectedTravelMode(),
                    unitSystem: Demo.getSelectedUnitSystem(),
                    provideRouteAlternatives: true
                };
                if(!initial)
                Demo.dirService.route(dirRequest, Demo.showDirections);
            }, 500);


        }


    },

    init: function (startloc, endloc , stopAddress ,  initial = false) {
        if(startloc !== undefined && endloc !== undefined){
            var param = parseURLParams(window.location.href);

            //var latLng = new google.maps.LatLng(39.778182, -86.152300);
            var latLng = new google.maps.LatLng(37.65207, -85.57618);
            Demo.map = new google.maps.Map(Demo.mapContainer, {
                zoom: 9,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            // Show directions onload
            Demo.getDirections(startloc, endloc , stopAddress , initial);
        }
    }
};

function showinitialMap() {
    if(document.getElementById('mini_service') !== null){
        if(document.getElementById('mini_service').value === 'frontend/services-mini.php' ||
            document.getElementById('mini_service').value === 'frontend/services-vertical.php' ||
            document.getElementById('mini_service').value === 'frontend/services-horizontal-2-col.php'
        )
            return;
    }

    $('#map_icon').find('#yesmap input').prop("checked", true);
    $('#map_icon').find('#nomap input').prop('checked', false);
    $('.mapButton').addClass('mapHiddenTrigger');
    $('.hideMapButton').removeClass('mapHiddenTrigger');

    $('#mapShown').val('yes');

    if(screen.width >= 768)
        $("#location_map").show();
    else{
        $("#location_map").hide();
        $("#location_map_mobile").show();
        document.getElementById('mapType').value = 'mobile';
        Demo.mapContainer = document.getElementById('map-container-mobile');
    }

    Demo.init('Baltimore-Washington International Thurgood Marshall Airport(BWI)(Baltimore)', 'Augusta, GA, USA' , [] , true);


}

$('#services').change(function () {
    var serviceVal = $(this).val();

    if (serviceVal === 'AIRA' || serviceVal === 'AIRD') {
        autoIcon = '<i class="fa fa-plane"></i>';
    }
    else if (serviceVal === 'SEAA' || serviceVal === 'SEAD') {
        autoIcon = '<i class="fa fa-ship"></i>';
    }
    else if (serviceVal === 'FTS' || serviceVal === 'TTS') {
        autoIcon = '<i class="fa fa-subway"></i>';
    }
    if (serviceVal === 'HRLY') {

        /*$("#pick_hourly ").change(function () {
            var pickupLocation = $(this).val();

            // if (radioCheck == "yes") {
            //     alert("yes");
                // $('#stopLocation').change(function () {
                    var dropLocation  = this.val();
                //     $.ajax({
                //         url: "/QandR/frontend/phpfile/distance.php",
                //         type: 'POST',
                //         data: {},

                //     });
                // });


            }
        });*/
    }
});

function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1;
    var queryEnd = url.indexOf("#") + 1 || url.length + 1;
    var query = url.slice(queryStart, queryEnd - 1);

    if (query === url || query === "")
        return;

    var params = {};
    var nvPairs = query.replace(/\+/g, " ").split("&");

    for (var i = 0; i < nvPairs.length; i++) {
        var nv = nvPairs[i].split("=");
        var n = decodeURIComponent(nv[0]);
        var v = decodeURIComponent(nv[1]);
        if (!(n in params)) {
            params[n] = [];
        }
        params[n].push(nv.length === 2 ? v : null);
    }
    return params;
}

//google.maps.event.addDomListener(window, 'load', Demo.init);


//code for google map end============


//global variable declare for map ====

var option;
var startlocation;
var endlocation;

//global variable declare for map end ====


/*Enabel the form input value function start=================*/
enableFormField();

function enableFormField() {
    $("#searchRateForm :input").prop("disabled", true);
    $("#services").prop("disabled", false);

}

/*Enabel the form input value function end=================*/


function add_postal_code_with_address(geo_location_element, input_element) {
    google.maps.event.addListener(geo_location_element, 'place_changed', function () {
        var place = geo_location_element.getPlace();

        //console.log(place);

        input_element.val(place.formatted_address);
        var name_id = '#'+input_element.attr('id')+'_name'
        $(name_id).html(place.name);

        var save = localStorage.getItem('name_combination');
        //console.log(save);
        if(save === null || save === ''){
            localStorage.setItem('name_combination' , JSON.stringify([{
                id : name_id ,
                value : place.name
            }]))
        } else {
            try {
                var new_name_item = true;
                var current_name_combination = JSON.parse(localStorage.getItem('name_combination'));

                current_name_combination.forEach((item , i) => {
                    if(item.id == name_id){
                        new_name_item = false;
                        current_name_combination[i].value = place.name
                    }

                })

                if(new_name_item)
                current_name_combination.push({
                    id : name_id , value : place.name
                });

                //console.log('new items : ' , current_name_combination)
                localStorage.setItem('name_combination' , JSON.stringify(current_name_combination));
            }
            catch(e) {
            console.log('err : ' , e);
            }
        }

        var postal_code = '';
        $(place['address_components']).each(function (index) {  // find zip code
            if ($.inArray('postal_code', this['types']) != -1) {
                postal_code = this['long_name'];
            }
        });

        if (postal_code != '') {
            input_element.val(input_element.val() + ' ' + postal_code);
        }

    });
}

function close_trip_stops_modal() {



    var current_stop_Addressess = [] , all_filled = true;




    $('input[id^="stopLocation"]').each(function(index, value) {

        if($(this).val().trim() === '')
        {
            all_filled = false;
        }
        else
        {
            current_stop_Addressess.push($(this).val());
        }


    });

    if(all_filled)
    {
        $('#trip_stops_modal').modal('hide');
    }
    else {
        $('#stops_alert').show();
    }

    if(current_stop_Addressess.length !== 0)
    {
        $('#show_stops').show().html(current_stop_Addressess.join(','))
        localStorage.setItem('stops_for_mini_services' , current_stop_Addressess.join('@'));
    }
    else {
        $('#show_stops').hide();
        localStorage.setItem('stops_for_mini_services' , '');
    }

}

//stoplocation value on yes and no radi button ====

//$('input[type=radio][name=add_stop]').change(function () {

$('#add_stop').change(function () {

    if (this.value == 'yes') {
        if(document.getElementById('trip_stops_modal')){
            $('#trip_stops_modal').modal('show');
            $('#stops_alert').hide();
            document.getElementById('close_stops_modal').onclick=function () {
                close_trip_stops_modal()
            }
        }



        $('#stop_points').show();
        //$('.aadMore').html('<div class="stopLocationfield"><input type="text" id="stopLocation1" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>');
        $('.aadMore').append('<div class="cont1 col-sm-12 mt-15 stopLocationfield"><label class="short-address" id="stopLocation1_name"></label><div class="input-group"><input type="text" id="stopLocation1" class="form-control" name="mytext[]"/><span id="div-1" class="input-group-addon bg-primary remove_field">X</span></div></div>'); //add input box

        var stopLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation1'), options);
        add_postal_code_with_address(stopLocation, $('#stopLocation1'));


    } else if (this.value == 'no') {
        $('#stop_points').hide();
        $('.aadMore').html(' ');

        if(document.getElementById('trip_stops_modal')){
            $('#show_stops').hide();
            close_trip_stops_modal();
        }
    }
});







$('#add_stop1').change(function () {

    if (this.value == 'no') {

        $('#stop_points').hide();
        $('#show_stops').hide();
        $('.aadMore').html(' ');


    }
});

/*google api for usa location setting=================*/
var options = {};
/*service onchange function start=================*/
$('#services').change(function () {

    $('.short-address').html('');
    localStorage.removeItem('name_combination');
    $('#CutoffTimeMain').css("display", "none");
    $('#submit_button').css("display", "block");

    option = $(this).find('option:selected').val();
    if (option == 'AIRA') {

        $('#intFlt').show();
    } else {
        $('#intFlt').hide();
    }


    //reset the all form value=======

    $('.stopLocationfield').remove();

    $('#searchRateForm')[0].reset();
    $('#services').val(option);
    $('#stop_points').hide();
    //$("#location_map").hide();
    /*again reset the passenger and luggage quantity to zero start===*/

    $('#luggage_quantity').val("0");
    $('#total_passenger').val("1");
    $('#luggage_quantity_small').val("0");
    $('#luggage_quantity_medium').val("0");
    $('#luggage_quantity_large').val("0");


    /*again reset the passenger and luggage quantity to zero end===*/

    /*start to select the service an all work perform for selected service===*/



    if (option == 'PTP') {

        $("#searchRateForm :input").prop("disabled", false);
        //$('#map_loaction').show()
        $('#stop_point').show();
        $('#triphr').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_hourly_loc').hide();
        $('#pick_charter_loc').hide();
        $('#drop_charte_loc').hide();
        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();
        $('#from_train_pickup_location').hide();
        $('#from_train_dropof_location').hide();
        $('#to_train_pickup_location').hide();
        $('#to_train_dropof_location').hide();

        $('#drop_point_loc').show();
        $('#from_pont_to_point').show();


        /* validation applied start here */
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);

        $('#perpassenger_pickup_location').prop("required", false);
        $('#perpassenger_dropof_location').prop("required", false);

        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropof_location_input').prop("required", false);
        $('#to_train_dropof_location_input').prop("required", false);
        $('#from_train_dropof_location_input').prop("required", false);
        $('#pick_point').prop("required", true);
        $('#drop_point').prop("required", true);
        /* validation applied End here */


        $('#pick_hourly').hide();
        $('#from_seapick').hide();
        $('#from_seadrop').hide();
        $('#to_seapick').hide();
        $('#drop_hourly_loc').hide();
        $('#pick_from_seaport').hide();
        $('#to_seadrop').hide();
    } else if (option == 'FTS') {

        $("#searchRateForm :input").prop("disabled", false);
        //$('#map_loaction').show()
        $('#stop_point').show();
        $('#triphr').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_hourly_loc').hide();
        $('#pick_charter_loc').hide();
        $('#drop_charte_loc').hide();
        $('#drop_point_loc').hide();
        $('#from_pont_to_point').hide();
        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();


        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();


        $('#to_train_pickup_location').hide();
        $('#to_train_dropof_location').hide();

        $('#from_train_pickup_location').show();
        $('#from_train_dropof_location').show();


        /* validation applied start here */
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);


        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);

        $('#perpassenger_pickup_location').prop("required", false);
        $('#perpassenger_dropof_location').prop("required", false);
        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropof_location_input').prop("required", false);

        $('#from_train_pickup_location_input').prop("required", true);
        $('#from_train_dropof_location_input').prop("required", true);


        /* validation applied End here */


        $('#pick_hourly').hide();
        $('#from_seapick').hide();
        $('#from_seadrop').hide();
        $('#to_seapick').hide();
        $('#drop_hourly_loc').hide();
        $('#pick_from_seaport').hide();
        $('#to_seadrop').hide();
    } else if (option == 'TTS') {

        $("#searchRateForm :input").prop("disabled", false);
        //$('#map_loaction').show()
        $('#stop_point').show();
        $('#triphr').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_hourly_loc').hide();
        $('#pick_charter_loc').hide();
        $('#drop_charte_loc').hide();
        $('#drop_point_loc').hide();
        $('#from_pont_to_point').hide();
        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();


        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();


        $('#from_train_pickup_location').hide();
        $('#from_train_dropof_location').hide();


        $('#to_train_pickup_location').show();
        $('#to_train_dropof_location').show();


        /* validation applied start here */
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);


        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);

        $('#perpassenger_pickup_location').prop("required", false);
        $('#perpassenger_dropof_location').prop("required", false);


        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropof_location_input').prop("required", false);

        $('#to_train_pickup_location_input').prop("required", true);
        $('#to_train_dropof_location_input').prop("required", true);

        /* validation applied End here */

        $('#pick_hourly').hide();
        $('#from_seapick').hide();
        $('#from_seadrop').hide();
        $('#to_seapick').hide();
        $('#drop_hourly_loc').hide();
        $('#pick_from_seaport').hide();
        $('#to_seadrop').hide();
    } else if (option == 'PPS') {

        $("#searchRateForm :input").prop("disabled", false);
        //$('#map_loaction').show()
        $('#stop_point').hide();
        $('#triphr').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_hourly_loc').hide();
        $('#pick_charter_loc').hide();
        $('#drop_charte_loc').hide();
        $('#drop_point_loc').hide();

        $('#to_train_pickup_location').hide();
        $('#to_train_dropof_location').hide();

        $('#from_pont_to_point').hide();
        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();
        $('#from_train_pickup_location').hide();
        $('#from_train_dropof_location').hide();

        $('#from_perpassenger_point').show();
        $('#to_perpassenger_point').show();


        /* validation applied start here */
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);

        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropof_location_input').prop("required", false);
        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropof_location_input').prop("required", false);

        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);

        $('#perpassenger_pickup_location').prop("required", true);
        $('#perpassenger_dropof_location').prop("required", true);

        /* validation applied End here */


        $('#pick_hourly').hide();
        $('#from_seapick').hide();
        $('#from_seadrop').hide();
        $('#to_seapick').hide();
        $('#drop_hourly_loc').hide();
        $('#pick_from_seaport').hide();
        $('#to_seadrop').hide();
    } else if (option == 'CH') {
        $("#searchRateForm :input").prop("disabled", false);
        //$('#map_loaction').show();
        $('#stop_point').show();
        $('#triphr').show();
        $('#drop_hourly_loc').hide();
        $('#drop_charte_loc').show();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();


        $('#to_train_pickup_location').hide();
        $('#to_train_dropof_location').hide();

        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_hourly_loc').hide();
        $('#pick_charter_loc').show();
        $('#from_seapick').hide();
        $('#from_seadrop').hide();
        $('#to_seapick').hide();
        $('#to_seadrop').hide();
        $('#pick_from_seaport').hide();
        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();
        $('#from_train_pickup_location').hide();
        $('#from_train_dropof_location').hide();


        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#perpassenger_pickup_location').prop("required", false);
        $('#perpassenger_dropof_location').prop("required", false);

        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropof_location_input').prop("required", false);

        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropof_location_input').prop("required", false);

        $('#pick_charter').prop("required", true);
        $('#drop_charter').prop("required", true);


        /* validation End here */


    } else if (option == 'HRLY') {

        $("#searchRateForm :input").prop("disabled", false);
        //$('#map_loaction').show();
        $('#drop_charte_loc').hide();
        $('#stop_point').show();
        $('#pick_charter_loc').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#from_airport_drop').hide();

        $('#to_train_pickup_location').hide();
        $('#to_train_dropof_location').hide();


        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();

        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_from_seaport').hide();
        $('#triphr').show();
        $('#drop_hourly_loc').show();
        $('#pick_hourly_loc').show();
        $('#pick_hourly').show();
        $('#from_seapick').hide();
        $('#from_seadrop').hide();
        $('#to_seapick').hide();

        $('#to_seadrop').hide();
        $('#from_train_pickup_location').hide();
        $('#from_train_dropof_location').hide();

        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);

        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropof_location_input').prop("required", false);
        $('#perpassenger_pickup_location').prop("required", false);
        $('#perpassenger_dropof_location').prop("required", false);

        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);


        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropof_location_input').prop("required", false);

        $('#pick_hourly').prop("required", true);
        $('#drop_hourly').prop("required", true);


        /* validation End here */

    } else if (option == 'SEAA') {
        $("#searchRateForm :input").prop("disabled", false);
        //$('#map_loaction').show();
        //$('#location_map').show();
        $('#stop_point').show();
        $('#drop_charte_loc').hide();
        $('#pick_hourly_loc').hide();


        $('#to_train_pickup_location').hide();
        $('#to_train_dropof_location').hide();

        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_from_seaport').show();
        $('#from_seapick').show();
        $('#from_seadrop').show();
        $('#to_seapick').hide();
        $('#pick_charter_loc').hide();
        $('#drop_hourly_loc').hide();
        $('#to_seadrop').hide();
        $('#triphr').hide();
        $('#from_train_pickup_location').hide();
        $('#from_train_dropof_location').hide();


        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropof_location_input').prop("required", false);


        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropof_location_input').prop("required", false);

        $('#perpassenger_pickup_location').prop("required", false);
        $('#perpassenger_dropof_location').prop("required", false);
        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#pick_from_seaport').prop("required", true);
        $('#drop_from_seaport').prop("required", true);


        /* validation End here */

    } else if (option == 'SEAD') {

        $("#searchRateForm :input").prop("disabled", false);
        //$('#map_loaction').show();
        //$('#location_map').show();
        $('#stop_point').show();
        $('#drop_charte_loc').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();

        $('#to_train_pickup_location').hide();
        $('#to_train_dropof_location').hide();

        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();


        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_charter_loc').hide();
        $('#drop_hourly_loc').hide();
        $('#from_seapick').hide();
        $('#to_seapick').show();
        $('#pick_hourly_loc').hide();
        $('#to_seadrop').show();
        $('#from_seadrop').hide();
        $('#pick_from_seaport').hide();
        $('#triphr').hide();
        $('#from_train_pickup_location').hide();
        $('#from_train_dropof_location').hide();


        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropof_location_input').prop("required", false);

        $('#perpassenger_pickup_location').prop("required", false);
        $('#perpassenger_dropof_location').prop("required", false);

        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropof_location_input').prop("required", false);

        $('#to_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#pick_to_seaport').prop("required", true);
        $('#drop_to_seaport').prop("required", true);


        /* validation End here */

    } else if (option == 'AIRD') {
        $("#searchRateForm :input").prop("disabled", false);
        //$('#map_loaction').show();
        //$('#location_map').show();
        $('#stop_point').show();
        $('#drop_charte_loc').hide();
        $('#pick_hourly_loc').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#pick_charter_loc').hide();
        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();

        $('#to_train_pickup_location').hide();
        $('#to_train_dropof_location').hide();

        $('#drop_hourly_loc').hide();
        $('#triphr').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#from_seapick').hide();

        $('#pick_from_seaport').hide();
        $('from_seapick').hide();
        $('#from_seadrop').hide();
        $('#to_airport_pickup').show();
        $('#to_airport_drop').show();
        $('#to_seapick').hide();
        $('#to_seadrop').hide();

        $('#from_train_pickup_location').hide();
        $('#from_train_dropof_location').hide();

        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropof_location_input').prop("required", false);

        $('#perpassenger_pickup_location').prop("required", false);
        $('#perpassenger_dropof_location').prop("required", false);


        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropof_location_input').prop("required", false);


        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#to_airport_pickloc').prop("required", true);
        $('#to_airport_droploc').prop("required", true);


        /* validation End here */

    } else if (option == 'AIRA') {
        $("#searchRateForm :input").prop("disabled", false);

        $('#drop_charte_loc').hide();
        //$('#map_loaction').show();
        //$('#location_map').show();
        $('#stop_point').show();
        $('#drop_hourly_loc').hide();
        $('#pick_hourly_loc').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();

        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();

        $('#to_train_pickup_location').hide();
        $('#to_train_dropof_location').hide();


        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#to_seapick').hide();
        $('#to_seadrop').hide();
        $('#triphr').hide();
        $('#from_airport_drop').show();
        $('#from_seapick').hide();
        $('#from_seadrop').hide();
        $('#from_airport_pickup').show();
        $('#from_seadrop').hide();
        $('#pick_charter_loc').hide();
        $('#pick_from_seaport').hide();
        $('#from_train_pickup_location').hide();
        $('#from_train_dropof_location').hide();


        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);

        $('#perpassenger_pickup_location').prop("required", false);
        $('#perpassenger_dropof_location').prop("required", false);


        $('#from_train_pickup_location_input').prop("required", false);
        $('#from_train_dropof_location_input').prop("required", false);

        $('#to_train_pickup_location_input').prop("required", false);
        $('#to_train_dropof_location_input').prop("required", false);


        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#from_airport_pickloc').prop("required", true);
        $('#from_airport_droploc').prop("required", true);


        /* validation End here */

    } else {
        $("#map_icon").css('background-image', 'url(images/map-img.jpg)');
        $("#searchRateForm :input").prop("disabled", true);
        $("#services").prop("disabled", false);
        //$('#map_loaction').show();
        $('#drop_hourly_loc').hide();
        $('#stop_point').show();
        $('#drop_charte_loc').hide();
        $('#pick_hourly_loc').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#to_airport_pickup').show();
        $('#to_airport_drop').hide();
        $('#to_seapick').hide();
        $('#to_seadrop').hide();
        $('#triphr').hide();
        $('#from_airport_drop').show();
        $('#from_seadrop').hide();
        $('#from_airport_pickup').hide();
        $('#from_seadrop').hide();
        $('#pick_charter_loc').hide();
        $('#pick_from_seaport').hide();
        $('#from_perpassenger_point').hide();
        $('#to_perpassenger_point').hide();


    }


});

/*work ended for the selected service */

$(document).on('blur' , '[id^="stopLocation"]' , function(){
    console.log('called 1');
     maoGenerate(null);
})

$(document).on('click' , '.remove_field' , function(){
    console.log('called 2');
    maoGenerate(null);
})





/*function start for map start and end point of map location  */
function maoGenerate(value) {

    console.log('called');

    if(document.getElementById('mini_service') !== null){
        if(document.getElementById('mini_service').value === 'frontend/services-mini.php' ||
            document.getElementById('mini_service').value === 'frontend/services-vertical.php' ||
            document.getElementById('mini_service').value === 'frontend/services-horizontal-2-col.php'
            )
            return;
    }


    if(!check_map_visibility()){
        $('#mapShown').val('no');
        return;
    }

    if(value !== 'yes'){
        $('#mapShown').val('no');
    }

    if(screen.width >= 768){
        $('#map-container-mobile').hide();
        $("#location_map_mobile").hide();
        $(".hide-map-mobile").hide();
        $(".hide-map-desktop").show();



        $("#location_map").show();
        $('#map-container').show();

        document.getElementById('mapType').value = 'desktop';
        Demo.mapContainer = document.getElementById('map-container');
    } else {
        $('#map-container').hide();
        $("#location_map").hide();

        $(".hide-map-desktop").hide();
        $(".hide-map-mobile").show();

        $("#location_map_mobile").show();
        $('#map-container-mobile').show();

        document.getElementById('mapType').value = 'mobile';
        Demo.mapContainer = document.getElementById('map-container-mobile');
    }

    var mapButton = $('.mapButton');
    var hideMapButton = $('.hideMapButton');
    console.log(value);
    if(value === null){
        var a = parseInt(localStorage.show_routing_map);
        a === 1 ? value = 'yes' : value = 'no';
        if(value === 'yes'){
            two_column()
            $('#map_icon').find('#yesmap input').prop("checked", true);
            $('#map_icon').find('#nomap input').prop('checked', false);
            mapButton.addClass('mapHiddenTrigger');
            hideMapButton.removeClass('mapHiddenTrigger');
        } else {
            one_column();
            $('#map_icon').find('#yesmap input').prop('checked', false);
            $('#map_icon').find('#nomap input').prop('checked', true);
            mapButton.removeClass('mapHiddenTrigger');
            hideMapButton.addClass('mapHiddenTrigger');
        }
    } else if(value == 'no'){
        one_column();
    } else if(value == 'yes'){
        two_column();
    }
    setTimeout(function () {
        option = $('#services').val();
        if (value === 'yes') {

            mapButton.addClass('mapHiddenTrigger');
            hideMapButton.removeClass('mapHiddenTrigger');

            if (option === 'AIRA') {

                startlocation = $('#from_airport_pickloc').val();
                endlocation = $('#from_airport_droploc').val();
                // alert(startlocation);
                startlocation = startlocation.split('(');
                startlocation = startlocation[0];
                // startlocation=startlocation.replace(/)/g ,"");
                // startlocation=startlocation.replace(')','' );
                // alert(startlocation);


            }
            else if (option === 'AIRD') {

                endlocation = $('#to_airport_droploc').val();
                //endlocation=endlocation.split('(');
                //endlocation=endlocation[0];
                endlocation = endlocation.replace(/[(]/g, ',');
                endlocation = endlocation.replace(/[)]/g, '');


                startlocation = $('#to_airport_pickloc').val();


            }
            else if (option === 'PTP') {

                startlocation = $('#pick_point').val();
                endlocation = $('#drop_point').val();

            }
            else if (option === 'SEAA') {
                startlocation = $('#pick_from_seaport').val();
                startlocation = startlocation.replace(/[(]/g, ',');
                startlocation = startlocation.replace(/[)]/g, '');
                // startlocation=startlocation[0];
                endlocation = $('#drop_from_seaport').val();


            }
            else if (option === 'CH') {

                startlocation = $('#pick_charter').val();
                endlocation = $('#drop_charter').val();


            }
            else if (option === 'HRLY') {

                startlocation = $('#pick_hourly').val();
                endlocation = $('#drop_hourly').val();

            }
            else if (option === 'SEAD') {

                endlocation = $('#drop_to_seaport').val();
                //endlocation=endlocation.split('(');
                //var replace='/(/';
                endlocation = endlocation.replace(/[(]/g, ',');
                endlocation = endlocation.replace(/[)]/g, '');
                //endlocation=endlocation.split(',');
                //str.replace(/_/g, ' ');
                //endlocation=endlocation[0]+','+endlocation[2]+','+endlocation[1];
                //alert(endlocation);


                startlocation = $('#pick_to_seaport').val();


            }
            else if (option === 'FTS') {

                endlocation = $('#from_train_dropof_location_input').val();

                startlocation = $('#from_train_pickup_location_input').val();
                startlocation = startlocation.split('(');
                startlocation = startlocation[0];

            }
            else if (option === 'TTS') {

                endlocation = $('#to_train_dropof_location_input').val();
                //endlocation=endlocation.split('(');
                //endlocation=endlocation[0];
                endlocation = endlocation.replace(/[(]/g, ',');
                endlocation = endlocation.replace(/[)]/g, '');

                startlocation = $('#to_train_pickup_location_input').val();


            }
            else if (option === 'PPS') {
                startlocation = $('#perpassenger_pickup_location').val();
                endlocation = $('#perpassenger_dropof_location').val();
            }

            if (option === undefined || option === '') {
                //alert("Please Select service");
                //$('#no_map').prop("checked", true);
                showinitialMap();
            }
            else {
                if (startlocation === 'Select' || startlocation === '') {
                    //alert('Please Provide Start Point');
                    //$('#no_map').prop("checked", true);
                    showinitialMap();

                }
                else if (endlocation === 'Select' || endlocation === '') {
                    //alert('Please Provide End Point');
                    //$('#no_map').prop("checked", true);
                    showinitialMap();
                }
                else {

                    var stopAddress = []
                    $('input[name^="mytext"]').each(function(e) {
                       //console.log($(this).val());
                       if($(this).val() !== ''){
                           stopAddress.push($(this).val());
                       }
                    });
                    $('#mapShown').val('yes');
                    Demo.init(startlocation, endlocation , stopAddress)
                    //$("#map_icon").hide();
                    $("#location_map").show();

                    $("#map_icon").css('background-image', 'none');
                }
            }
        }
        if (value === 'no') {

            mapButton.removeClass('mapHiddenTrigger');
            hideMapButton.addClass('mapHiddenTrigger');
            //$("#map_icon").show();
            $("#location_map").hide();
            $("#location_map_mobile").hide();
            // $("#map_icon").css('background-image', 'url(images/map-img.jpg)');


        }
    }, 500);
}

$('input[type=radio][name=map]').change(function () {
    console.log('called 3');
    maoGenerate(this.value);
});



/*function start for map start and end point of map location  */

/*function start for google api for start and end point for map location */


/*

frontend/services.html - input html id
---------------------------------------

#stopLocation, #stopLocation1, #stopLocation2, ...... done ------ common for all ----

=== From Airport ==
#from_airport_pickloc
#from_airport_droploc ------------------------------- done

=== To Airport ===
#to_airport_pickloc --------------------------------- done
#to_airport_droploc

=== Point to Point ===
#pick_point ----------------------------------------- done
#drop_point ----------------------------------------- done

=== From Seaport ===
#pick_from_seaport
#drop_from_seaport ---------------------------------- done

=== To Seaport ===
#pick_to_seaport ------------------------------------ done
#drop_to_seaport

=== Charter/Hourly ===
#pick_hourly ---------------------------------------- done
#drop_hourly ---------------------------------------- done

=== Shuttle/Per Passenger ===
Dropdown--------------------- No need to do anything

=== From Train Station ===
#from_train_pickup_location_input
#from_train_dropof_location_input ------------------- done

=== To Train Station ===
#to_train_pickup_location_input --------------------- done
#to_train_dropof_location_input
 */

$(window).load(function () {

    var options = {};
    // var stopLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation'), options);
    // add_postal_code_with_address(stopLocation, $('#stopLocation'));

    //=====================
    var from_airport_droploc = new google.maps.places.Autocomplete(document.getElementById('from_airport_droploc'), options);
    add_postal_code_with_address(from_airport_droploc, $('#from_airport_droploc'));


    var from_train_dropof_location_input = new google.maps.places.Autocomplete(document.getElementById('from_train_dropof_location_input'), options);
    add_postal_code_with_address(from_train_dropof_location_input, $('#from_train_dropof_location_input'));

    var to_train_pickup_location_input = new google.maps.places.Autocomplete(document.getElementById('to_train_pickup_location_input'), options);
    add_postal_code_with_address(to_train_pickup_location_input, $('#to_train_pickup_location_input'));

    google.maps.event.addListener(to_train_pickup_location_input, 'place_changed', function () {
        var getDate = $('#selected_date').val();
        var getTime = $('#selected_time').val();
        var getLocation = $('#to_train_pickup_location_input').val();
        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
            checkBlackOutDate(getDate, getTime, getLocation);
        }

    });

    var toAirportPickupLocation = new google.maps.places.Autocomplete(document.getElementById('to_airport_pickloc'), options);
    add_postal_code_with_address(toAirportPickupLocation, $('#to_airport_pickloc'));
    google.maps.event.addListener(toAirportPickupLocation, 'place_changed', function () {
        var getDate = $('#selected_date').val();
        var getTime = $('#selected_time').val();
        var getLocation = $('#to_airport_pickloc').val();
        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
            checkBlackOutDate(getDate, getTime, getLocation);
        }
    });


    var drop_from_seaport = new google.maps.places.Autocomplete(document.getElementById('drop_from_seaport'), options);
    add_postal_code_with_address(drop_from_seaport, $('#drop_from_seaport'));

    var to_seaport_pick_up_location = new google.maps.places.Autocomplete(document.getElementById('pick_to_seaport'), options);
    add_postal_code_with_address(to_seaport_pick_up_location, $('#pick_to_seaport'));
    google.maps.event.addListener(to_seaport_pick_up_location, 'place_changed', function () {
        var getDate = $('#selected_date').val();
        var getTime = $('#selected_time').val();
        var getLocation = $('#pick_to_seaport').val();
        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
            checkBlackOutDate(getDate, getTime, getLocation);
        }


    });


    // var pick_charter_pickup_location = new google.maps.places.Autocomplete(document.getElementById('pick_charter'), options);
    //
    // google.maps.event.addListener(pick_charter_pickup_location, 'place_changed', function () {
    //     var getDate = $('#selected_date').val();
    //     var getTime = $('#selected_time').val();
    //     var getLocation = $('#pick_charter').val();
    //
    //     if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
    //         checkBlackOutDate(getDate, getTime, getLocation);
    //
    //     }
    //
    // });
    /*  point to point pickup location google api*/

    var places2 = new google.maps.places.Autocomplete(document.getElementById('pick_point'), options);
    add_postal_code_with_address(places2, $('#pick_point'));
    google.maps.event.addListener(places2, 'place_changed', function () {
        var place = places2.getPlace();


        if (document.getElementById('pick_point').value.toLowerCase().indexOf("airport") >= 0) {
            $('#airportDisclaimerMsg').html("please select the Airport Arrival service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();

        } else if (document.getElementById('pick_point').value.toLowerCase().indexOf("port") >= 0) {
            $('#airportDisclaimerMsg').html("please select the From Seaport service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();
        } else {
            $('#commanDisclaimerMsg').hide();
            var getDate = $('#selected_date').val();
            var getTime = $('#selected_time').val();
            var getLocation = $('#pick_point').val();

            if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
                checkBlackOutDate(getDate, getTime, getLocation);
            }
        }
    });

    var point_to_point_drop_location = new google.maps.places.Autocomplete(document.getElementById('drop_point'), options);
    add_postal_code_with_address(point_to_point_drop_location, $('#drop_point'));

    google.maps.event.addListener(point_to_point_drop_location, 'place_changed', function () {
        var place = point_to_point_drop_location.getPlace();
        if (document.getElementById('drop_point').value.toLowerCase().indexOf("airport") >= 0) {
            $('#airportDisclaimerMsg').html("please select the Airport Departure service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();
        } else if (document.getElementById('drop_point').value.toLowerCase().indexOf("port") >= 0) {
            $('#airportDisclaimerMsg').html("please select the To Seaport service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();
        } else {
            $('#commanDisclaimerMsg').hide();
        }
    });
    //var places = new google.maps.places.Autocomplete(document.getElementById('stoplocation'), options);
    var hourly_pickUp_location = new google.maps.places.Autocomplete(document.getElementById('pick_hourly'), options);
    add_postal_code_with_address(hourly_pickUp_location, $('#pick_hourly'));


    google.maps.event.addListener(hourly_pickUp_location, 'place_changed', function () {
        var getDate = $('#selected_date').val();
        var getTime = $('#selected_time').val();
        var getLocation = $('#pick_hourly').val();
        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
            checkHourlyBlackOutDate(getDate, getTime, getLocation);
        }


    });


    var drop_hourly = new google.maps.places.Autocomplete(document.getElementById('drop_hourly'), options);
    add_postal_code_with_address(drop_hourly, $('#drop_hourly'));

    var places = new google.maps.places.Autocomplete(document.getElementById('drop_charter'), options);
});

/*function end for google api for start and end point for map location */

/*function start for getting the service list from limo any where */
getServiceTypes();


function checkHourlyBlackOutDate(getDate, getTime, getLocation) {
    /* edited by infograins

        $('#CutoffTimeMain').hide();
        // $('#CutoffTimeMain').html('');

        $('#refresh_overlay').show()
        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        if (typeof (getLocalStorageValue) == "string")
        {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
            var getJson = {"action": "isHrlyBlackOutDate", "pick_date": getDate, "pick_time": getTime, "pick_up_location": getLocation, "user_id": getLocalStorageValue[0].user_id, "limoanywhereKey": getLocalStorageValue[0].limo_any_where_api_key, "limoanywhereID": getLocalStorageValue[0].limo_any_where_api_id};
            $.ajax({

                url: _SERVICEPATHServer,

                type: 'POST',

                data: getJson,

                success: function (response) {


                    var responseObj = response;
                    if (typeof (response) == "string")
                    {

                        responseObj = JSON.parse(response);
                        $('#refresh_overlay').hide()
                        if (responseObj.code == 1007)
                        {
                            var isCheckAllBlackOut = 'Value Found';
                            var isAllVehicleExist = 0;
                            var totalVehicleLoop = 0;
                            var totalVehicleNotExit = 0;

                            var allVehicleArray = [];
                            var vichleExists = [];
                            var disclamer_message = 'no';
                            var hourlyInfoArray = [];
                            var ischeckHourly = "no";
                            $.each(responseObj.data, function (index, result) {
                                totalVehicleLoop = parseInt(index) + 1;

                                //alert(result.blackOutDateInformation[index].vehicle_code);
                                //vichleExists.push({"vehicle_name":result.blackOutDateInformation});
                                if (typeof (disclamer_message = result.blackOutDateInformation) != "undefined")
                                {
                                    disclamer_message = result.blackOutDateInformation[0].msg
                                }
                                if (result.vehicle == "No Value Found")
                                {
                                    totalVehicleNotExit++;
                                    allVehicleArray.push({"vehicle_info": result.vehicleNotExist, "black_out_info": disclamer_message});
                                    vichleExists.push({"vehicle_code": '0'});
                                } else
                                {
                                    //alert(result.vehicle);

                                    vichleExists.push({"vehicle_code": result.blackOutDateInformation[0].vehicle_code});
                                    isAllVehicleExist++;
                                    disclamer_message = result.blackOutDateInformation[0].msg;
                                    if (result.hourlyInfo != "Hourly Not Found")
                                    {
                                        // alert(result.hourlyInfo[0].cut_off_time);
                                        var cutoffTIme = parseInt(result.hourlyInfo[0].cut_off_time);
                                        var totalDate = 0;
                                        var totalTime = 0;
                                        totalDate = parseInt(cutoffTIme / 24);
                                        totalTime = parseInt(cutoffTIme % 24);
                                        var passengerDate = new Date($('#selected_date').val() + " " + $('#selected_time').val());
                                        var CompareDate = new Date();
                                        CompareDate.setDate(CompareDate.getDate() + totalDate, CompareDate.getMinutes() + (totalTime * 60));
                                        if (passengerDate >= CompareDate)
                                        {
                                            $('#submit_button').prop("disabled", false);
                                            $('#CutoffTimeMain').hide();
                                        } else
                                        {
                                            $('#CutoffTimeMain').show();


                                            $('#CutoffTimeMainMessage').html("Please reservation is possible after " + CompareDate);

                                            setTimeout(function () {

                                                $('#submit_button').prop('disabled', true);

                                            }, 1000);

                                        }

                                        hourlyInfoArray.push(result.hourlyInfo);
                                        ischeckHourly = "yes";
                                    }
                                }
                            });
                            if (totalVehicleLoop == isAllVehicleExist)
                            {


                                if (ischeckHourly != "yes")
                                {
                                    alert(disclamer_message);
                                    localStorage.removeItem("blackOutDateInformation");
                                    localStorage.removeItem('vehicle_name');

                                    $('#submit_button').prop("disabled", true);
                                } else
                                {

                                    var getJsonFullBlackOut = {"allVehicle": allVehicleArray, "checkBlackOutDate": "blackout", "ischeckHourly": ischeckHourly, "hourlyInfoArray": hourlyInfoArray};
                                    getJsonFullBlackOut = JSON.stringify(getJsonFullBlackOut)

                                    var getJsonVehicleName = {"vehicleExists": vichleExists};
                                    getJsonVehicleName = JSON.stringify(getJsonVehicleName);
                                    localStorage.setItem("vehicle_name", getJsonVehicleName);
                                    localStorage.setItem("blackOutDateInformation", getJsonFullBlackOut);
                                    $('#submit_button').prop("disabled", false);
                                }
                            } else if (isAllVehicleExist > 0 || ischeckHourly == "yes")
                            {
                                var getJsonFullBlackOut = {"allVehicle": allVehicleArray, "checkBlackOutDate": "blackout", "ischeckHourly": ischeckHourly, "hourlyInfoArray": hourlyInfoArray};
                                getJsonFullBlackOut = JSON.stringify(getJsonFullBlackOut)
                                var getJsonVehicleName = {"vehicleExists": vichleExists};
                                getJsonVehicleName = JSON.stringify(getJsonVehicleName);
                                localStorage.setItem("vehicle_name", getJsonVehicleName);
                                localStorage.setItem("blackOutDateInformation", getJsonFullBlackOut);
                                $('#submit_button').prop("disabled", false);
                            }
                            if (totalVehicleLoop == totalVehicleNotExit)
                            {

                                localStorage.removeItem("blackOutDateInformation");
                                localStorage.removeItem('vehicle_name');
                                $('#submit_button').prop("disabled", false);

                            }




                        } else
                        {

                            localStorage.removeItem("blackOutDateInformation");
                            localStorage.removeItem('vehicle_name');
                            $('#submit_button').prop("disabled", false);
                        }


                    }



                }
            });


        }*/


    $('#refresh_overlay').show()

    $('#CutoffTimeMain').hide();


    $('#CutoffTimeMainMessage').html("");
    var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
    if (typeof (getLocalStorageValue) == "string") {
        getLocalStorageValue = JSON.parse(getLocalStorageValue);
        var getJson = {
            "action": "isBlackOutDate",
            "pick_date": getDate,
            "pick_time": getTime,
            "pick_up_location": getLocation,
            "user_id": getLocalStorageValue[0].user_id,
            "limoanywhereKey": getLocalStorageValue[0].limo_any_where_api_key,
            "limoanywhereID": getLocalStorageValue[0].limo_any_where_api_id
        };
        $.ajax({

            url: _SERVICEPATHServer,

            type: 'POST',

            data: getJson,

            success: function (response) {


                var responseObj = response;
                if (typeof (response) == "string") {

                    responseObj = JSON.parse(response);

                    $('#refresh_overlay').hide()

                    if (responseObj.code == 1007) {
                        var isCheckAllBlackOut = 'Value Found';
                        var isAllVehicleExist = 0;
                        var totalVehicleLoop = 0;
                        var totalVehicleNotExit = 0;

                        var allVehicleArray = [];
                        var disclamer_message = 'no';
                        $.each(responseObj.data, function (index, result) {


                            totalVehicleLoop = parseInt(index) + 1;
                            // alert(disclamer_message=result.blackOutDateInformation);
                            if (typeof (disclamer_message = result.blackOutDateInformation) != "undefined") {
                                disclamer_message = result.blackOutDateInformation[0].msg
                                // alert(result.blackOutDateInformation[0].msg);
                            }


                            if (result.vehicle == "No Value Found") {
                                // isCheckAllBlackOut='No Value Found';
                                totalVehicleNotExit++;

                                allVehicleArray.push({
                                    "vehicle_info": result.vehicleNotExist,
                                    "black_out_info": disclamer_message
                                });


                            } else {

                                isAllVehicleExist++;
                                disclamer_message = result.blackOutDateInformation[0].msg;
                                // alert(disclamer_message);

                                // allVehicleArray.push({"vehicle_info":result.vehicle,"black_out_info":result.blackOutDateInformation});

                            }


                        });
                        if (totalVehicleLoop == isAllVehicleExist) {

                            //alert(disclamer_message);
                            localStorage.removeItem("blackOutDateInformation");

                            $('#submit_button').prop("disabled", false);

                        } else if (isAllVehicleExist > 0) {
                            var getJsonFullBlackOut = {"allVehicle": allVehicleArray, "checkBlackOutDate": "blackout"};

                            getJsonFullBlackOut = JSON.stringify(getJsonFullBlackOut)
                            // alert("some vehicle exist and this is black out date");
                            localStorage.setItem("blackOutDateInformation", getJsonFullBlackOut);
                            $('#submit_button').prop("disabled", false);


                        }
                        if (totalVehicleLoop == totalVehicleNotExit) {

                            // alert("No Black Out date");
                            localStorage.removeItem("blackOutDateInformation");
                            $('#submit_button').prop("disabled", false);

                        }


                    } else {

                        localStorage.removeItem("blackOutDateInformation");
                        $('#submit_button').prop("disabled", false);
                    }


                }


            }
        });


    }


}

function checkBlackOutDate(getDate, getTime, getLocation) {
    $('#refresh_overlay').show()

    $('#CutoffTimeMain').hide();


    $('#CutoffTimeMainMessage').html("");
    var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
    if (typeof (getLocalStorageValue) == "string") {
        getLocalStorageValue = JSON.parse(getLocalStorageValue);
        var getJson = {
            "action": "isBlackOutDate",
            "service_type": $('#services').val(),
            "pick_date": getDate,
            "pick_time": getTime,
            "pick_up_location": getLocation,
            "user_id": getLocalStorageValue[0].user_id,
            "limoanywhereKey": getLocalStorageValue[0].limo_any_where_api_key,
            "limoanywhereID": getLocalStorageValue[0].limo_any_where_api_id
        };
        $.ajax({

            url: _SERVICEPATHServer,

            type: 'POST',

            data: getJson,

            success: function (response) {


                var responseObj = response;
                if (typeof (response) == "string") {

                    responseObj = JSON.parse(response);

                    $('#refresh_overlay').hide()

                    if (responseObj.code == 1007) {
                        var isCheckAllBlackOut = 'Value Found';
                        var isAllVehicleExist = 0;
                        var totalVehicleLoop = 0;
                        var totalVehicleNotExit = 0;

                        var allVehicleArray = [];
                        var disclamer_message = 'no';
                        // return 0;
                        $.each(responseObj.data, function (index, result) {


                            totalVehicleLoop = parseInt(index) + 1;
                            // alert(disclamer_message=result.blackOutDateInformation);
                            if (typeof (disclamer_message = result.blackOutDateInformation) != "undefined") {
                                disclamer_message = result.blackOutDateInformation[0].msg
                                // alert(result.blackOutDateInformation[0].msg);  
                            }


                            if (result.vehicle == "No Value Found") {
                                // isCheckAllBlackOut='No Value Found';
                                totalVehicleNotExit++;

                                allVehicleArray.push({
                                    "vehicle_info": result.vehicleNotExist,
                                    "black_out_info": disclamer_message
                                });


                            } else {

                                isAllVehicleExist++;
                                disclamer_message = result.blackOutDateInformation[0].msg;
                                // alert(disclamer_message);

                                // allVehicleArray.push({"vehicle_info":result.vehicle,"black_out_info":result.blackOutDateInformation});

                            }


                        });
                        if (totalVehicleLoop == isAllVehicleExist) {

                            alert(disclamer_message);
                            localStorage.removeItem("blackOutDateInformation");

                            $('#submit_button').prop("disabled", true);

                        } else if (isAllVehicleExist > 0) {
                            var getJsonFullBlackOut = {"allVehicle": allVehicleArray, "checkBlackOutDate": "blackout"};

                            getJsonFullBlackOut = JSON.stringify(getJsonFullBlackOut)
                            // alert("some vehicle exist and this is black out date");
                            localStorage.setItem("blackOutDateInformation", getJsonFullBlackOut);
                            $('#submit_button').prop("disabled", false);


                        }
                        if (totalVehicleLoop == totalVehicleNotExit) {

                            // alert("No Black Out date");
                            localStorage.removeItem("blackOutDateInformation");
                            $('#submit_button').prop("disabled", false);

                        }


                    } else {

                        localStorage.removeItem("blackOutDateInformation");
                        $('#submit_button').prop("disabled", false);
                    }


                }


            }
        });


    }

}


function getServiceTypes() {
    var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
    var user_id = 25;
    if (typeof (getLocalStorageValue) == "string") {
        getLocalStorageValue = JSON.parse(getLocalStorageValue);
        user_id = getLocalStorageValue[0].user_id;
    }
    var serviceTypeData = [];
    $.ajax({
        url: WEBSITE_URL+'/SMA/phpfile/service_type.php',
        // url: '//localhost/mediusware/limo/SMA/phpfile/service_type.php',
        type: 'post',
        data: 'action=GetServiceTypes&user_id=' + user_id,
        dataType: 'json',
        success: function (data) {
            if (data.ResponseText == 'OK') {

                var innerText = document.getElementById('mini_service') == null ? "Select" : 'Select Service';
                var ResponseHtml = '<option value="">'+innerText+'</option>';
                $.each(data.ServiceTypes.ServiceType, function (index, result) {
                    ResponseHtml += "<option value='" + result.SvcTypeCode + "'>" + result.SvcTypeDescription + "</option>";
                });


                $('#services').html(ResponseHtml);

            }
        }
    });
}


var getinfo = {
    _Serverpath: "phpfile/client.php",

    getAirportName: function (inputBoxFieldValue) {

        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        var user_id = 25;
        if (typeof (getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
            user_id = getLocalStorageValue[0].user_id;
        }
        var inputBoxFieldValue = inputBoxFieldValue;
        if (typeof (getUserId) == "string") {
            getUserId = JSON.parse(getUserId);

        }
        var fd = new FormData();

        fd.append("action", "getAirportName");
        fd.append("user_id", user_id);
        fd.append("inputValue", inputBoxFieldValue);
        //fd.append("user_id",getUserId.user_id);

        $.ajax({
            url: getinfo._Serverpath,
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function (result) {

            var locationdata1 = [];
            var response = JSON.parse(result);

            var ResponseHtml = '<option value="">Select</option>';
            for (i = 0; i < response.data.length; i++) {

                locationdata1.push({
                    "value": response.data[i].is_int_flight + "@@" + response.data[i].insidemeet_msg + "@@" + response.data[i].curbside_msg + "@@" + response.data[i].int_flight_msg + "@@" + response.data[i].is_insidemeet_greet + "@@" + response.data[i].is_curbside + "@@" + response.data[i].fhv_content,
                    "label": response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')'
                });
                ResponseHtml += "<option value='" + response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')' + "' seq='" + response.data[i].is_int_flight + "' insidemeet_msg='" + response.data[i].insidemeet_msg + "' curbside_msg='" + response.data[i].curbside_msg + "' int_flight_msg='" + response.data[i].int_flight_msg + "' meatandGreet='" + response.data[i].is_insidemeet_greet + "' curbsideSeq='" + response.data[i].is_curbside + "'>" + response.data[i].airport_name + "</option>";

            }

            var airportLocation = locationdata1;

            setTimeout(function () {
                /* from airport service autocomplete start here */
                $("#from_airport_pickloc").autocomplete({
                    source: airportLocation,
                    delay: 0,
                    select: function (event, ui) {
                        event.preventDefault()
                        $(this).val(ui.item.label);


                        var getDate = $('#selected_date').val();
                        var getTime = $('#selected_time').val();
                        var getLocation = ui.item.label;

                        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {

                            checkBlackOutDate(getDate, getTime, getLocation);
                        }
                        var selectedValue = ui.item.value;
                        var selectedAirportValue = selectedValue.split('@@');
                        var option = selectedAirportValue[0];
                        var meatandGreet = selectedAirportValue[4];
                        var curbsideSeq = selectedAirportValue[5];
                        var insidemeet_msg = selectedAirportValue[1];
                        var curbside_msg = selectedAirportValue[2];
                        var int_flight_msg = selectedAirportValue[3];
                        var optional_greeter = selectedAirportValue[6];

                        localStorage.setItem('optional_greeter_content', optional_greeter);

                        if (meatandGreet != 0 || curbsideSeq != '0' || option != '0') {


                            $('.holdername').show();
                        } else {
                            $('.holdername').hide();
                        }

                        if (meatandGreet != '0') {
                            //var msg ="this is the content";
                            $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                            //var popover = $('#meetAndGreet').data('bs.popover');
                            // popover.a.content = "YOUR NEW TEXT";
                            //$(".popover-content")[0].innerHTML = 'something else';
                            $('#meetAndGreet').show();

                            $('#meetGreetUpdateService').prop("checked", true);
                        } else {

                            $('#meetAndGreet').hide();
                        }
                        if (curbsideSeq != '0') {
                            $('#curbside_disclaimer').attr("data-content", curbside_msg);
                            $('#curbSide').show();
                            $('#curbsideUpdateService').prop("checked", true);

                        } else {
                            $('#curbSide').hide();


                        }


                        if (option != '0') {

                            $('#international_disclaimer').attr("data-content", int_flight_msg);
                            $('.interNationFlight').show();
                        } else {
                            $('.interNationFlight').hide();


                        }

                    },
                    focus: function () {
                        //$(this).val('asdfasdfasd');
                    }
                }).autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append(autoIcon + " &nbsp; " + item.label)
                        .appendTo(ul);
                };
                /* from airport service autocomplete end here */

                $("#to_airport_droploc").autocomplete({
                    source: airportLocation,
                    delay: 0,
                    select: function (event, ui) {
                        event.preventDefault()
                        $(this).val(ui.item.label);
                        var selectedValue = ui.item.value;
                        var selectedAirportValue = selectedValue.split('@@');
                        var option = selectedAirportValue[0];
                        var meatandGreet = selectedAirportValue[4];
                        var curbsideSeq = selectedAirportValue[5];
                        var insidemeet_msg = selectedAirportValue[1];
                        var curbside_msg = selectedAirportValue[2];
                        var int_flight_msg = selectedAirportValue[3];

                        if (meatandGreet != 0 || curbsideSeq != '0' || option != '0') {

                            $('.holdername').show();
                        } else {

                            $('.holdername').hide();
                        }


                        if (meatandGreet != '0') {
                            //var msg ="this is the content";
                            $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                            //var popover = $('#meetAndGreet').data('bs.popover');
                            // popover.a.content = "YOUR NEW TEXT";
                            //$(".popover-content")[0].innerHTML = 'something else';
                            $('#meetAndGreet').show();
                        } else {

                            $('#meetAndGreet').hide();
                        }
                        if (curbsideSeq != '0') {
                            $('#curbside_disclaimer').attr("data-content", curbside_msg);
                            $('#curbSide').show();
                        } else {
                            $('#curbSide').hide();

                        }
                        if (option != '0') {

                            $('#international_disclaimer').attr("data-content", int_flight_msg);
                            $('.interNationFlight').show();
                        } else {
                            $('.interNationFlight').hide();


                        }

                    },
                    focus: function () {



                        //$(this).val('asdfasdfasd');
                    }
                }).autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append(autoIcon + " &nbsp; " + item.label)
                        .appendTo(ul);
                };


            }, 1000);

        });

    },

    getTrainInformationName: function () {


        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        var user_id = 25;
        if (typeof (getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
            user_id = getLocalStorageValue[0].user_id;
        }


        // var inputBoxFieldValue =inputBoxFieldValue;
        if (typeof (getUserId) == "string") {
            getUserId = JSON.parse(getUserId);

        }
        var fd = new FormData();
        fd.append("action", "getTrainName");
        // fd.append("inputValue",inputBoxFieldValue);
        fd.append("user_id", user_id);

        $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function (result) {

            var locationdata1 = [];
            var response = JSON.parse(result);


            var ResponseHtml = '<option value="">Select</option>';

            for (i = 0; i < response.data.length; i++) {

                locationdata1.push({
                    "value": response.data[i].train_name + '(' + response.data[i].train_code + ')' + '(' + response.data[i].city_name + ')',
                    "label": response.data[i].train_name + '(' + response.data[i].train_code + ')' + '(' + response.data[i].city_name + ')'
                });
                ResponseHtml += "<option value='" + response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')' + "' seq='" + response.data[i].is_int_flight + "' insidemeet_msg='" + response.data[i].insidemeet_msg + "' curbside_msg='" + response.data[i].curbside_msg + "' int_flight_msg='" + response.data[i].int_flight_msg + "' meatandGreet='" + response.data[i].is_insidemeet_greet + "' curbsideSeq='" + response.data[i].is_curbside + "'>" + response.data[i].airport_name + "</option>";

            }
            var airportLocation = locationdata1;
            setTimeout(function () {


                /* from airport service autocomplete start here */


                $("#from_train_pickup_location_input").autocomplete({
                    source: airportLocation,
                    delay: 0,
                    select: function (event, ui) {
                        event.preventDefault()
                        $(this).val(ui.item.label);


                        var getDate = $('#selected_date').val();
                        var getTime = $('#selected_time').val();
                        var getLocation = ui.item.label;

                        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {

                            checkBlackOutDate(getDate, getTime, getLocation);
                        }


                    },
                    focus: function () {



                        //$(this).val('asdfasdfasd');
                    }
                }).autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append(autoIcon + " &nbsp; " + item.label)
                        .appendTo(ul);
                };
                /* from airport service autocomplete end here */

                $("#to_train_dropof_location_input").autocomplete({
                    source: airportLocation,
                    delay: 0,
                    select: function (event, ui) {


                    },
                    focus: function () {



                        //$(this).val('asdfasdfasd');
                    }


                }).autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append(autoIcon + " &nbsp; " + item.label)
                        .appendTo(ul);
                };


            }, 1000);


        });


    },

    getPerpassengerName: function () {

        var getUserId = window.localStorage.getItem('limoanyWhereVerification');

        var user_id = 25;
        if (typeof (getUserId) == "string") {

            getUserId = JSON.parse(getUserId);
            user_id = getUserId[0].user_id

        }


        var fd = new FormData();
        fd.append("action", "getPerpassengerName");
        fd.append("user_id", user_id);

        $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function (result) {
            var resultObj = result;

            var pickup_locationdata = '<option value="">Select Location</option>';
            var dropof_locationdata = '<option value="">Select Location</option>';

            if (typeof (result) == "string") {
                resultObj = JSON.parse(result);


                if (resultObj.code == 1007) {

                    $.each(resultObj.data['pickupLocation'], function (index, result) {
                        pickup_locationdata += "<option value='" + result.pickup_location + "'>" + result.pickup_location + "</option>";

                    });

                    $.each(resultObj.data['dropLocation'], function (index, result) {
                        dropof_locationdata += "<option value='" + result.drop_off_location + "'>" + result.drop_off_location + "</option>";

                    });
                    /*   code start here */
                    $('#perpassenger_pickup_location').html(pickup_locationdata);
                    $('#perpassenger_dropof_location').html(dropof_locationdata);
                    $('#perpassenger_pickup_location').on("change", function () {
                        var getLocation = $(this).val();
                        var getDate = $('#selected_date').val();
                        var getTime = $('#selected_time').val();
                        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
                            checkBlackOutDate(getDate, getTime, getLocation);
                        }
                    });
                    /* code end here*/


                }


            }


        });


    },

    /*function end for getting the airport list */

    /*function start for getting the seaportname list */
    getSeaPortName: function (inputBoxFieldValue) {

        //var getUserId=window.localStorage.getItem('companyInfo');
//var getinputTextValue = $('#pick_from_seaport').val();


        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        var user_id = 25;
        if (typeof (getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
            user_id = getLocalStorageValue[0].user_id;
        }


        var locationdata1 = [];
        var inputBoxFieldValue = inputBoxFieldValue;

        /*if(typeof(getUserId)=="string")
         {   
         
         getUserId=JSON.parse(getUserId);
         
         }
         */
        var fd = new FormData();
        fd.append("action", "getSeaPortName");

        fd.append("inputValue", inputBoxFieldValue);
        fd.append("user_id", user_id)

        $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function (result) {
            var response = JSON.parse(result);
            var ResponseHtml = '<option value="">Select</option>';
            for (i = 0; i < response.data.length; i++) {
                locationdata1.push({
                    "value": response.data[i].inside_meet_text + "@@" + response.data[i].curbside_text + "@@" + response.data[i].is_inside_meet_check + "@@" + response.data[i].is_curbside_check,
                    "label": response.data[i].seaport_name + '(' + response.data[i].seaport_code + ')' + '(' + response.data[i].city_name + ')'
                });
                ResponseHtml += "<option value='" + response.data[i].seaport_name + '(' + response.data[i].seaport_code + ')' + '(' + response.data[i].city_name + ')' + "'  insidemeet_msg='" + response.data[i].insidemeet_msg + "' curbside_msg='" + response.data[i].curbside_msg + "'  meatandGreet='" + response.data[i].is_insidemeet_greet + "' curbsideSeq='" + response.data[i].is_curbside + "'>" + response.data[i].airport_name + "</option>";


            }
            var airportLocation = locationdata1;
            /*   code start here */
            $("#pick_from_seaport").autocomplete({
                source: airportLocation,
                select: function (event, ui) {
                    event.preventDefault()
                    $(this).val(ui.item.label);
                    var getDate = $('#selected_date').val();
                    var getTime = $('#selected_time').val();
                    var getLocation = ui.item.label;

                    if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '') {
                        checkBlackOutDate(getDate, getTime, getLocation);
                    }


                    var selectedValue = ui.item.value;
                    var selectedAirportValue = selectedValue.split('@@');


                    // var option = selectedAirportValue[0];
                    var meatandGreet = selectedAirportValue[2];

                    var curbsideSeq = selectedAirportValue[3];
                    var insidemeet_msg = selectedAirportValue[0];
                    var curbside_msg = selectedAirportValue[1];

                    if (meatandGreet != 0 || curbsideSeq != '0') {

                        $('.holdername').show();
                    } else {

                        $('.holdername').hide();
                    }

                    if (meatandGreet != 0) {

                        $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                        $('#intFlt').show();
                        $('#meetAndGreet').show();
                        $('#meetGreetUpdateService').prop("checked", true);


                    } else {
                        $('#meetAndGreet').hide();
                    }
                    if (curbsideSeq != '0') {
                        $('#curbside_disclaimer').attr("data-content", curbside_msg);
                        $('#intFlt').show();
                        $('#curbSide').show();
                        $('#curbsideUpdateService').prop("checked", true);
                    } else {
                        $('#curbSide').hide();
                    }
                }
            }).autocomplete("instance")._renderItem = function (ul, item) {
                return $("<li>")
                    .append(autoIcon + " &nbsp; " + item.label)
                    .appendTo(ul);
            };
            /* code end here*/
            $("#drop_to_seaport").autocomplete({
                source: airportLocation,
                select: function (event, ui) {
                    event.preventDefault()

                    $(this).val(ui.item.label);
                    var selectedValue = ui.item.value;
                    var selectedAirportValue = selectedValue.split('@@');

                    // var option = selectedAirportValue[0];
                    var meatandGreet = selectedAirportValue[2];

                    var curbsideSeq = selectedAirportValue[3];
                    var insidemeet_msg = selectedAirportValue[0];
                    var curbside_msg = selectedAirportValue[1];


                }
            }).autocomplete("instance")._renderItem = function (ul, item) {
                return $("<li>")
                    .append(autoIcon + " &nbsp; " + item.label)
                    .appendTo(ul);
            };
        });
    }
    /*function end for getting the seaportname list */
};


// getinfo.getAirportName('r');
getinfo.getAirportName();
getinfo.getSeaPortName();
getinfo.getPerpassengerName();
getinfo.getTrainInformationName();




if(parseInt(localStorage.show_routing_map) === 1 ){
    //showinitialMap();
}


// if(parseInt(localStorage.show_routing_map) === 1 ){
//     one_column();
// }

function one_column() {
    console.log('one column');
    $('#map-column').hide();
    $('#input-column').addClass('col-md-offset-3');
}

function two_column() {
    console.log('two column');
    $('#input-column').removeClass('col-md-offset-3');
    $('#map-column').show();

}

$(window).bind('resize', function(event) {
//Whatever you want to run here
    window_size_changed();
});



//getinfo.getSeaPortName();
