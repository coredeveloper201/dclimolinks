 //code for google map start===========
var Demo = {
  // HTML Nodes
  mapContainer: document.getElementById('map-container'),
  dirContainer: document.getElementById('dir-container'),
  fromInput: document.getElementById('from-input'),
  toInput: document.getElementById('to-input'),
  travelModeInput: document.getElementById('travel-mode-input'),
  unitInput: document.getElementById('unit-input'),

  // API Objects
  dirService: new google.maps.DirectionsService(),
  dirRenderer: new google.maps.DirectionsRenderer(),
  map: null,

  showDirections: function(dirResult, dirStatus) {
    if (dirStatus != google.maps.DirectionsStatus.OK) {
      //alert('Directions failed: ' + dirStatus);
      return;
    }

    // Show directions
    Demo.dirRenderer.setMap(Demo.map);
    Demo.dirRenderer.setPanel(Demo.dirContainer);
    Demo.dirRenderer.setDirections(dirResult);
  },

  getSelectedTravelMode: function() {
    var value ="DRIVING";
    return value;
  },

  getSelectedUnitSystem: function() {
    return 0;
  },
  getDirections: function(startpoint,endpoint) {

      //alert(startpoint);
      var fromStr = startpoint;
      var toStr =  endpoint;
      var dirRequest = {
      origin: fromStr,
      destination: toStr,
      travelMode: Demo.getSelectedTravelMode(),
      unitSystem: Demo.getSelectedUnitSystem(),
      provideRouteAlternatives: true
    };
    Demo.dirService.route(dirRequest, Demo.showDirections);
  },

  init: function(startloc,endloc) {
    var param=parseURLParams(window.location.href);
    
    //var latLng = new google.maps.LatLng(39.778182, -86.152300);
    var latLng = new google.maps.LatLng(37.65207, -85.57618);
    Demo.map = new google.maps.Map(Demo.mapContainer, {
      zoom: 9,
      center: latLng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // Show directions onload
     Demo.getDirections(startloc,endloc);
  }
};

  function parseURLParams(url) {
      var queryStart = url.indexOf("?") + 1;
      var queryEnd   = url.indexOf("#") + 1 || url.length + 1;
      var query      = url.slice(queryStart, queryEnd - 1);

      if (query === url || query === "") return;

      var params  = {};
      var nvPairs = query.replace(/\+/g, " ").split("&");

      for (var i=0; i<nvPairs.length; i++) {
        var nv = nvPairs[i].split("=");
        var n  = decodeURIComponent(nv[0]);
        var v  = decodeURIComponent(nv[1]);
        if ( !(n in params) ) {
          params[n] = [];
        }
        params[n].push(nv.length === 2 ? v : null);
      }
      return params;
    }

//google.maps.event.addDomListener(window, 'load', Demo.init);


 //code for google map end============



//global variable declare for map ====

var option;
var startlocation;
var endlocation;

//global variable declare for map end ====


/*Enabel the form input value function start=================*/
enableFormField();
function enableFormField(){
      $("#searchRateForm :input").prop("disabled", true);
      $("#services").prop("disabled", false);
        /*  $('#from_airport_droploc').prop( "disabled", false );
           $('#from_airport_pickloc').prop( "disabled", false );
           $('#selected_date').prop( "disabled", false );
           $('#selected_time').prop( "disabled", false );
           $('#location_map').prop( "disabled", false );
           $('#no_map').prop( "disabled", true );
           $('#add_stop').prop( "disabled", true );
           
           $("#total_passenger" ).prop( "disabled", true );
           $("#luggage_quantity" ).prop( "disabled", true );
           $('#submit_button').prop("disabled", true);*/
   }
/*Enabel the form input value function end=================*/

//stoplocation value on yes and no radi button ====

 $('input[type=radio][name=add_stop]').change(function() {

           //alert(this.value);
        if (this.value == 'yes') {
           
           //$('.form-control2').show();
            
            $('#stop_points').show();
            $('.aadMore').html('<div class="stopLocationfield"><input type="text" id="stopLocation" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>');
             
              var places = new google.maps.places.Autocomplete(document.getElementById('stopLocation'),options);
             

        }
        else if (this.value == 'no') {
           $('#stop_points').hide();
             $('.aadMore').html(' ');
        }
    });
  
 /*google api for usa location setting=================*/
  var options = {
    types: ['(cities)'],
     componentRestrictions: {country: "us"}
     
   };   
/*service onchange function start=================*/   
    $('#services').change(function()
    {


    option = $(this).find('option:selected').val();
        if(option=='AIRA'){

            $('#intFlt').show();    
        }
        else{
            $('#intFlt').hide();
        }



        //reset the all form value=======

          $('.stopLocationfield').remove();

          $('#searchRateForm')[0].reset();
          $('#services').val(option);
          $('#stop_points').hide();
          $("#location_map").hide();
      /*again reset the passenger and luggage quantity to zero start===*/
          
               $('#luggage_quantity').val("0");
               $('#total_passenger').val("0");
           
       /*again reset the passenger and luggage quantity to zero end===*/

       /*start to select the service an all work perform for selected service===*/


         option = $(this).find('option:selected').val();
         if(option=='PTP'){
           $("#searchRateForm :input").prop("disabled", false);
            $('#map_loaction').show();
            $('#stop_point').show();
            $('#triphr').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();
            $('#pick_hourly_loc').hide();
            $('#pick_charter_loc').hide();
            //$('#location_map').show();
            $('#drop_point_loc').show();
            $('#from_pont_to_point').show();
            $('#pick_hourly').hide();
            $('#from_seapick').hide();
            $('#from_seadrop').hide();
            $('#to_seapick').hide();
          
            $('#to_seadrop').hide();  
        }else if(option=='CH'){
             $("#searchRateForm :input").prop("disabled", false);
            $('#map_loaction').hide();
            $('#stop_point').hide();
            $('#triphr').show();
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();
            $('#pick_hourly_loc').hide();
            $('#pick_charter_loc').show();
            $('#from_seapick').hide();
            $('#from_seadrop').hide();
            $('#to_seapick').hide();
            $('#to_seadrop').hide();  
        
         }else if(option=='HRLY'){

           $("#searchRateForm :input").prop("disabled", false);
            $('#map_loaction').hide();
            $('#stop_point').hide();
            $('#pick_charter_loc').hide();
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();

            $('#triphr').show();
            $('#pick_hourly_loc').show();
            $('#pick_hourly').show();
            $('#from_seapick').hide();
            $('#from_seadrop').hide();
            $('#to_seapick').hide();
           
            $('#to_seadrop').hide();  
        }
        else if(option=='SEAA'){
            $("#searchRateForm :input").prop("disabled", false);
            $('#map_loaction').show();
            //$('#location_map').show();
            $('#stop_point').show();
            $('#pick_hourly_loc').hide();
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();

            $('#from_seapick').show();
            $('#from_seadrop').show();
            $('#to_seapick').hide();
            $('#pick_charter_loc').hide();
            
            $('#to_seadrop').hide();
            $('#triphr').hide();

            }else if(option=='SEAD'){
             $('#map_loaction').show();
             //$('#location_map').show();
             $('#stop_point').show();
            
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();
            $('#pick_charter_loc').hide();
         
            $('#from_seapick').hide();
            $('#to_seapick').show();
            $('#pick_hourly_loc').hide();
            $('#to_seadrop').show();
            $('#from_seadrop').hide();
         
            $('#triphr').hide();

        }
         else if(option=='AIRD'){
            $("#searchRateForm :input").prop("disabled", false);
            $('#map_loaction').show();
            //$('#location_map').show();
            $('#stop_point').show();
          
            $('#pick_hourly_loc').hide();
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#pick_charter_loc').hide();
       
            $('#triphr').hide();
            $('#from_airport_drop').hide();
            $('#from_airport_pickup').hide();
            $('#from_seapick').hide();
        
            
            $('from_seapick').hide();
            $('#from_seadrop').hide();
            $('#to_airport_pickup').show();
            $('#to_airport_drop').show();
            $('#to_seapick').hide();
            $('#to_seadrop').hide();
           
        }else if(option=='AIRA'){
            $("#searchRateForm :input").prop("disabled", false);





            $('#map_loaction').show();
            //$('#location_map').show();
            $('#stop_point').show();
        
            $('#pick_hourly_loc').hide();
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();
            $('#to_seapick').hide();
            $('#to_seadrop').hide();
            $('#triphr').hide();
            $('#from_airport_drop').show();
            $('#from_seadrop').hide();
            $('#from_airport_pickup').show();
            $('#from_seadrop').hide();
            $('#pick_charter_loc').hide();
        
        }else{
           $("#searchRateForm :input").prop("disabled", true);
            $("#services").prop("disabled", false);
            $('#map_loaction').show();
            //$('#location_map').show();
            $('#stop_point').show();
        
            $('#pick_hourly_loc').hide();
            $('#from_pont_to_point').hide();
            $('#drop_point_loc').hide();
            $('#to_airport_pickup').hide();
            $('#to_airport_drop').hide();
            $('#to_seapick').hide();
            $('#to_seadrop').hide();
            $('#triphr').hide();
            $('#from_airport_drop').show();
            $('#from_seadrop').hide();
            $('#from_airport_pickup').show();
            $('#from_seadrop').hide();
            $('#pick_charter_loc').hide();
           /*$('#from_airport_droploc').prop( "disabled", true );
           $('#from_airport_pickloc').prop( "disabled", true );
           $('#selected_date').prop( "disabled", true );
           $('#selected_time').prop( "disabled", true );
           $('#location_map').prop( "disabled", true );
           $('#no_map').prop( "disabled", true );
           $('#add_stop').prop( "disabled", true );
           
           $("#total_passenger" ).prop( "disabled", true );
           $("#luggage_quantity" ).prop( "disabled", true );
           $('#submit_button').prop("disabled", true);*/
        }
    });
        
        /*work ended for the selected service */

        /*function start for airport drop location */

      /* $('#to_airport_droploc').change(function(){

            $('#no_map').prop("checked", true);
            $("#map_icon").show();
            $("#location_map").hide();

       });*/

        /*function end for airport onchange drop location */

        /*function start for airport pickup onchange location */

        /*$('#from_airport_pickloc').change(function(){

             //alert('changed');

            $('#no_map').prop("checked", true);
            $("#map_icon").show();
            $("#location_map").hide();

       });
*/
        /*function end for airport pickup onchange location */

         /*function start for map start and end point of map location  */

        $('input[type=radio][name=map]').change(function() {
        if (this.value == 'yes') {
            
            if(option=='AIRA'){
         
              startlocation = $('#from_airport_pickloc').val();
              endlocation  =  $('#from_airport_droploc').val();
             

            }else if(option=='AIRD'){
              
              endlocation = $('#to_airport_droploc').val();
              startlocation =  $('#to_airport_pickloc').val();
              

            }
            else if(option=='PTP'){
              
              startlocation = $('#pick_point').val();
              endlocation  =  $('#drop_point').val();
               
            }
            
            else if(option=='SEAA'){
              
              startlocation = $('#pick_from_seaport').val();
              endlocation  =  $('#drop_from_seaport').val();
             

            }
            else if(option=='SEAD'){
              
              endlocation = $('#drop_to_seaport').val();
              startlocation =  $('#pick_to_seaport').val();
             

            }
             
            // Demo.init(startlocation,endlocation);
            if(option==undefined){
               alert("Please Select service");
                 $('#no_map').prop("checked", true);
              }else{

                if(startlocation=='Select' || startlocation=='' ){
                  alert('Plese Provide Start Point');
                  $('#no_map').prop("checked", true);
                 
                }else if(endlocation=='Select' || endlocation=='' ){
                    alert('Plese Provide End Point');
                    $('#no_map').prop("checked", true);
                }else{
                Demo.init(startlocation,endlocation)
                $("#map_icon").hide();
                $("#location_map").show();
                }
           }           
        }
        if (this.value == 'no'){

            $("#map_icon").show();
            $("#location_map").hide();

        }

   }); 

   /*function start for map start and end point of map location  */

   /*function start for google api for start and end point for map location */
            google.maps.event.addDomListener(window, 'load', function () {
                  var places = new google.maps.places.Autocomplete(document.getElementById('stopLocation'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('from_airport_droploc'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('to_airport_pickloc'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('drop_from_seaport'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('pick_to_seaport'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('pick_charter'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('pick_point'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('drop_point'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('stoplocation'),options);
                  var places = new google.maps.places.Autocomplete(document.getElementById('pick_hourly'),options);
             });
       
      /*function end for google api for start and end point for map location */     
         
    /*function start for getting the service list from limoany where */     
getServiceTypes();
function getServiceTypes()
          {
            var serviceTypeData = [];
    $.ajax({
            url : "webservice/service.php",
            type : 'post',
            data : 'action=GetServiceTypes',
            dataType : 'json',
            success : function(data){
                //console.log(data);
                if(data.ResponseText == 'OK'){

                    var ResponseHtml='<option value="">Select</option>';
                    $.each(data.ServiceTypes.ServiceType, function( index, result){
                     ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
                     console.log(result);
                                    });
                 
                 
                  
                 $('#services').html(ResponseHtml);
                   
                }
      }});
 }
 /*function end for getting the service list from limo any where*/

/*function start for getting the airport list */
$("#from_airport_pickloc").keyup(function(){

  var inputBoxFieldValue =$('#from_airport_pickloc').val();
  getinfo.getAirportName(inputBoxFieldValue);
  //alert(inputBoxFieldValue);
});

$("#to_airport_droploc").keyup(function(){

  var inputBoxFieldValue =$('#to_airport_droploc').val();
  getinfo.getAirportName(inputBoxFieldValue);
  //alert(inputBoxFieldValue);
});

$("#pick_from_seaport").keyup(function(){

  var inputBoxFieldValue =$('#pick_from_seaport').val();
  getinfo.getSeaPortName(inputBoxFieldValue);
  //alert(inputBoxFieldValue);
});

$("#drop_to_seaport").keyup(function(){

  var inputBoxFieldValue =$('#drop_to_seaport').val();
  getinfo.getSeaPortName(inputBoxFieldValue);
  //alert(inputBoxFieldValue);
});

var getinfo={
       _Serverpath:"phpfile/client.php",

getAirportName:function(inputBoxFieldValue){
 
 //var getUserId=window.localStorage.getItem('limoanyWhereVerification');
     //console.log(getUserId);
 var locationdata1 = [];

 var inputBoxFieldValue =inputBoxFieldValue;
 //alert(inputBoxFieldValue);
      
         if(typeof(getUserId)=="string")
                {   

                    getUserId=JSON.parse(getUserId);

                }

              var fd = new FormData();
                fd.append("action","getAirportName");
                fd.append("inputValue",inputBoxFieldValue);
                //fd.append("user_id",getUserId.user_id);

    $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData:false,
            contentType:false,
            data:fd
    }).done(function(result){

           
         var response=JSON.parse(result);
         
          
          var ResponseHtml='<option value="">Select</option>';  
          for(i=0;i<response.data.length;i++){
            locationdata1.push({"value":response.data[i].is_int_flight+"@@"+response.data[i].insidemeet_msg+"@@"+response.data[i].curbside_msg+"@@"+response.data[i].int_flight_msg+"@@"+response.data[i].is_insidemeet_greet+"@@"+response.data[i].is_curbside,"label":response.data[i].airport_name +'('+response.data[i].airport_code+')' +'('+response.data[i].city_name+')'});
            ResponseHtml+="<option value='"+response.data[i].airport_name +'('+response.data[i].airport_code+')' +'('+response.data[i].city_name+')'+"' seq='"+response.data[i].is_int_flight+"' insidemeet_msg='"+response.data[i].insidemeet_msg+"' curbside_msg='"+response.data[i].curbside_msg+"' int_flight_msg='"+response.data[i].int_flight_msg+"' meatandGreet='"+response.data[i].is_insidemeet_greet+"' curbsideSeq='"+response.data[i].is_curbside+"'>"+response.data[i].airport_name+"</option>";

          }
            var airportLocation = locationdata1;
          setTimeout(function(){
      
            $( "#from_airport_pickloc,#to_airport_droploc").autocomplete({
            source: airportLocation,
            select:function(event, ui){

              setTimeout(function(){

                $('#from_airport_pickloc').val(ui.item.label);

              },1000);
              
              var selectedValue = ui.item.value;
              var selectedAirportValue = selectedValue.split('@@');

                    var option = selectedAirportValue[0];
                    var meatandGreet = selectedAirportValue[4];
                        
                    var curbsideSeq = selectedAirportValue[5];
                    var insidemeet_msg = selectedAirportValue[1];
                    var curbside_msg = selectedAirportValue[2];
                    var int_flight_msg = selectedAirportValue[3];
                 alert(selectedAirportValue);
                 //alert(JSON.stringify(ui));
                 if(meatandGreet!='0'){
                      //var msg ="this is the content";
                        $('#meetAndGreet_disclaimer').attr("data-content",insidemeet_msg); 
                         //var popover = $('#meetAndGreet').data('bs.popover');
                          // popover.a.content = "YOUR NEW TEXT";
                        //$(".popover-content")[0].innerHTML = 'something else';
                        $('#meetAndGreet').show();
                    }
                    else
                    {

                         $('#meetAndGreet').hide();
                    }
                    if(curbsideSeq!='0'){
                        $('#curbside_disclaimer').attr("data-content",curbside_msg);
                        $('#curbSide').show();
                    }
                    else
                    {
                        $('#curbSide').hide();


                    }



                    if(option!='0')
                    {

                        $('#international_disclaimer').attr("data-content",int_flight_msg);
                        $('.interNationFlight').show();
                    }
                    else
                    {
                          $('.interNationFlight').hide();


                    }

            }


          });


          },1000);
          


         
        /* for(i=0;i<response.data.length;i++){
            ResponseHtml+="<option value='"+response.data[i].airport_code+"' seq='"+response.data[i].is_int_flight+"' insidemeet_msg='"+response.data[i].insidemeet_msg+"' curbside_msg='"+response.data[i].curbside_msg+"' int_flight_msg='"+response.data[i].int_flight_msg+"' meatandGreet='"+response.data[i].is_insidemeet_greet+"' curbsideSeq='"+response.data[i].is_curbside+"'>"+response.data[i].airport_name+"</option>";
        }*/
           /* $('#from_airport_pickloc1').html(ResponseHtml);
           




            $('#from_airport_pickloc').off();
            $('#from_airport_pickloc').on("change",function(){
                   //$('#from_airport_pickloc1').val(selecrBoxValue);
                    var selecrBoxValue= $('#from_airport_pickloc').val();
                    
                    alert(selecrBoxValue);
                    var option = $(this).find('option:selected').attr('seq');
                    var meatandGreet = $(this).find('option:selected').attr('meatandGreet');
                        
                    var curbsideSeq = $(this).find('option:selected').attr('curbsideSeq');
                    var insidemeet_msg = $(this).find('option:selected').attr('insidemeet_msg');
                    var curbside_msg = $(this).find('option:selected').attr('curbside_msg');
                    var int_flight_msg = $(this).find('option:selected').attr('int_flight_msg');
                    if(meatandGreet!='0'){
                      //var msg ="this is the content";
                        $('#meetAndGreet_disclaimer').attr("data-content",insidemeet_msg); 
                         //var popover = $('#meetAndGreet').data('bs.popover');
                          // popover.a.content = "YOUR NEW TEXT";
                        //$(".popover-content")[0].innerHTML = 'something else';
                        $('#meetAndGreet').show();
                    }
                    else
                    {

                         $('#meetAndGreet').hide();
                    }
                    if(curbsideSeq!='0'){
                        $('#curbside_disclaimer').attr("data-content",curbside_msg);
                        $('#curbSide').show();
                    }
                    else
                    {
                        $('#curbSide').hide();


                    }



                    if(option!='0')
                    {

                        $('#international_disclaimer').attr("data-content",int_flight_msg);
                        $('.interNationFlight').show();
                    }
                    else
                    {
                          $('.interNationFlight').hide();


                    }*/
                    //alert(result);

          /*$('#to_airport_droploc').html(ResponseHtml);
          $('#to_airport_droploc').multiselect({
                               maxHeight: 200,
                               buttonWidth: '100%',
                               height: '42px',
                               includeSelectAllOption: true,
                               enableFiltering:true 
                               });  */

   });

},

/*function end for getting the airport list */

/*function start for getting the seaportname list */
getSeaPortName:function(inputBoxFieldValue){
 
 //var getUserId=window.localStorage.getItem('companyInfo');
//var getinputTextValue = $('#pick_from_seaport').val();
 var locationdata1 = [];
 var inputBoxFieldValue =inputBoxFieldValue;

         /*if(typeof(getUserId)=="string")
                {   

                    getUserId=JSON.parse(getUserId);

                }
*/
                var fd = new FormData();
                fd.append("action","getSeaPortName");
                
                fd.append("inputValue",inputBoxFieldValue);
                //fd.append("user_id",getUserId[0].id)

    $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData:false,
            contentType:false,
            data:fd
    }).done(function(result){

           
        var response=JSON.parse(result);
     
           //var ResponseHtml='<option value="">Select</option>';
        for(i=0;i<response.data.length;i++){

        locationdata1.push(response.data[i].seaport_name +'('+response.data[i].seaport_code+')' +'('+response.data[i].city_name+')');

            //ResponseHtml+="<option value='"+response.data[i].seaport_code+"'>"+response.data[i].seaport_name+"</option>";
        }

         var airportLocation = locationdata1;
            console.log(airportLocation);
          $("#pick_from_seaport,#drop_to_seaport").autocomplete({
            source: airportLocation
          });
          

          /* $('#pick_from_seaport').html(ResponseHtml);
          
             $('#pick_from_seaport').multiselect({
                               maxHeight: 200,
                               buttonWidth: '100%',
                               height: '42px',
                               includeSelectAllOption: true,
                               enableFiltering:true 
                               });  
        
           
           $('#drop_to_seaport').html(ResponseHtml);
           $('#drop_to_seaport').multiselect({
                               maxHeight: 200,
                               buttonWidth: '100%',
                               height: '42px',
                               includeSelectAllOption: true,
                               enableFiltering:true 
                               }); 
           */

   });

}

/*function end for getting the seaportname list */
};
//Demo.init();
//google.maps.event.addDomListener(window, 'load', mapLoc.init);
//getinfo.getAirportName();
//getinfo.getSeaPortName();
