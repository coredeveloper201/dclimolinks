$('#refresh_overlay').css("display", "block");

isCheckedLimoAnyWhere();

var getLoadPage = {}
var _Serverpath = _SERVICEPATHCLIENT;

function isCheckedLimoAnyWhere() {
    var comp_id = $("#comp_id").html();

    var url = location.href.replace(document.getElementById('mini_service') == null ? 'frontend/services.php' : document.getElementById('mini_service').value , 'SMA/phpfile/checkLimoanywhere_client.php')

    $.ajax({

        url: url,

        type: 'POST',

        data: "action=isCheckedLimoAnyWhere&userId=" + comp_id,

        success: function (response) {

            var responseObj = response;


            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);

            }

            if (responseObj.code != 1003) {

                localStorage.removeItem("limoanyWhereVerification");
                top.location = "errorPage.php?id=" + comp_id;

            }
            else {

                // window.locatStorage.setItem("limoanyWhereVerification",response);
                localStorage.setItem("limoanyWhereVerification", JSON.stringify(responseObj.data));
                if(localStorage.companyInfo === undefined){
                    localStorage.setItem("companyInfo", JSON.stringify(responseObj.data));
                }
                checkSystem();
                localStorage.setItem("show_routing_map", parseInt(responseObj.data[0].show_routing_map));
                if(parseInt(responseObj.data[0].show_routing_map) === 1){
                    $('#input-column').removeClass('col-md-offset-3');
                    $('#map-column').show();
                    setTimeout(function(){
                        showinitialMap();

                        if(screen.width >= 768){
                            $('.hide-map-mobile').hide();
                            $('.hide-map-desktop').show();
                        } else {
                            $('.hide-map-mobile').show();
                            $('.hide-map-desktop').hide();
                        }

                    } , 1800);

                } else {
                    $('#map-column').hide();
                    $('#input-column').addClass('col-md-offset-3');
                }
            }

        }
    });

}


function checkSystem() {
    var getUserId = window.localStorage.getItem('limoanyWhereVerification');
    if (typeof(getUserId) == "string") {
        getUserId = JSON.parse(getUserId);
    }
    var user_id = getUserId[0].user_id;
    var current_date = new Date();
    var month = current_date.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    var day = current_date.getDate();
    if (day < 10) {
        day = '0' + day;
    }
    var year = current_date.getFullYear();
    var getHour = current_date.getHours();
    if (getHour < 10) {
        getHour = '0' + getHour;
    }
    var getMinute = current_date.getMinutes();

    if (getMinute < 10) {

        getMinute = '0' + getMinute;
    }

    var currentDate = year + "-" + month + "-" + day;
    var current_time = getHour + ":" + getMinute + ":" + '00';


    $.ajax({

        url: _Serverpath,

        type: 'POST',

        data: "action=checksystem&user_id=" + user_id + "&current_date=" + currentDate + "&current_time=" + current_time,

        success: function (response) {

            var responseObj = response;

            var responseHTML = '';

            if (typeof(response) == "string") {

                responseObj = JSON.parse(response);

            }

            if (responseObj.code == 1007) {

                localStorage.setItem('sys_message', responseObj.data[0].message);
                //$('#sys_shut_message').val(responseObj.data[0].message);
                window.location.href = "system_down.html";
                //$('#refresh_overlay').css("display","none");


                //location.reload();

            } else {


                $('#refresh_overlay').css("display", "none");


                // $('#indexpage').css('display','block');
                $('#servicepage').css('display', 'block');
                $('#selectvehiclepage').css('display', 'block');
                $('#paymentpage').css('display', 'block');


            }

        },

        error: function () {


        }

    });


}


setTimeout(function () {


}, 500);

