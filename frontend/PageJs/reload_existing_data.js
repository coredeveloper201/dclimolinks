function show_disclaimer_on_reload(){

    $('#luggage_disclaimer_message').html(window.localStorage.getItem("LuggageDisclaimer"))

    var current_luggageJson = {
        small : $('#luggage_quantity_small').val(),
        medium : $('#luggage_quantity_medium').val(),
        large : $('#luggage_quantity_large').val()
    }
    var current_cost = getExtraLuggageCost_new(current_luggageJson , Number(document.getElementById('total_passenger').value)) , extra_now = 0;

    Object.values(current_cost).forEach(function (item) {
        extra_now += Number(item);
    })

    if(extra_now > 0)
        $('#luggage_disclaimer').show();
    else
        $('#luggage_disclaimer').hide();
}

function show_map_name_value() {
    try {
        var existing_name_combination = JSON.parse(localStorage.getItem('name_combination'));
        existing_name_combination.forEach(function(item) {
            $(''+item.id+'').html(item.value);
        })
    } catch(e) {
        console.log('err ' , e);
    }

}

function reload_airport_pickup(data){
    var stops = [], existing_luggageInfo = {}
    $("#searchRateForm :input").prop("disabled", false);


    $('#drop_charte_loc').hide();
    $('#map_loaction').show();
    //$('#location_map').show();
    $('#stop_point').show();
    $('#drop_hourly_loc').hide();
    $('#pick_hourly_loc').hide();
    $('#from_pont_to_point').hide();
    $('#drop_point_loc').hide();
    $('#to_airport_pickup').hide();
    $('#to_airport_drop').hide();
    $('#to_seapick').hide();
    $('#to_seadrop').hide();
    $('#triphr').hide();
    $('#from_airport_drop').show();
    $('#from_seadrop').hide();
    $('#from_airport_pickup').show();
    $('#from_seadrop').hide();
    $('#pick_charter_loc').hide();
    $('#pick_from_seaport').hide();


    /* validation start here */
    $('#pick_point').prop("required", false);
    $('#drop_point').prop("required", false);
    $('#to_airport_pickloc').prop("required", false);
    $('#to_airport_droploc').prop("required", false);
    $('#pick_from_seaport').prop("required", false);
    $('#drop_from_seaport').prop("required", false);
    $('#pick_to_seaport').prop("required", false);
    $('#drop_to_seaport').prop("required", false);
    $('#pick_hourly').prop("required", false);
    $('#drop_hourly').prop("required", false);
    $('#pick_charter').prop("required", false);
    $('#drop_charter').prop("required", false);
    $('#from_airport_pickloc').prop("required", true);
    $('#from_airport_droploc').prop("required", true);

    setTimeout(function () {
        document.getElementById('services').value = data.serviceType;

        document.getElementById('selected_date').value = data.pickup_date;
        document.getElementById('selected_time').value = data.pickup_time;

        document.getElementById('from_airport_pickloc').value = data.pickuplocation;
        document.getElementById('from_airport_droploc').value = data.dropoff_location;

        document.getElementById('total_passenger').value = data.total_passenger;

        if(data.stopAddtress){
            stops = data.stopAddtress.split('@');
            if(stops.length > 0){
                document.getElementById('add_stop').checked = true;
                $('#stop_points').show();

                $(".aadMore").html('');
                var wrapper = $(".aadMore"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID

                var x = 1; //initlal text box count
                stops.forEach(function (item , i) { //on add input button click


                    //text box increment
                    //$('.aadMore').append('<div class="stopLocationfield"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>'); //add input box
                    $('.aadMore').append('<div class="cont'+(i+1)+' col-sm-12 mt-15 stopLocationfield"><label class="short-address" id="stopLocation'+(i+1)+'_name"></label><div class="input-group"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control" name="mytext[]"/><span id="div-' + (i+1) + '" class="input-group-addon bg-primary remove_field">X</span></div></div>'); //add input box


                    var stopeLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation' + (i+1)), options);
                    add_postal_code_with_address(stopeLocation, $('#stopLocation' + (i+1)));


                });
            }
        }

        maoGenerate(null);

        if(localStorage.getItem('noLuggage') === 'yes'){
            $('#lugSelector').trigger('click');

        }

        if(data.interNationFlightChecked === 'yes' || data.ismeetGreetUpdateChecked === 'yes' || data.iscurbsideUpdateChecked === 'yes'){
            $('#intFlt').show();
            $('.interNationFlight').show();
        }

        if(data.interNationFlightChecked === 'yes'){
            $('.holdername').show();
            $('.interNationFlight').show();
            $('#interNationFlightUpdateService').trigger('click');
        }
        if(data.ismeetGreetUpdateChecked === 'yes'){
            $('#meetAndGreet').show();
            $('.holdername').show();
            $('#meetGreetUpdateService').trigger('click');
            $('#curbSide').show();
        }
        if(data.iscurbsideUpdateChecked === 'yes'){
            $('#curbSide').show();
            $('.holdername').show();
            $('#meetAndGreet').show();
            $('#curbsideUpdateService').trigger('click');
        }


        try {
            existing_luggageInfo = JSON.parse(window.localStorage.getItem("luggageJson"));
            document.getElementById('luggage_quantity_small').value = existing_luggageInfo['small'];
            document.getElementById('luggage_quantity_medium').value = existing_luggageInfo['medium'];
            document.getElementById('luggage_quantity_large').value = existing_luggageInfo['large'];
        }
        catch (e) {

        }

show_map_name_value();
    } , 800)


}

function reload_airport_drop_off(data){
    var stops = [], existing_luggageInfo = {};

    $("#searchRateForm :input").prop("disabled", false);
    $('#map_loaction').show();
    //$('#location_map').show();
    $('#stop_point').show();
    $('#drop_charte_loc').hide();
    $('#pick_hourly_loc').hide();
    $('#from_pont_to_point').hide();
    $('#drop_point_loc').hide();
    $('#pick_charter_loc').hide();
    $('#from_perpassenger_point').hide();
    $('#to_perpassenger_point').hide();

    $('#to_train_pickup_location').hide();
    $('#to_train_dropof_location').hide();

    $('#drop_hourly_loc').hide();
    $('#triphr').hide();
    $('#from_airport_drop').hide();
    $('#from_airport_pickup').hide();
    $('#from_seapick').hide();

    $('#pick_from_seaport').hide();
    $('from_seapick').hide();
    $('#from_seadrop').hide();
    $('#to_airport_pickup').show();
    $('#to_airport_drop').show();
    $('#to_seapick').hide();
    $('#to_seadrop').hide();

    $('#from_train_pickup_location').hide();
    $('#from_train_dropof_location').hide();

    /* validation start here */
    $('#pick_point').prop("required", false);
    $('#drop_point').prop("required", false);
    $('#from_airport_pickloc').prop("required", false);
    $('#from_airport_droploc').prop("required", false);
    $('#pick_from_seaport').prop("required", false);
    $('#drop_from_seaport').prop("required", false);
    $('#to_train_pickup_location_input').prop("required", false);
    $('#to_train_dropof_location_input').prop("required", false);

    $('#perpassenger_pickup_location').prop("required", false);
    $('#perpassenger_dropof_location').prop("required", false);


    $('#from_train_pickup_location_input').prop("required", false);
    $('#from_train_dropof_location_input').prop("required", false);


    $('#pick_to_seaport').prop("required", false);
    $('#drop_to_seaport').prop("required", false);
    $('#pick_hourly').prop("required", false);
    $('#drop_hourly').prop("required", false);
    $('#pick_charter').prop("required", false);
    $('#drop_charter').prop("required", false);
    $('#to_airport_pickloc').prop("required", true);
    $('#to_airport_droploc').prop("required", true);

    setTimeout(function () {
        document.getElementById('services').value = data.serviceType;

        document.getElementById('selected_date').value = data.pickup_date;
        document.getElementById('selected_time').value = data.pickup_time;

        document.getElementById('to_airport_pickloc').value = data.pickuplocation;
        document.getElementById('to_airport_droploc').value = data.dropoff_location;

        document.getElementById('total_passenger').value = data.total_passenger;

        if(data.stopAddtress){
            stops = data.stopAddtress.split('@');
            if(stops.length > 0){
                document.getElementById('add_stop').checked = true;
                $('#stop_points').show();

                $(".aadMore").html('');
                var wrapper = $(".aadMore"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID

                var x = 1; //initlal text box count
                stops.forEach(function (item , i) { //on add input button click


                    //text box increment
                    //$('.aadMore').append('<div class="stopLocationfield"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>'); //add input box
                    $('.aadMore').append('<div class="cont'+(i+1)+' col-sm-12 mt-15 stopLocationfield"><label class="short-address" id="stopLocation'+(i+1)+'_name"></label><div class="input-group"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control" name="mytext[]"/><span id="div-' + (i+1) + '" class="input-group-addon bg-primary remove_field">X</span></div></div>'); //add input box


                    var stopeLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation' + (i+1)), options);
                    add_postal_code_with_address(stopeLocation, $('#stopLocation' + (i+1)));


                });
            }
        }

        maoGenerate(null);

        if(localStorage.getItem('noLuggage') === 'yes'){
            $('#lugSelector').trigger('click');

        }

        if(data.interNationFlightChecked === 'yes' || data.ismeetGreetUpdateChecked === 'yes' || data.iscurbsideUpdateChecked === 'yes'){
            $('#intFlt').show();
            $('.interNationFlight').show();
        }

        if(data.interNationFlightChecked === 'yes'){
            $('#intFlt').show();
            $('.holdername').show();
            $('.interNationFlight').show();
            $('#interNationFlightUpdateService').trigger('click');
        }
        if(data.ismeetGreetUpdateChecked === 'yes'){
            $('#meetAndGreet').show();
            $('.holdername').show();
            $('#meetGreetUpdateService').trigger('click');
            $('#curbSide').show();
        }
        if(data.iscurbsideUpdateChecked === 'yes'){
            $('#curbSide').show();
            $('.holdername').show();
            $('#meetAndGreet').show();
            $('#curbsideUpdateService').trigger('click');
        }


        try {
            existing_luggageInfo = JSON.parse(window.localStorage.getItem("luggageJson"));
            document.getElementById('luggage_quantity_small').value = existing_luggageInfo['small'];
            document.getElementById('luggage_quantity_medium').value = existing_luggageInfo['medium'];
            document.getElementById('luggage_quantity_large').value = existing_luggageInfo['large'];
        }
        catch (e) {

        }

show_map_name_value();
    } , 800)


}

function reload_ptp(data){
    var stops = [], existing_luggageInfo = {};

    $("#searchRateForm :input").prop("disabled", false);
    $('#map_loaction').show()
    $('#stop_point').show();
    $('#triphr').hide();
    $('#from_airport_drop').hide();
    $('#from_airport_pickup').hide();
    $('#to_airport_pickup').hide();
    $('#to_airport_drop').hide();
    $('#pick_hourly_loc').hide();
    $('#pick_charter_loc').hide();
    $('#drop_charte_loc').hide();
    $('#from_perpassenger_point').hide();
    $('#to_perpassenger_point').hide();
    $('#from_train_pickup_location').hide();
    $('#from_train_dropof_location').hide();
    $('#to_train_pickup_location').hide();
    $('#to_train_dropof_location').hide();

    $('#drop_point_loc').show();
    $('#from_pont_to_point').show();


    /* validation applied start here */
    $('#from_airport_pickloc').prop("required", false);
    $('#from_airport_droploc').prop("required", false);

    $('#perpassenger_pickup_location').prop("required", false);
    $('#perpassenger_dropof_location').prop("required", false);

    $('#to_airport_pickloc').prop("required", false);
    $('#to_airport_droploc').prop("required", false);
    $('#pick_from_seaport').prop("required", false);
    $('#drop_from_seaport').prop("required", false);
    $('#pick_to_seaport').prop("required", false);
    $('#drop_to_seaport').prop("required", false);
    $('#pick_hourly').prop("required", false);
    $('#drop_hourly').prop("required", false);
    $('#pick_charter').prop("required", false);
    $('#drop_charter').prop("required", false);
    $('#to_train_pickup_location_input').prop("required", false);
    $('#to_train_dropof_location_input').prop("required", false);
    $('#to_train_dropof_location_input').prop("required", false);
    $('#from_train_dropof_location_input').prop("required", false);
    $('#pick_point').prop("required", true);
    $('#drop_point').prop("required", true);
    /* validation applied End here */


    $('#pick_hourly').hide();
    $('#from_seapick').hide();
    $('#from_seadrop').hide();
    $('#to_seapick').hide();
    $('#drop_hourly_loc').hide();
    $('#pick_from_seaport').hide();
    $('#to_seadrop').hide();

    setTimeout(function () {
        document.getElementById('services').value = data.serviceType;

        document.getElementById('selected_date').value = data.pickup_date;
        document.getElementById('selected_time').value = data.pickup_time;

        document.getElementById('pick_point').value = data.pickuplocation;
        document.getElementById('drop_point').value = data.dropoff_location;

        document.getElementById('total_passenger').value = data.total_passenger;

        if(data.stopAddtress){
            stops = data.stopAddtress.split('@');
            if(stops.length > 0){
                document.getElementById('add_stop').checked = true;
                $('#stop_points').show();

                $(".aadMore").html('');
                var wrapper = $(".aadMore"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID

                var x = 1; //initlal text box count
                stops.forEach(function (item , i) { //on add input button click


                    //text box increment
                    //$('.aadMore').append('<div class="stopLocationfield"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>'); //add input box
                    $('.aadMore').append('<div class="cont'+(i+1)+' col-sm-12 mt-15 stopLocationfield"><label class="short-address" id="stopLocation'+(i+1)+'_name"></label><div class="input-group"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control" name="mytext[]"/><span id="div-' + (i+1) + '" class="input-group-addon bg-primary remove_field">X</span></div></div>'); //add input box


                    var stopeLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation' + (i+1)), options);
                    add_postal_code_with_address(stopeLocation, $('#stopLocation' + (i+1)));


                });
            }
        }

        maoGenerate(null);

        if(localStorage.getItem('noLuggage') === 'yes'){
            $('#lugSelector').trigger('click');

        }

        if(data.interNationFlightChecked === 'yes' || data.ismeetGreetUpdateChecked === 'yes' || data.iscurbsideUpdateChecked === 'yes'){
            $('#intFlt').show();
        }

        if(data.interNationFlightChecked === 'yes'){
            $('.holdername').show();
            $('.interNationFlight').show();
            $('#interNationFlightUpdateService').trigger('click');
        }
        if(data.ismeetGreetUpdateChecked === 'yes'){
            $('#meetAndGreet').show();
            $('.holdername').show();
            $('#meetGreetUpdateService').trigger('click');
            $('#curbSide').show();
        }
        if(data.iscurbsideUpdateChecked === 'yes'){
            $('#curbSide').show();
            $('.holdername').show();
            $('#meetAndGreet').show();
            $('#curbsideUpdateService').trigger('click');
        }


        try {
            existing_luggageInfo = JSON.parse(window.localStorage.getItem("luggageJson"));
            document.getElementById('luggage_quantity_small').value = existing_luggageInfo['small'];
            document.getElementById('luggage_quantity_medium').value = existing_luggageInfo['medium'];
            document.getElementById('luggage_quantity_large').value = existing_luggageInfo['large'];
        }
        catch (e) {

        }

show_map_name_value();
    } , 800)


}

function reload_SEAA(data){
    var stops = [], existing_luggageInfo = {};

    $("#searchRateForm :input").prop("disabled", false);
    $('#map_loaction').show();
    //$('#location_map').show();
    $('#stop_point').show();
    $('#drop_charte_loc').hide();
    $('#pick_hourly_loc').hide();


    $('#to_train_pickup_location').hide();
    $('#to_train_dropof_location').hide();

    $('#from_perpassenger_point').hide();
    $('#to_perpassenger_point').hide();
    $('#from_pont_to_point').hide();
    $('#drop_point_loc').hide();
    $('#from_airport_drop').hide();
    $('#from_airport_pickup').hide();
    $('#to_airport_pickup').hide();
    $('#to_airport_drop').hide();
    $('#pick_from_seaport').show();
    $('#from_seapick').show();
    $('#from_seadrop').show();
    $('#to_seapick').hide();
    $('#pick_charter_loc').hide();
    $('#drop_hourly_loc').hide();
    $('#to_seadrop').hide();
    $('#triphr').hide();
    $('#from_train_pickup_location').hide();
    $('#from_train_dropof_location').hide();


    /* validation start here */
    $('#pick_point').prop("required", false);
    $('#drop_point').prop("required", false);
    $('#from_airport_pickloc').prop("required", false);
    $('#from_airport_droploc').prop("required", false);
    $('#to_airport_pickloc').prop("required", false);
    $('#to_airport_droploc').prop("required", false);
    $('#to_train_pickup_location_input').prop("required", false);
    $('#to_train_dropof_location_input').prop("required", false);


    $('#from_train_pickup_location_input').prop("required", false);
    $('#from_train_dropof_location_input').prop("required", false);

    $('#perpassenger_pickup_location').prop("required", false);
    $('#perpassenger_dropof_location').prop("required", false);
    $('#pick_to_seaport').prop("required", false);
    $('#drop_to_seaport').prop("required", false);
    $('#pick_hourly').prop("required", false);
    $('#drop_hourly').prop("required", false);
    $('#pick_charter').prop("required", false);
    $('#drop_charter').prop("required", false);
    $('#pick_from_seaport').prop("required", true);
    $('#drop_from_seaport').prop("required", true);

    setTimeout(function () {
        document.getElementById('services').value = data.serviceType;

        document.getElementById('selected_date').value = data.pickup_date;
        document.getElementById('selected_time').value = data.pickup_time;

        document.getElementById('pick_from_seaport').value = data.pickuplocation;
        document.getElementById('drop_from_seaport').value = data.dropoff_location;

        document.getElementById('total_passenger').value = data.total_passenger;

        if(data.stopAddtress){
            stops = data.stopAddtress.split('@');
            if(stops.length > 0){
                document.getElementById('add_stop').checked = true;
                $('#stop_points').show();

                $(".aadMore").html('');
                var wrapper = $(".aadMore"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID

                var x = 1; //initlal text box count
                stops.forEach(function (item , i) { //on add input button click


                    //text box increment
                    //$('.aadMore').append('<div class="stopLocationfield"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>'); //add input box
                    $('.aadMore').append('<div class="cont'+(i+1)+' col-sm-12 mt-15 stopLocationfield"><label class="short-address" id="stopLocation'+(i+1)+'_name"></label><div class="input-group"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control" name="mytext[]"/><span id="div-' + (i+1) + '" class="input-group-addon bg-primary remove_field">X</span></div></div>'); //add input box


                    var stopeLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation' + (i+1)), options);
                    add_postal_code_with_address(stopeLocation, $('#stopLocation' + (i+1)));


                });
            }
        }

        maoGenerate(null);

        if(localStorage.getItem('noLuggage') === 'yes'){
            $('#lugSelector').trigger('click');

        }

        if(data.interNationFlightChecked === 'yes' || data.ismeetGreetUpdateChecked === 'yes' || data.iscurbsideUpdateChecked === 'yes'){
            $('#intFlt').show();
        }

        if(data.interNationFlightChecked === 'yes'){
            $('.holdername').show();
            $('.interNationFlight').show();
            $('#interNationFlightUpdateService').trigger('click');
        }
        if(data.ismeetGreetUpdateChecked === 'yes'){
            $('#meetAndGreet').show();
            $('.holdername').show();
            $('#meetGreetUpdateService').trigger('click');
            $('#curbSide').show();
        }
        if(data.iscurbsideUpdateChecked === 'yes'){
            $('#curbSide').show();
            $('.holdername').show();
            $('#meetAndGreet').show();
            $('#curbsideUpdateService').trigger('click');
        }


        try {
            existing_luggageInfo = JSON.parse(window.localStorage.getItem("luggageJson"));
            document.getElementById('luggage_quantity_small').value = existing_luggageInfo['small'];
            document.getElementById('luggage_quantity_medium').value = existing_luggageInfo['medium'];
            document.getElementById('luggage_quantity_large').value = existing_luggageInfo['large'];
        }
        catch (e) {

        }

show_map_name_value();
    } , 800)
}

function reload_SEAD(data){
    var stops = [], existing_luggageInfo = {};

    $("#searchRateForm :input").prop("disabled", false);
    $('#map_loaction').show();
    //$('#location_map').show();
    $('#stop_point').show();
    $('#drop_charte_loc').hide();
    $('#from_pont_to_point').hide();
    $('#drop_point_loc').hide();
    $('#from_airport_drop').hide();
    $('#from_airport_pickup').hide();

    $('#to_train_pickup_location').hide();
    $('#to_train_dropof_location').hide();

    $('#from_perpassenger_point').hide();
    $('#to_perpassenger_point').hide();


    $('#to_airport_pickup').hide();
    $('#to_airport_drop').hide();
    $('#pick_charter_loc').hide();
    $('#drop_hourly_loc').hide();
    $('#from_seapick').hide();
    $('#to_seapick').show();
    $('#pick_hourly_loc').hide();
    $('#to_seadrop').show();
    $('#from_seadrop').hide();
    $('#pick_from_seaport').hide();
    $('#triphr').hide();
    $('#from_train_pickup_location').hide();
    $('#from_train_dropof_location').hide();


    /* validation start here */
    $('#pick_point').prop("required", false);
    $('#drop_point').prop("required", false);
    $('#from_airport_pickloc').prop("required", false);
    $('#from_airport_droploc').prop("required", false);
    $('#to_airport_pickloc').prop("required", false);
    $('#to_train_pickup_location_input').prop("required", false);
    $('#to_train_dropof_location_input').prop("required", false);

    $('#perpassenger_pickup_location').prop("required", false);
    $('#perpassenger_dropof_location').prop("required", false);

    $('#from_train_pickup_location_input').prop("required", false);
    $('#from_train_dropof_location_input').prop("required", false);

    $('#to_airport_droploc').prop("required", false);
    $('#pick_from_seaport').prop("required", false);
    $('#drop_from_seaport').prop("required", false);
    $('#pick_hourly').prop("required", false);
    $('#drop_hourly').prop("required", false);
    $('#pick_charter').prop("required", false);
    $('#drop_charter').prop("required", false);
    $('#pick_to_seaport').prop("required", true);
    $('#drop_to_seaport').prop("required", true);

    setTimeout(function () {
        document.getElementById('services').value = data.serviceType;

        document.getElementById('selected_date').value = data.pickup_date;
        document.getElementById('selected_time').value = data.pickup_time;

        document.getElementById('pick_to_seaport').value = data.pickuplocation;
        document.getElementById('drop_to_seaport').value = data.dropoff_location;

        document.getElementById('total_passenger').value = data.total_passenger;

        if(data.stopAddtress){
            stops = data.stopAddtress.split('@');
            if(stops.length > 0){
                document.getElementById('add_stop').checked = true;
                $('#stop_points').show();

                $(".aadMore").html('');
                var wrapper = $(".aadMore"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID

                var x = 1; //initlal text box count
                stops.forEach(function (item , i) { //on add input button click


                    //text box increment
                    //$('.aadMore').append('<div class="stopLocationfield"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>'); //add input box
                    $('.aadMore').append('<div class="cont'+(i+1)+' col-sm-12 mt-15 stopLocationfield"><label class="short-address" id="stopLocation'+(i+1)+'_name"></label><div class="input-group"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control" name="mytext[]"/><span id="div-' + (i+1) + '" class="input-group-addon bg-primary remove_field">X</span></div></div>'); //add input box


                    var stopeLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation' + (i+1)), options);
                    add_postal_code_with_address(stopeLocation, $('#stopLocation' + (i+1)));


                });
            }
        }

        maoGenerate(null);

        if(localStorage.getItem('noLuggage') === 'yes'){
            $('#lugSelector').trigger('click');

        }

        if(data.interNationFlightChecked === 'yes' || data.ismeetGreetUpdateChecked === 'yes' || data.iscurbsideUpdateChecked === 'yes'){
            $('#intFlt').show();
        }

        if(data.interNationFlightChecked === 'yes'){
            $('.holdername').show();
            $('.interNationFlight').show();
            $('#interNationFlightUpdateService').trigger('click');
        }
        if(data.ismeetGreetUpdateChecked === 'yes'){
            $('#meetAndGreet').show();
            $('.holdername').show();
            $('#meetGreetUpdateService').trigger('click');
            $('#curbSide').show();
        }
        if(data.iscurbsideUpdateChecked === 'yes'){
            $('#curbSide').show();
            $('.holdername').show();
            $('#meetAndGreet').show();
            $('#curbsideUpdateService').trigger('click');
        }


        try {
            existing_luggageInfo = JSON.parse(window.localStorage.getItem("luggageJson"));
            document.getElementById('luggage_quantity_small').value = existing_luggageInfo['small'];
            document.getElementById('luggage_quantity_medium').value = existing_luggageInfo['medium'];
            document.getElementById('luggage_quantity_large').value = existing_luggageInfo['large'];
        }
        catch (e) {

        }

show_map_name_value();
    } , 800)
}

function reload_HRLY(data) {
    var stops = [], existing_luggageInfo = {};

    $("#searchRateForm :input").prop("disabled", false);
    $('#map_loaction').show();
    $('#drop_charte_loc').hide();
    $('#stop_point').show();
    $('#pick_charter_loc').hide();
    $('#from_pont_to_point').hide();
    $('#drop_point_loc').hide();
    $('#from_airport_drop').hide();

    $('#to_train_pickup_location').hide();
    $('#to_train_dropof_location').hide();


    $('#from_perpassenger_point').hide();
    $('#to_perpassenger_point').hide();

    $('#from_airport_pickup').hide();
    $('#to_airport_pickup').hide();
    $('#to_airport_drop').hide();
    $('#pick_from_seaport').hide();
    $('#triphr').show();
    $('#drop_hourly_loc').show();
    $('#pick_hourly_loc').show();
    $('#pick_hourly').show();
    $('#from_seapick').hide();
    $('#from_seadrop').hide();
    $('#to_seapick').hide();

    $('#to_seadrop').hide();
    $('#from_train_pickup_location').hide();
    $('#from_train_dropof_location').hide();

    /* validation start here */
    $('#pick_point').prop("required", false);
    $('#drop_point').prop("required", false);
    $('#from_airport_pickloc').prop("required", false);
    $('#from_airport_droploc').prop("required", false);
    $('#to_airport_pickloc').prop("required", false);
    $('#to_airport_droploc').prop("required", false);

    $('#to_train_pickup_location_input').prop("required", false);
    $('#to_train_dropof_location_input').prop("required", false);
    $('#perpassenger_pickup_location').prop("required", false);
    $('#perpassenger_dropof_location').prop("required", false);

    $('#pick_from_seaport').prop("required", false);
    $('#drop_from_seaport').prop("required", false);
    $('#pick_to_seaport').prop("required", false);
    $('#drop_to_seaport').prop("required", false);
    $('#pick_charter').prop("required", false);
    $('#drop_charter').prop("required", false);


    $('#from_train_pickup_location_input').prop("required", false);
    $('#from_train_dropof_location_input').prop("required", false);

    $('#pick_hourly').prop("required", true);
    $('#drop_hourly').prop("required", true);

    setTimeout(function () {
        document.getElementById('services').value = data.serviceType;

        document.getElementById('selected_date').value = data.pickup_date;
        document.getElementById('selected_time').value = data.pickup_time;

        document.getElementById('pick_hourly').value = data.pickuplocation;
        document.getElementById('drop_hourly').value = data.dropoff_location;

        document.getElementById('total_passenger').value = data.total_passenger;

        if(data.stopAddtress){
            stops = data.stopAddtress.split('@');
            if(stops.length > 0){
                document.getElementById('add_stop').checked = true;
                $('#stop_points').show();

                $(".aadMore").html('');
                var wrapper = $(".aadMore"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID

                var x = 1; //initlal text box count
                stops.forEach(function (item , i) { //on add input button click


                    //text box increment
                    //$('.aadMore').append('<div class="stopLocationfield"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>'); //add input box
                    $('.aadMore').append('<div class="cont'+(i+1)+' col-sm-12 mt-15 stopLocationfield"><label class="short-address" id="stopLocation'+(i+1)+'_name"></label><div class="input-group"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control" name="mytext[]"/><span id="div-' + (i+1) + '" class="input-group-addon bg-primary remove_field">X</span></div></div>'); //add input box


                    var stopeLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation' + (i+1)), options);
                    add_postal_code_with_address(stopeLocation, $('#stopLocation' + (i+1)));


                });
            }
        }

        maoGenerate(null);

        if(localStorage.getItem('noLuggage') === 'yes'){
            $('#lugSelector').trigger('click');

        }

        if(data.interNationFlightChecked === 'yes' || data.ismeetGreetUpdateChecked === 'yes' || data.iscurbsideUpdateChecked === 'yes'){
            $('#intFlt').show();
        }

        if(data.interNationFlightChecked === 'yes'){
            $('.holdername').show();
            $('.interNationFlight').show();
            $('#interNationFlightUpdateService').trigger('click');
        }
        if(data.ismeetGreetUpdateChecked === 'yes'){
            $('#meetAndGreet').show();
            $('.holdername').show();
            $('#meetGreetUpdateService').trigger('click');
            $('#curbSide').show();
        }
        if(data.iscurbsideUpdateChecked === 'yes'){
            $('#curbSide').show();
            $('.holdername').show();
            $('#meetAndGreet').show();
            $('#curbsideUpdateService').trigger('click');
        }


        try {
            existing_luggageInfo = JSON.parse(window.localStorage.getItem("luggageJson"));
            document.getElementById('luggage_quantity_small').value = existing_luggageInfo['small'];
            document.getElementById('luggage_quantity_medium').value = existing_luggageInfo['medium'];
            document.getElementById('luggage_quantity_large').value = existing_luggageInfo['large'];
        }
        catch (e) {

        }

show_map_name_value();
    } , 800)
}



function reload_FTS(data) {
    var stops = [], existing_luggageInfo = {};

    $("#searchRateForm :input").prop("disabled", false);
    $('#map_loaction').show()
    $('#stop_point').show();
    $('#triphr').hide();
    $('#from_airport_drop').hide();
    $('#from_airport_pickup').hide();
    $('#to_airport_pickup').hide();
    $('#to_airport_drop').hide();
    $('#pick_hourly_loc').hide();
    $('#pick_charter_loc').hide();
    $('#drop_charte_loc').hide();
    $('#drop_point_loc').hide();
    $('#from_pont_to_point').hide();
    $('#from_perpassenger_point').hide();
    $('#to_perpassenger_point').hide();


    $('#from_perpassenger_point').hide();
    $('#to_perpassenger_point').hide();


    $('#to_train_pickup_location').hide();
    $('#to_train_dropof_location').hide();

    $('#from_train_pickup_location').show();
    $('#from_train_dropof_location').show();


    /* validation applied start here */
    $('#from_airport_pickloc').prop("required", false);
    $('#from_airport_droploc').prop("required", false);
    $('#to_airport_pickloc').prop("required", false);
    $('#to_airport_droploc').prop("required", false);
    $('#pick_from_seaport').prop("required", false);
    $('#drop_from_seaport').prop("required", false);
    $('#pick_to_seaport').prop("required", false);


    $('#drop_to_seaport').prop("required", false);
    $('#pick_hourly').prop("required", false);
    $('#drop_hourly').prop("required", false);
    $('#pick_charter').prop("required", false);
    $('#drop_charter').prop("required", false);
    $('#pick_point').prop("required", false);
    $('#drop_point').prop("required", false);

    $('#perpassenger_pickup_location').prop("required", false);
    $('#perpassenger_dropof_location').prop("required", false);
    $('#to_train_pickup_location_input').prop("required", false);
    $('#to_train_dropof_location_input').prop("required", false);

    $('#from_train_pickup_location_input').prop("required", true);
    $('#from_train_dropof_location_input').prop("required", true);


    /* validation applied End here */


    $('#pick_hourly').hide();
    $('#from_seapick').hide();
    $('#from_seadrop').hide();
    $('#to_seapick').hide();
    $('#drop_hourly_loc').hide();
    $('#pick_from_seaport').hide();
    $('#to_seadrop').hide();

    setTimeout(function () {
        document.getElementById('services').value = data.serviceType;

        document.getElementById('selected_date').value = data.pickup_date;
        document.getElementById('selected_time').value = data.pickup_time;

        document.getElementById('from_train_pickup_location_input').value = data.pickuplocation;
        document.getElementById('from_train_dropof_location_input').value = data.dropoff_location;

        document.getElementById('total_passenger').value = data.total_passenger;

        if(data.stopAddtress){
            stops = data.stopAddtress.split('@');
            if(stops.length > 0){
                document.getElementById('add_stop').checked = true;
                $('#stop_points').show();

                $(".aadMore").html('');
                var wrapper = $(".aadMore"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID

                var x = 1; //initlal text box count
                stops.forEach(function (item , i) { //on add input button click


                    //text box increment
                    //$('.aadMore').append('<div class="stopLocationfield"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>'); //add input box
                    $('.aadMore').append('<div class="cont'+(i+1)+' col-sm-12 mt-15 stopLocationfield"><label class="short-address" id="stopLocation'+(i+1)+'_name"></label><div class="input-group"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control" name="mytext[]"/><span id="div-' + (i+1) + '" class="input-group-addon bg-primary remove_field">X</span></div></div>'); //add input box


                    var stopeLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation' + (i+1)), options);
                    add_postal_code_with_address(stopeLocation, $('#stopLocation' + (i+1)));


                });
            }
        }

        maoGenerate(null);

        if(localStorage.getItem('noLuggage') === 'yes'){
            $('#lugSelector').trigger('click');

        }

        if(data.interNationFlightChecked === 'yes' || data.ismeetGreetUpdateChecked === 'yes' || data.iscurbsideUpdateChecked === 'yes'){
            $('#intFlt').show();
        }

        if(data.interNationFlightChecked === 'yes'){
            $('.holdername').show();
            $('.interNationFlight').show();
            $('#interNationFlightUpdateService').trigger('click');
        }
        if(data.ismeetGreetUpdateChecked === 'yes'){
            $('#meetAndGreet').show();
            $('.holdername').show();
            $('#meetGreetUpdateService').trigger('click');
            $('#curbSide').show();
        }
        if(data.iscurbsideUpdateChecked === 'yes'){
            $('#curbSide').show();
            $('.holdername').show();
            $('#meetAndGreet').show();
            $('#curbsideUpdateService').trigger('click');
        }


        try {
            existing_luggageInfo = JSON.parse(window.localStorage.getItem("luggageJson"));
            document.getElementById('luggage_quantity_small').value = existing_luggageInfo['small'];
            document.getElementById('luggage_quantity_medium').value = existing_luggageInfo['medium'];
            document.getElementById('luggage_quantity_large').value = existing_luggageInfo['large'];
        }
        catch (e) {

        }

show_map_name_value();
    } , 800)
}

function reload_TTS(data) {
    var stops = [], existing_luggageInfo = {};

    $("#searchRateForm :input").prop("disabled", false);
    $('#map_loaction').show()
    $('#stop_point').show();
    $('#triphr').hide();
    $('#from_airport_drop').hide();
    $('#from_airport_pickup').hide();
    $('#to_airport_pickup').hide();
    $('#to_airport_drop').hide();
    $('#pick_hourly_loc').hide();
    $('#pick_charter_loc').hide();
    $('#drop_charte_loc').hide();
    $('#drop_point_loc').hide();
    $('#from_pont_to_point').hide();
    $('#from_perpassenger_point').hide();
    $('#to_perpassenger_point').hide();


    $('#from_perpassenger_point').hide();
    $('#to_perpassenger_point').hide();


    $('#from_train_pickup_location').hide();
    $('#from_train_dropof_location').hide();


    $('#to_train_pickup_location').show();
    $('#to_train_dropof_location').show();


    /* validation applied start here */
    $('#from_airport_pickloc').prop("required", false);
    $('#from_airport_droploc').prop("required", false);
    $('#to_airport_pickloc').prop("required", false);
    $('#to_airport_droploc').prop("required", false);
    $('#pick_from_seaport').prop("required", false);
    $('#drop_from_seaport').prop("required", false);
    $('#pick_to_seaport').prop("required", false);


    $('#drop_to_seaport').prop("required", false);
    $('#pick_hourly').prop("required", false);
    $('#drop_hourly').prop("required", false);
    $('#pick_charter').prop("required", false);
    $('#drop_charter').prop("required", false);
    $('#pick_point').prop("required", false);
    $('#drop_point').prop("required", false);

    $('#perpassenger_pickup_location').prop("required", false);
    $('#perpassenger_dropof_location').prop("required", false);


    $('#from_train_pickup_location_input').prop("required", false);
    $('#from_train_dropof_location_input').prop("required", false);

    $('#to_train_pickup_location_input').prop("required", true);
    $('#to_train_dropof_location_input').prop("required", true);

    /* validation applied End here */

    $('#pick_hourly').hide();
    $('#from_seapick').hide();
    $('#from_seadrop').hide();
    $('#to_seapick').hide();
    $('#drop_hourly_loc').hide();
    $('#pick_from_seaport').hide();
    $('#to_seadrop').hide();

    setTimeout(function () {
        document.getElementById('services').value = data.serviceType;

        document.getElementById('selected_date').value = data.pickup_date;
        document.getElementById('selected_time').value = data.pickup_time;

        document.getElementById('to_train_pickup_location_input').value = data.pickuplocation;
        document.getElementById('to_train_dropof_location_input').value = data.dropoff_location;

        document.getElementById('total_passenger').value = data.total_passenger;

        if(data.stopAddtress){
            stops = data.stopAddtress.split('@');
            if(stops.length > 0){
                document.getElementById('add_stop').checked = true;
                $('#stop_points').show();

                $(".aadMore").html('');
                var wrapper = $(".aadMore"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID

                var x = 1; //initlal text box count
                stops.forEach(function (item , i) { //on add input button click


                    //text box increment
                    //$('.aadMore').append('<div class="stopLocationfield"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>'); //add input box
                    $('.aadMore').append('<div class="cont'+(i+1)+' col-sm-12 mt-15 stopLocationfield"><label class="short-address" id="stopLocation'+(i+1)+'_name"></label><div class="input-group"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control" name="mytext[]"/><span id="div-' + (i+1) + '" class="input-group-addon bg-primary remove_field">X</span></div></div>'); //add input box


                    var stopeLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation' + (i+1)), options);
                    add_postal_code_with_address(stopeLocation, $('#stopLocation' + (i+1)));


                });
            }
        }

        maoGenerate(null);

        if(localStorage.getItem('noLuggage') === 'yes'){
            $('#lugSelector').trigger('click');

        }

        if(data.interNationFlightChecked === 'yes' || data.ismeetGreetUpdateChecked === 'yes' || data.iscurbsideUpdateChecked === 'yes'){
            $('#intFlt').show();
        }

        if(data.interNationFlightChecked === 'yes'){
            $('.holdername').show();
            $('.interNationFlight').show();
            $('#interNationFlightUpdateService').trigger('click');
        }
        if(data.ismeetGreetUpdateChecked === 'yes'){
            $('#meetAndGreet').show();
            $('.holdername').show();
            $('#meetGreetUpdateService').trigger('click');
            $('#curbSide').show();
        }
        if(data.iscurbsideUpdateChecked === 'yes'){
            $('#curbSide').show();
            $('.holdername').show();
            $('#meetAndGreet').show();
            $('#curbsideUpdateService').trigger('click');
        }


        try {
            existing_luggageInfo = JSON.parse(window.localStorage.getItem("luggageJson"));
            document.getElementById('luggage_quantity_small').value = existing_luggageInfo['small'];
            document.getElementById('luggage_quantity_medium').value = existing_luggageInfo['medium'];
            document.getElementById('luggage_quantity_large').value = existing_luggageInfo['large'];
        }
        catch (e) {

        }

show_map_name_value();
    } , 800)
}


function reload_PPS(data) {
    var stops = [], existing_luggageInfo = {};




    $("#searchRateForm :input").prop("disabled", false);
    $('#map_loaction').show()
    $('#stop_point').hide();
    $('#triphr').hide();
    $('#from_airport_drop').hide();
    $('#from_airport_pickup').hide();
    $('#to_airport_pickup').hide();
    $('#to_airport_drop').hide();
    $('#pick_hourly_loc').hide();
    $('#pick_charter_loc').hide();
    $('#drop_charte_loc').hide();
    $('#drop_point_loc').hide();

    $('#to_train_pickup_location').hide();
    $('#to_train_dropof_location').hide();

    $('#from_pont_to_point').hide();
    $('#from_perpassenger_point').hide();
    $('#to_perpassenger_point').hide();
    $('#from_train_pickup_location').hide();
    $('#from_train_dropof_location').hide();

    $('#from_perpassenger_point').show();
    $('#to_perpassenger_point').show();


    /* validation applied start here */
    $('#from_airport_pickloc').prop("required", false);
    $('#from_airport_droploc').prop("required", false);
    $('#to_airport_pickloc').prop("required", false);
    $('#to_airport_droploc').prop("required", false);
    $('#pick_from_seaport').prop("required", false);
    $('#drop_from_seaport').prop("required", false);
    $('#pick_to_seaport').prop("required", false);

    $('#to_train_pickup_location_input').prop("required", false);
    $('#to_train_dropof_location_input').prop("required", false);
    $('#from_train_pickup_location_input').prop("required", false);
    $('#from_train_dropof_location_input').prop("required", false);

    $('#drop_to_seaport').prop("required", false);
    $('#pick_hourly').prop("required", false);
    $('#drop_hourly').prop("required", false);
    $('#pick_charter').prop("required", false);
    $('#drop_charter').prop("required", false);
    $('#pick_point').prop("required", false);
    $('#drop_point').prop("required", false);

    $('#perpassenger_pickup_location').prop("required", true);
    $('#perpassenger_dropof_location').prop("required", true);

    /* validation applied End here */


    $('#pick_hourly').hide();
    $('#from_seapick').hide();
    $('#from_seadrop').hide();
    $('#to_seapick').hide();
    $('#drop_hourly_loc').hide();
    $('#pick_from_seaport').hide();
    $('#to_seadrop').hide();

    setTimeout(function () {
        document.getElementById('services').value = data.serviceType;

        document.getElementById('selected_date').value = data.pickup_date;


        document.getElementById('perpassenger_pickup_location').value = data.pickuplocation;
        document.getElementById('perpassenger_dropof_location').value = data.dropoff_location;

        setTimeout(function () {
            PPShuttle();
            $('.timePickerR').html('');



            setTimeout(function () {
                document.getElementById('selected_time').value = data.pickup_time;
                $('#selected_time').trigger('change');
                show_disclaimer_on_reload();
            } , 500)
        } , 200)

        document.getElementById('total_passenger').value = data.total_passenger;

        if(data.stopAddtress){
            stops = data.stopAddtress.split('@');
            if(stops.length > 0){
                document.getElementById('add_stop').checked = true;
                $('#stop_points').show();

                $(".aadMore").html('');
                var wrapper = $(".aadMore"); //Fields wrapper
                var add_button = $(".add_field_button"); //Add button ID

                var x = 1; //initlal text box count
                stops.forEach(function (item , i) { //on add input button click


                    //text box increment
                    //$('.aadMore').append('<div class="stopLocationfield"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>'); //add input box
                    $('.aadMore').append('<div class="cont'+(i+1)+' col-sm-12 mt-15 stopLocationfield"><label class="short-address" id="stopLocation'+(i+1)+'_name"></label><div class="input-group"><input type="text" value="' + item + '" id="stopLocation' + (i+1) + '" class="form-control" name="mytext[]"/><span id="div-' + (i+1) + '" class="input-group-addon bg-primary remove_field">X</span></div></div>'); //add input box


                    var stopeLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation' + (i+1)), options);
                    add_postal_code_with_address(stopeLocation, $('#stopLocation' + (i+1)));


                });
            }
        }

        maoGenerate(null);

        if(localStorage.getItem('noLuggage') === 'yes'){
            $('#lugSelector').trigger('click');

        }

        if(data.interNationFlightChecked === 'yes' || data.ismeetGreetUpdateChecked === 'yes' || data.iscurbsideUpdateChecked === 'yes'){
            $('#intFlt').show();
        }

        if(data.interNationFlightChecked === 'yes'){
            $('.holdername').show();
            $('.interNationFlight').show();
            $('#interNationFlightUpdateService').trigger('click');
        }
        if(data.ismeetGreetUpdateChecked === 'yes'){
            $('#meetAndGreet').show();
            $('.holdername').show();
            $('#meetGreetUpdateService').trigger('click');
            $('#curbSide').show();
        }
        if(data.iscurbsideUpdateChecked === 'yes'){
            $('#curbSide').show();
            $('.holdername').show();
            $('#meetAndGreet').show();
            $('#curbsideUpdateService').trigger('click');
        }


        try {
            existing_luggageInfo = JSON.parse(window.localStorage.getItem("luggageJson"));
            document.getElementById('luggage_quantity_small').value = existing_luggageInfo['small'];
            document.getElementById('luggage_quantity_medium').value = existing_luggageInfo['medium'];
            document.getElementById('luggage_quantity_large').value = existing_luggageInfo['large'];
        }
        catch (e) {

        }

show_map_name_value();
    } , 800)
}

var current_rate_info = window.localStorage.getItem("rateSetInformation");

try {

    if(localStorage.limo_editMode == "1"){
        current_rate_info = JSON.parse(current_rate_info);
        //console.log(current_rate_info);

        if(current_rate_info.serviceType === 'AIRA'){
            reload_airport_pickup(current_rate_info);

        }
        else if(current_rate_info.serviceType === 'AIRD'){
            reload_airport_drop_off(current_rate_info);

        }
        else if(current_rate_info.serviceType === 'PTP'){
            reload_ptp(current_rate_info);

        }
        else if(current_rate_info.serviceType === 'SEAA'){
            reload_SEAA(current_rate_info);

        }
        else if(current_rate_info.serviceType === 'SEAD'){
            reload_SEAD(current_rate_info);

        }
        else if(current_rate_info.serviceType === 'HRLY'){
            reload_HRLY(current_rate_info);

        }
        else if(current_rate_info.serviceType === 'FTS'){
            reload_FTS(current_rate_info)

        }
        else if(current_rate_info.serviceType === 'TTS'){
            reload_TTS(current_rate_info);

        }

        else if(current_rate_info.serviceType === 'PPS'){
            reload_PPS(current_rate_info);
        }
    }







}
catch (e) {

}


