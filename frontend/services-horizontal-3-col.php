<?php
$_social = file_get_contents('https://dclimolinks.com/QandRAuth/SMA/phpfile/client.php?action=getSocialInfo&userID=25');
$_social = json_decode($_social, true);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script>
        if (localStorage.sScript !== undefined) {
            delete localStorage.sScript;
            window.location.href = 'select-vehicle.php';
        }
    </script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <link href="css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css"/>
    <!--    <link rel="stylesheet" type="text/css" href="css/jquery.simple-dtpicker.css"/>-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="css/style-new.css">
<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"-->
<!--          rel="stylesheet">-->


    <title>DC Limo Links</title>

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=<?php echo $_social['google_map'] ?>&libraries=places"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="PageJs/config_comman.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/moment.js" type="text/javascript"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
<!--    <script src="js/jquery.simple-dtpicker.js" type="text/javascript"></script>-->

    <!--<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>-->
    <!-- <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script> -->

    <!-- <script src="js/bootstrap-datepicker.js"></script> -->
<!--    <script src="js/bootstrap-datetimepicker.js" type="text/javascript"></script>-->
    <!-- <script src="js/jquery.datetimepicker.js"></script> -->

    <script src="js/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
    <script src="js/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="js/bootstrap-datepicker-widget.js" type="text/javascript"></script>

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

    <style>
        .pac-container {
            z-index: 5000;
        }

        .input-error{
            border : 1.5px solid red;
        }
    </style>
</head>
<body>
<div id="comp_id" style="display:none;">rjd6u1</div>



<div class="container">

    <input type="hidden" name="mini_service" id="mini_service" value="frontend/services-horizontal-3-col.php">
<!--    <input type="hidden" name="luggage_quantity" id="luggage_quantity" value="0" >-->
<!--    <input type="hidden" name="luggage_quantity_small" id="luggage_quantity_small" value="0">-->
<!--    <input type="hidden" name="luggage_quantity_medium" id="luggage_quantity_medium" value="0">-->
<!--    <input type="hidden" name="luggage_quantity_large" id="luggage_quantity_large" value="0">-->
<!--    <input type="hidden" name="total_passenger" id="total_passenger" value="0">-->
    <p class="ride-info" >
        Step 1: Ride Info
    </p>
    <form action="" id="searchRateForm">
        <div class="row">

            <div class="col-xs-12">


                <div class="col-xs-4">
                    <div class="form-group">
                        <div class="input-wrapper">
                            <label for="">Select Service Type</label>
                            <select class="form-control valid input-select" id="services">

                            </select>
                            <div class="alert alert-danger fade in" id="commanDisclaimerMsg"
                                 style="display:none"><a href="#" class="close" data-dismiss="alert"
                                                         aria-label="close">&times;</a> <strong>
                                    <span class="glyphicon glyphicon-warning-sign"></span> </strong>
                            </div>
                            <div class="alert alert-danger fade in" id="CutoffTimeMain"
                                 style="display:none"><a href="#" class="close" data-dismiss="alert"
                                                         aria-label="close">&times;</a> <strong>
                                    <span class="glyphicon glyphicon-warning-sign"></span> </strong>
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <div class="input-wrapper">
                                    <label for="">Pick-Up Date</label>
                                    <div class='input-group date' id='datepicker1'>

                                        <input type="text" class="form-control customInput date myheight"
                                               id="selected_date" name="selected_date" autocomplete="off" placeholder="Pick-Up Date"
                                               style="border-top-right-radius: 0px;
                                                           border-bottom-right-radius: 0px; width:100%; "/>

                                        <span class="input-group-addon inputbg" id="calender_btn">
                                                        <span class="glyphicon glyphicon-calendar"></span> </span></div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-6 timePickerR">
                                <div class="input-wrapper">
                                    <label for="">Pick-Up Time</label>
                                    <input type="text" class="form-control input-text" id="selected_time" required
                                           name="selected_time" autocomplete="off" placeholder="Pick-Up Time">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">


                        <!--  From  Airport pickup start -->
                        <div class="row" id="from_airport_pickup" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">PickUp Airport</label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input" placeholder="PickUp Airport"
                                           id="from_airport_pickloc"
                                           name="from_airport_pickloc" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <!--  From  Airport pickup end -->


                        <!--  To  Airport pickup location start -->

                        <div class="row" id="to_airport_pickup">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Pickup Location</label>

                                    <label class="short-address" id="to_airport_pickloc_name"></label>
                                    <input onchange="maoGenerate(null)" type="text"
                                           class="form-control input-text ui-autocomplete-input"
                                           placeholder="Pickup Location" id="to_airport_pickloc"
                                           name="to_airport_pickloc" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  To  Airport pickup location end -->


                        <!--  Point-to-point pickup location start -->

                        <div class="row" id="from_pont_to_point" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Pickup Location</label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input" id="pick_point"
                                           name="pick_point" placeholder="Pickup Location"
                                           autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  Point-to-point pickup location end -->


                        <!--  From  Seaport pickup start -->
                        <div class="row" id="from_seapick" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">PickUp Seaport</label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input"
                                           id="pick_from_seaport"
                                           name="pick_from_seaport" placeholder="PickUpSeaport"
                                           autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <!--  From  Seaport pickup end -->


                        <!--  From  Seaport pickup location start -->

                        <div class="row" id="to_seapick" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Pickup Location</label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input"
                                           id="pick_to_seaport"
                                           placeholder="Pickup Location"
                                           name="pick_to_seaport" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  From  Seaport pickup location end -->


                        <!--  Hourly pickup location start -->

                        <div class="row" id="pick_hourly_loc" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Pickup/Start Location</label>
                                    <label class="short-address" id="pick_hourly_name"></label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input"
                                           id="pick_hourly"
                                           name="pick_hourly" placeholder="Pickup Location"
                                           autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  Hourly pickup location end -->

                        <!--  From  Train pickup location start -->

                        <div class="row" id="from_train_pickup_location" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Pickup Train</label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input"
                                           id="from_train_pickup_location_input"
                                           name="from_train_pickup_location_input"
                                           placeholder="Pickup Location" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  From  Train pickup location end -->

                        <!--  To  Train pickup location start -->

                        <div class="row" id="to_train_pickup_location" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Pickup Location</label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input"
                                           id="to_train_pickup_location_input"
                                           name="to_train_pickup_location_input"
                                           placeholder="Pickup Location" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  To  Train pickup location end -->


                        <!-- perpassenger pickup location start -->

                        <div class="row" id="from_perpassenger_point" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Pickup Location</label>
                                    <select class="form-control" id="perpassenger_pickup_location" placeholder="Pickup Location"
                                            name="perpassenger_pickup_location"></select>
                                </div>
                            </div>
                        </div>

                        <!-- perpassenger pickup location end -->


                        <div class="row" id="pick_charter_loc" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Pickup/Start Location</label>
                                    <select class="form-control" id="pick_charter"
                                            name="pick_charter" placeholder="Pickup Location"
                                            autocomplete="off"></select>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="form-group" id="stop_point">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <h4>Making Stops?</h4>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 mt-10">
                                <label class="radio-inline">
                                    <input type="radio" name="add_stop" id="add_stop" value="yes">Yes
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="add_stop" id="add_stop1" value="no" checked="checked">No
                                </label>
                            </div>
                        </div>
                    </div>

                    <p id="show_stops" style="display: none">

                    </p>


                    <div class="form-group">
                        <!--  From  Airport Drop-off start -->
                        <div class="row" id="from_airport_drop">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Drop-off Location</label>
                                    <label class="short-address" id="from_airport_droploc_name"></label>
                                    <input type="text"
                                           class="form-control input-text ui-autocomplete-input myheight changeatt"
                                           id="from_airport_droploc" name="from_airport_droploc"
                                           onchange="maoGenerate(null)"
                                           placeholder="Drop-off Location" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <!--  From  Airport Drop-off end -->

                        <!--  To  Airport Drop-off location start -->

                        <div class="row" id="to_airport_drop" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Drop-off Airport</label>
                                    <input type="text"
                                           class="form-control input-text ui-autocomplete-input myheight changeatt"
                                           id="to_airport_droploc" name="to_airport_droploc"
                                           onchange="maoGenerate(null)"
                                           placeholder="Drop-off Airport" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  To  Airport Drop-off location end -->


                        <!--  Point-to-point Drop-off location start -->

                        <div class="row" id="drop_point_loc" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Drop-off Location</label>
                                    <label class="short-address" id="drop_point_name"></label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input"
                                           id="drop_point"
                                           name="drop_point" autocomplete="off"
                                           onchange="maoGenerate(null)"
                                           placeholder="Drop-off Location">
                                </div>
                            </div>
                        </div>

                        <!--  Point-to-point Drop-off location end -->


                        <!--  To Seaport Drop-off location start -->

                        <div class="row" id="from_seadrop" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Drop-off Location</label>
                                    <label class="short-address" id="drop_from_seaport_name"></label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input"
                                           id="drop_from_seaport"
                                           onchange="maoGenerate(null)"
                                           placeholder="Drop-off Location"
                                           name="drop_from_seaport" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  To Seaport Drop-off location end -->


                        <!--  To Seaport Drop-off location start -->

                        <div class="row" id="to_seadrop" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Drop-off Seaport</label>
                                    <label class="short-address" id="drop_to_seaport_name"></label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input"
                                           id="drop_to_seaport"
                                           name="drop_to_seaport" placeholder="Drop-off seaport"
                                           onchange="maoGenerate(null)"
                                           autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  To Seaport Drop-off location end -->

                        <!--  Hourly Drop-off location start -->

                        <div class="row" id="drop_hourly_loc" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Drop-off/End Location</label>
                                    <label class="short-address" id="drop_hourly_name"></label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input"
                                           id="drop_hourly"
                                           onchange="maoGenerate(null)"
                                           name="drop_hourly" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  Hourly Drop-off location end -->

                        <!--  From Train Drop-off location start -->

                        <div class="row" id="from_train_dropof_location" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Drop-off/End Location</label>
                                    <label class="short-address" id="from_train_dropof_location_input_name"></label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input"
                                           onchange="maoGenerate(null)"
                                           id="from_train_dropof_location_input"
                                           placeholder="Drop-off Location"
                                           name="from_train_dropof_location_input" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  From Train Drop-off location end -->


                        <!--  From Train Drop-off location start -->

                        <div class="row" id="to_train_dropof_location" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Drop-off Train</label>
                                    <label class="short-address" id="to_train_dropof_location_input_name"></label>
                                    <input type="text" class="form-control input-text ui-autocomplete-input"
                                           onchange="maoGenerate(null)"
                                           id="to_train_dropof_location_input"
                                           placeholder="Drop-off Train"
                                           name="to_train_dropof_location_input" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <!--  From Train Drop-off location end -->


                        <!-- to perpassenger end point start -->

                        <div class="row" id="to_perpassenger_point" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Drop-off Location</label>
                                    <label class="short-address" id="perpassenger_dropof_location_name"></label>
                                    <select type="text" class="form-control" id="perpassenger_dropof_location"
                                            onchange="maoGenerate(null)"
                                            placeholder="Drop-off Location"
                                            name="perpassenger_dropof_location"></select>
                                </div>
                            </div>
                        </div>

                        <!-- to perpassenger end point end -->

                        <div class="row" id="drop_charte_loc" style="display:none">
                            <div class="col-md-12">
                                <div class="input-wrapper">
                                    <label for="">Drop-off Location</label>
                                    <label class="short-address" id="drop_charter_name"></label>
                                    <input type="text" class="form-control" id="drop_charter"
                                           placeholder="Drop-off Location"
                                           onchange="maoGenerate(null)"
                                           name="drop_charter">
                                </div>
                            </div>
                        </div>

                        <!-- to perpassenger end point start -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-wrapper" id="PPS-time-picker">

                                </div>
                            </div>
                        </div>

                        <!-- to perpassenger end point end -->
                    </div>

            </div>



                <div class="col-xs-4">


                    <div class="form-group" >
                        <div class="row">
                            <div class="col-md-10">
                                <label for="PassengerNumber">How Many Passengers</label>
                                <div class="btn-custom-group">
                                    <!--                                <a href="Javascript:void(0)" class="btn btn-default">-</a>-->
                                    <input type="text" id="total_passenger" name="total_passenger"
                                           class="form-control input-btn-group-text" value="1">
                                    <!--                                <a href="Javascript:void(0)" class="btn btn-default">+</a>-->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group section-border" >
                        <div class="row ">
                            <!--                        <div class="col-xs-2 col-sm-1 col-md-1"></div>-->
                            <div class="col-xs-10 col-sm-8 col-md-8">
                                <label class="lblfont">Luggage Details</label>
                                <a href="#" id="luggageDetails-info" data-toggle="popover"
                                   data-placement="right" data-trigger="hover"
                                   data-content="Please try and be as accurate as possible so we could properly evaluate your vehicle type selection and avoid luggage capacity issues"
                                   data-original-title="" title=""><img src="images/info.png"
                                                                        alt="Luggage Capacity details info"
                                                                        style="height:16px; margin-left:3px; margin-bottom: 4px;"></a>

                                <label for="lugSelector" class="luggage-checkbox">
                                    <input type="checkbox" name="lugSelector" id="lugSelector"
                                           onchange="changeLugSelector(this)"> I don't have any luggage
                                </label>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-4 mt-5">
                                <div class="input-group input-group-lg bootstrap-touchspin inputwidth1">
                                    <span class="input-group-addon bootstrap-touchspin-prefix"> </span>
                                    <span class="input-group-btn">
          <button class="btn btn-default bootstrap-touchspin-down" type="button" style="pointer-events: none;"> <span
                      class=" glyphicon glyphicon-briefcase fa-1x"
                      style="width: 24px; font-size: 11px"></span>S </button>
          </span>
                                    <input id="luggage_quantity_small" type="text" value=""
                                           name="luggage_quantity_small"
                                           style="display: block; border-radius:6px; " class="form-control">
                                    <span class="input-group-addon bootstrap-touchspin-postfix"
                                          style="display: none;"></span> <span
                                            class="input-group-btn positionleftsmall"> <span
                                                class="icon-plus"></span> </span></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 mt-5">

                                <div class="input-group input-group-lg bootstrap-touchspin inputwidth1">
                                    <span class="input-group-addon bootstrap-touchspin-prefix"> </span>
                                    <span class="input-group-btn">
          <button class="btn btn-default bootstrap-touchspin-down" type="button" style="pointer-events: none;"> <span
                      class=" glyphicon glyphicon-briefcase fa-2x"
                      style="width: 22px; font-size: 13px"></span>M </button>
          </span>
                                    <input id="luggage_quantity_medium" type="text" value=""
                                           name="luggage_quantity_medium"
                                           style="display: block; border-radius:6px; " class="form-control">
                                    <span class="input-group-addon bootstrap-touchspin-postfix"
                                          style="display: none;"></span> <span
                                            class="input-group-btn positionleftsmall"> <span
                                                class="icon-plus"></span> </span></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 mt-5">


                                <div class="input-group input-group-lg bootstrap-touchspin inputwidth1">
                                    <span class="input-group-addon bootstrap-touchspin-prefix"> </span><span
                                            class="input-group-btn">
          <button class="btn btn-default bootstrap-touchspin-down" type="button" style="pointer-events: none;"> <span
                      class=" glyphicon glyphicon-briefcase fa-3x"
                      style="width: 22px; font-size: 15px"></span> L</button>
          </span>
                                    <input id="luggage_quantity_large" type="text" value=""
                                           name="luggage_quantity_large"
                                           style="display: block; border-radius:6px; " class="form-control">
                                    <span class="input-group-addon bootstrap-touchspin-postfix"
                                          style="display: none;"></span> <span
                                            class="input-group-btn"> <span class="icon-plus"></span> </span>
                                </div>
                            </div>
                        </div>

                        <p id="luggage_pick_error" style="display: none; color: red; font-weight: bold">
                            Please Enter Atleast one Small or Medium or Large Luggage
                        </p>
                    </div>

                    <div class="form-group" >
                        <div class="row">
                            <div class="col-md-10" id="intFlt" style="display:none;">
                                <label>Pick options</label>
                                <div class="input-wrapper">
                                    <label class="interNationFlight" style="display:none;">
                                        <input type="checkbox" type="checkbox" id="interNationFlightUpdateService" value="">
                                        International Flights? <a href="#" data-toggle="popover"
                                                                  id="international_disclaimer"
                                                                  data-placement="right" data-trigger="hover"
                                                                  data-content="Some content here">
                                            <i class="fa fa-info-circle fa-custom-blue"></i>
                                        </a>
                                    </label>
                                    <label class="block-element" id="curbSide" style="display:none;">
                                        <input type="radio" id="curbsideUpdateService" name="optradio"
                                               class="block-radio-element"> Curbside
                                        <a href="#" id="curbside_disclaimer" data-toggle="popover"
                                           data-placement="right" data-trigger="hover"
                                           data-content="Some content here">
                                            <i class="fa fa-info-circle fa-custom-blue"></i>
                                        </a>

                                    </label>
                                    <label class="block-element " id="meetAndGreet" style="display:none;">
                                        <input type="radio" id="meetGreetUpdateService" name="optradio"
                                               class="block-radio-element"> Meet and Greet

                                        <a href="#" id="meetAndGreet_disclaimer"
                                           data-toggle="popover" data-placement="right"
                                           data-trigger="hover"
                                           data-content="Some content here">
                                            <i class="fa fa-info-circle fa-custom-blue"></i>
                                        </a>


                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row" id="luggage_disclaimer">

                            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                <div class="alert alert-info" style="white-space: pre-wrap" id="luggage_disclaimer_message">

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary btn-flat" style="width: 100%">Quote and Reseve</button>
                    </div>

                </div>

                <div class="col-xs-4">
                        <div class="row " >
                            <div style="margin: 0px; padding: 10px 20px 0px;width: 100%;display:inline-block">
                                <div id="map-container" style="width: 100%; height: 400px"></div>
                            </div>
                        </div>
                </div>

        </div>








        </div>
    </form>

    <input type="hidden" id="mapShown" value="no">
    <input type="hidden" id="mapType" value="desktop">
</div>

<!-- Latest compiled and minified JavaScript -->


<script type="text/javascript">

    //	 		 $('#pick_hourly').keypress(function(){
    //
    //	 		 alert($(this).val());
    //	 		 	console.log($(this).val());
    //
    //	 		 })


    $(function () {

        var new_trip = localStorage.getItem('new_trip');
        if (new_trip != 'yes') {

            localStorage.removeItem('new_trip');
        }


        $('#selected_time').focus(function () {
            //alert('ok');
            var i = 0;

            $("#set_time").remove();
            //if($('tr').attr("id")!="set_time")

            if (i == 0) {
                $('.table-condensed tbody').append('<tr id="set_time"><td colspan="5"><button type="button" id="set_time_btn" class="btn btn-primary btn-block btn-timepicker-close" data-action="close">Set Time</button></td><tr>');

                i++;
            }
            $('#set_time_btn').hide();


        });


        $('#icon_time').click(function () {

            $('#selected_time').focus();
        });


        $('#selected_date').datetimepicker({
            format: 'MM/DD/YYYY',
            minDate: (new Date())
        });

        $('#selected_time').datetimepickerNew({
            pickDate : false,
            pickTime : true
        });


        $('#calender_btn').on('click', function () {

            $('#selected_date').focus();
        });


    });

    //if(document.getElementById('mini_service') == null){
        $("input[name='luggage_quantity']").TouchSpin({
            initval: 0
        });

        $("input[name='total_passenger']").TouchSpin({
            min: 1,
            initval: 0
        });

        $("input[name='luggage_quantity_small']").TouchSpin({
            initval: 0
        });
        $("input[name='luggage_quantity_medium']").TouchSpin({
            initval: 0
        });
        $("input[name='luggage_quantity_large']").TouchSpin({
            initval: 0
        });
    //}




    $(document).ready(function () {
        var msg = 'new content here';
        $('[data-toggle="popover"]').popover();

    });

</script>


<script type="text/javascript" src="PageJs/systemcheck.js"></script>
<!--<script src="js/customform.js" type="text/javascript"></script> -->
<script src="js/common.js?js=<?php echo uniqid() ?>" type="text/javascript"></script>
<script src="PageJs/services.js?js=<?php echo uniqid(); ?>" type="text/javascript"></script>
<script src="PageJs/comman_all.js" type="text/javascript"></script>


<script>
    function bind_input_with_google_search() {
        var element_count = $('.stopLocationfield').length;

        for (var ww = 1; ww <= element_count; ww++) {
            var stopeLocation = new google.maps.places.Autocomplete(document.getElementById('stopLocation' + ww), options);
            add_postal_code_with_address(stopeLocation, $('#stopLocation' + ww));
        }
    }

    $(document).ready(function () {
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".aadMore"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1;


        $(document).on('click', '.add_field_button', function (e) {

            if ($('.stopLocationfield').length > 0) {
                x = $('.stopLocationfield').length + 1;
            }

            console.log('count : ' + x);
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                //text box increment
                $('.aadMore').append('<div class="cont' + x + ' col-sm-12 mt-15 stopLocationfield"><div class="input-group"><input type="text" id="stopLocation' + x + '" class="form-control" name="mytext[]"/><span id="div-' + x + '" class="input-group-addon bg-primary remove_field">X</span></div></div>'); //add input box

            }

            bind_input_with_google_search();
        })

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            //$(this).parent('div').remove();

            var no = $(this).attr('id').split('-')[1];
            $('.cont' + no).remove();
            if ($('.stopLocationfield').html()) {


            } else {

                $('#add_stop').prop('checked', false);
                $('#add_stop1').prop('checked', 'checked');
                $('#stop_points').hide();
                $('.aadMore').html(' ');
                if(document.getElementById('trip_stops_modal')){
                    close_trip_stops_modal();
                }
            }
            bind_input_with_google_search();
        });
    });


</script>

<script src="PageJs/reload_existing_data.js" type="text/javascript"></script>

<!-- <script src="//maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places" type="text/javascript"></script> -->

</div>
</div>
</div>
</div>
</div>
</div>

<div class="modal fade" id="trip_stops_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add your stops</h4>
            </div>
            <div class="modal-body">
                <div class="form-group" style="display:none;" id="stop_points">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-wrapper">
                                <label for="">Enter Stop Location</label>
                                <div class="row aadMore">

                                </div>
                                <button class="btn btn-normal mt-10 btn-flat add_field_button">Add more fields</button>
                            </div>
                        </div>
                        <p id="stops_alert" style="color: red; padding-left: 17px">Please fill all the inputs</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"  id="close_stops_modal" >
                    Done
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="clearSessionModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Clear Session</h4>
            </div>
            <div class="modal-body">
                Your session is about to expire and you will be logged-out. <br>
                Do you want to extend session?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="expireSession()">Expire
                    Session
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="clearSession()">Extend
                    Session
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="authModal" onclick="event.stopPropagation()">
    <div class="modal-dialog authModalScreen modal-lg">
        <div class="modal-content" style="padding: 100px 0">

            <div class="box" style="margin: 0; display: inline-block; width: 100%">
                <br><br>
                <div>
                    <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                        <a href="./login.php" class="btn btn-primary" style="min-width: 120px">Login</a>
                        <p class="font1"><a href="login.php">Learn more &gt;&gt;</a></p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" style="min-width: 120px" type="button"
                                    data-toggle="dropdown">Register <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="personal_register.html">Personal Account</a></li>
                                <li><a href="corporate_register.html">Corporate Account</a></li>
                            </ul>
                        </div>
                        <p class="font1"><a href="">Learn more &gt;&gt;</a></p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                        <button type="button" class="btn btn-primary" style="min-width: 120px" data-dismiss="modal">
                            Continue
                        </button>
                        <p class="font1"><a href="">Learn more &gt;&gt;</a></p>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function clearSession() {
                setTimeout(function () {
                    if (localStorage.localUserInfo !== "guestinfo") {
                        $('#clearSessionModal').modal('show');
                    }
                }, 1000 * 60 * 15);
            }

            function expireSession() {
                var companyInfo = localStorage.companyInfo;
                localStorage.clear();
                localStorage.companyInfo = companyInfo;
                window.location.href = WEBSITE_URL + '/frontend/';
            }

            $(function () {
                clearSession();
                $('.dropdown-toggle').dropdown();
            });
        </script>

    </div>
</div>

</body>
</html>