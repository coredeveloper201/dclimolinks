<?php
$_social = file_get_contents('https://dclimolinks.com/QandRAuth/SMA/phpfile/client.php?action=getSocialInfo&userID=25');
$_social = json_decode($_social, true);

?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Payment</title>
    <link href="css/bootstrap.min.css"/>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link href="css/bootstrap-multiselect.css" rel="stylesheet">
    <link rel="stylesheet" href="build/css/intlTelInput.css"/>
    <link href="css/checkout.css" rel="stylesheet" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-datetimepicker.js"></script>
    <script src="js/bootstrap-multiselect.js"></script>
    <script src="js/jquery.bootstrap-touchspin.js"></script>
    <script src="PageJs/config_comman.js"></script>
    <script src="build/js/intlTelInput.js"></script>
    <script src="build/js/utils.js"></script>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=<?php echo $_social['google_map'] ?>&libraries=places"></script>
</head>
<style>
    .input-group-lg > .form-control, .input-group-lg > .input-group-addon, .input-group-lg > .input-group-btn > .btn {
        height: 36px;
        padding: 7px 17px;
        font-size: 18px;
        line-height: 0.333333;
        border-radius: 3px;
    }

    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 0px solid #ddd;
        font-weight: 500;
    }

    .btn-default {
        color: #333;
        background-color: #F3F3F3;
        border-color: #ccc;
    }

    .form-control {
        display: block;
        width: 100%;
        height: 35px;
        padding: 0px 10px;
        font-size: 16px;
        line-height: 1.42857143;
        color: #0076A3;
        background-color: #FFFFFF;
        background-image: none;
        border: 1px solid #C1C1C1;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }

    @media only screen and (max-width: 480px) {
        .form-control {
            display: block;
            width: 100%;
            height: 35px;
            padding: 6px 12px;
            font-size: 13px;
            line-height: 1.428571;
            color: #555;
            background-color: #F3F3F3;
            background-image: none;
            border: 1px solid #D6D6D6;
            border-radius: 0px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        .form-control1 {
            display: block;
            width: 100%;
            height: 35px;
            padding: 6px 14px;
            font-size: 13px;
            line-height: 1.428571;
            color: #555;
            background-color: #F3F3F3;
            background-image: none;
            border: 1px solid #D6D6D6;
            border-radius: 0px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        .form-control2 {
            display: block;
            width: 80%;
            height: 30px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.428571;
            color: #555;
            background-color: #F3F3F3;
            background-image: none;
            border: 1px solid #D6D6D6;
            border-radius: 0px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        .btn-primary3 {
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
            margin-top: 7px;
            margin-left: 2%;
        }

        .btn {
            display: inline-block;
            padding: 3px 9px;
            margin-bottom: 0;
            font-size: 11px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 0px;
        }

        .input-group-lg > .form-control, .input-group-lg > .input-group-addon, .input-group-lg > .input-group-btn > .btn {
            height: 40px;
            padding: 10px 14px;
            font-size: 19px;
            line-height: 0.333333;
            border-radius: 6px;
            border: 1px solid #CCC;
        }

        .btn1 {
            display: inline-block;
            padding: 3px 12px;
            margin-bottom: 0;
            font-size: 18px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 0px;
        }

        .width-input {
            width: 60%;
        }
    }

    .btn-shadow {
        border: rgba(165, 187, 203, 0.2) thin solid;
        border-radius: 3px;
        -webkit-box-shadow: 2px 5px 12px -6px rgba(0, 0, 0, 0.60);
        -moz-box-shadow: 2px 5px 12px -6px rgba(0, 0, 0, 0.60);
        box-shadow: 2px 5px 12px -6px rgba(0, 0, 0, 0.60);
        text-shadow: 1px 1px 2px rgba(11, 53, 90, 0.87);
    }

    .btn-primary {
        background: rgba(0, 0, 0, 0) linear-gradient(to bottom, rgb(13, 114, 150) 32%, rgba(59, 150, 184, 0.98) 69%) repeat scroll 0 0;
        border-radius: 2px;
        color: #fff;
        width: 100%;
        margin: 10px 0;
    }

    .navbar-default{
        border-color: #0077A2;
        border-radius: 0px;
    }

    @media (min-width: 768px){
        .navbar {
            border-radius: 0px;
        }
    }

    .top_header{
        background-color: #0077A2;
        min-height: 55px;
    }
    .font2 {
        text-align: right;
        padding: 18px 33px 9px 33px;
        font-size: 15px;
        color: #fff;
        font-weight: 600;
    }

    h4 {
        color: #000;
        font-weight: bold;
        font-size: 16px;
    }

    .title {
        color: #000;
        font-weight: bold;
        font-size: 14px;
    }

    .form-control3{
        border-radius: 0px;
    }

    .border-btm {
        border-bottom: 0px;
        margin-bottom: 20px;
    }

    .btn {
        min-width: 170px;
    }

</style>
<body>
<!--upper-content starts-->
<div class="head-main">
    <div class="navbar navbar-default top_header">
        <p class="font2"><span class="glyphicon glyphicon-user"></span>&nbsp;Welcome&nbsp;<span
                    class="ufname"></span>&nbsp; <a href="javascript:void(0)" style="color: white;"> <span
                        class="glyphicon glyphicon-log-out" id="logoutUserVehicle"></span></a></p>
    </div>
    <div class="container">
        <!--2nd row starts-->
        <div class="row" >

        <div class="col-md-12">


            <div class="row" >
                <div class="col-xs-12 col-sm-12 col-md-12" >
                    <p class="ride-info">
                        Step 3: Get Quote
                    </p>
                </div>
            </div>

            <!--2nd row ends-->
            <br>
            <br>
            <!--3rd row starts-->
            <div class="row">
                <div class="col-xs-1 col-sm-2 col-md-2"></div>
                <!--inner-container starts-->
                <div class="col-xs-12 col-sm-12 col-md-12">

                    <!--inner row starts-->
                    <div class="row">
                        <input type="hidden" name="get_quote_page" id="get_quote_page" value="yes" >
                        <!--Passenger Information starts-->
                        <div class="col-md-6">
                            <div class="headbox" style="margin-bottom: 15px">
                                <p><img src="images/passenger.png" alt=""/> <span
                                            class="headingpad">Passenger Details</span></p>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" id="passengerFirstName" tabindex="2"
                                           placeholder="First Name" required>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" id="passengerLastName" tabindex="2"
                                           placeholder="Last Name" required>
                                </div>
                            </div>
                            <div class="row" style="margin-top:2%;">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" id="passengerMobileNumber" tabindex="2"
                                           onkeyup="validPhoneNumber(this)"
                                           required>
                                    <div style="width: 100%;display: inline-block;" class="phoneValidation">

                                    </div>

                                    <!--<input type="tel" class="form-control-passengerdetail form-control alignLEFT"
                                           id="passengerMobileNumber" name="phoneNumber" autocomplete="off"
                                           data-mask="000 0000000" data-fv-field="phoneNumber" placeholder="091234 56789">-->
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <input type="text" class="form-control" id="passengerEmailName" tabindex="2"
                                           placeholder="Email Id" required>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 paddingLeft25">
                                            <button class="btn btn-primary btn-block btnwidth btn-shadow" id="riding_child_btn">
                                                <img
                                                        src="images/child_seat.png" alt="" height="20"/> Request Child Seat
                                            </button>
                                            <h5 class="marginbtm" id="childInformation" style="display:none;"><span
                                                        class="fontbold">&nbsp;<span id="childValueStorhere" class="h5"></span>&nbsp;&nbsp; <i
                                                            class="glyphicon glyphicon-edit" id="childSeatEdit"
                                                            style="cursor:pointer;"></i> &nbsp; <i
                                                            class="glyphicon glyphicon-trash" id="childSeatDelete"
                                                            style="cursor:pointer;"></i></span></h5>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 paddingRight25">
                                            <button class="btn btn-primary btn-block btnwidth btn-shadow" style="display: none" id="addPackage"><img
                                                        src="images/package.png" alt="" height="20"/> Add Limo Package
                                            </button>
                                            <h4 class="marginbtm" id="specialPackageItem"></h4>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="row" style="margin-top:2%; display:none">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <h5>Number Of Passengers</h5>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="input-group input-group-lg bootstrap-touchspin width-input">
                                        <span class="input-group-addon bootstrap-touchspin-prefix"></span>
                                        <span class="input-group-btn"></span>
                                        <input id="numberOfPassengerInput" type="text" value="" name="demo3_21"
                                               style="display: block; border-radius:3px; background-color: #F3F3F3;">
                                        <span
                                                class="input-group-addon bootstrap-touchspin-postfix"
                                                style="display: none;"></span><span class="input-group-btn"><span
                                                    class="icon-plus"></span></button></span>
                                    </div>

                                    <script>
                                        $("input[name='demo3_21']").TouchSpin({
                                            initval: 0
                                        });
                                    </script>
                                </div>
                            </div>

                            <br>


                            <!--  special request end here -->


                            <!--additional  Information starts-->
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <h4 class="fntcolor2 border-btm" style="">Additional
                                        Information</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                <textarea class="form-control3" id="otherComment" tabindex="8"
                                          placeholder="Other Comments or special request"></textarea>
                                </div>
                            </div>
                            <!--additional  Information ends-->
                            <!--Payment  Information starts-->


                            <!--payment  Information ends-->
                        </div>
                        <!--Passenger Information ends-->
                        <!--trip summary starts-->
                        <div class="col-md-6">
                            <div class="headbox">
                                <p><img src="images/trip_icon.png" alt=""/><span class="headingpad">Trip Summary</span></p>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">

                                    <div class="tripSummary1" style="">
                                        <div class="row" style="margin-top:6px">
                                            <div class="col-xs-6 col-sm-6 col-md-6 paddingLeft25">
                                                <h5 style="margin-bottom: 0px"><strong>Pickup Date:</strong> <span
                                                            class="pickupDate"></span></h5>
                                                <h5 style="margin-bottom: 0px;display: none;"><strong>Drop-Off Date:</strong> <span
                                                            class="dropOffDate"></span></h5>
                                            </div>
                                            <!--/.fontstyle-->
                                            <div class="col-xs-6 col-sm-6 col-md-6 paddingLeft25">
                                                <h5 style="margin-bottom: 0px"><strong>Pickup Time:</strong> <span
                                                            class="pickupTime"></span></h5>
                                                <h5 style="margin-bottom: 0px;display: none;"><strong>Drop-Off Time:</strong> <span
                                                            class="dropOffTime"></span></h5>
                                            </div>
                                            <!--/.fontstyle-->
                                        </div>
                                        <!--/.row-->
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 paddingLeft25">
                                                <h5 style="margin-bottom: 0px"><strong>Passenger Qty</strong>: <span
                                                            class="numberOfPassenger"> </span></h5>
                                            </div>
                                            <!--/.fontstyle-->
                                            <div class="col-xs-6 col-sm-6 col-md-6 paddingLeft25">
                                                <h5 style="margin-bottom: 0px"><strong>Luggage Qty</strong>: <span
                                                            class="vehicleLuggageCapacity"></span></h5>
                                            </div>
                                            <!--/.fontstyle-->
                                        </div>

                                        <div class="row marginrow selectedVeh" style="margin-left: 0px;margin-right: 0px;">
                                            <div class="col-xs-12 col-sm-12 col-md-12 text-center fontstyle">
                                                <h5 style="margin-bottom: 0px"><strong>Vehicle Type</strong>: <span class="pessangerVehicleTitle">3-Pax sedan</span></h5>
                                            </div>
                                            <!--/.fontstyle-->
                                            <div class="col-xs-12 col-sm-12 col-md-12 text-center"><img id="selected_vehicle" alt="" class="img-responsive marginauto" src="https://manage.mylimobiz.com/Uploads/2061/VehicleTypes/1/616201501426_hLCo2SQ118G03FU8HSdo.jpg">
                                            </div>
                                            <!--/.fontstyle-->
                                        </div>


                                    </div>
                                    <div class="routingDetails" style="margin-top:20px; padding-bottom:15px; margin-left: 0px;margin-right: 0px; padding-left: 10px">
                                        <div class="row" style="margin-top:6px">
                                            <div class="col-xs-12 col-sm-12 col-md-12 paddingLeft25" style="padding-bottom:10px;">
                                                <h5 style="margin-bottom: 0px"><img src="images/location_blue.png" alt=""> <span class="passengerPickupLocation">Miami International Airport(MIA)(Miami)</span></h5>
                                                <div class="ppRateIns Pick-up collapse" id="Pick-up" style="padding: 10px;
                                                                        border-left: 5px solid #f5f5f5;
                                                                        margin: 5px 19px;
                                                                        background: #fbfbfb;"></div>
                                            </div>
                                            <!--/.fontstyle-->
                                            <div class="passengerStopsPayment" style="padding-top:30px; padding-left:25px">

                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 paddingLeft25" style="margin-top: -15px;">
                                                <h5 style="margin-bottom: 0px"><img src="images/location_red.png" alt=""> <span class="passengerDropOffLocation">Miami Lakes, FL, USA</span></h5>
                                                <div class="ppRateIns Drop-off collapse" id="Drop-off" style="padding: 10px;
                                                                        border-left: 5px solid #f5f5f5;
                                                                        margin: 5px 19px;
                                                                        background: #fbfbfb;"></div>
                                            </div>
                                        </div>
                                        <!--/.fontstyle-->
                                    </div>
                                </div>
                            </div>
                            <!--promo code starts-->
                            <h4 class="fntcolor2 border-btm" style="display:none">Promo Code</h4>
                            <div class="row" style="display:none">
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    <input type="text" class="form-control4" id="promocode" placeholder="Enter Promo code"
                                           required>
                                    <h4>
                                        <button type="button" class="btn btn-prim">Apply Promo Code</button>
                                    </h4>
                                </div>
                            </div>


                            <div class="row">

                                <div class="col-xs-4 col-sm-6 col-md-6"></div>
                                <div class="col-xs-8 col-sm-6 col-md-6">

                                </div>
                            </div>

                        </div>

                        <div class="col-md-12 text-center" style="margin-top: 40px">
                            <a href="./select-vehicle.php" class="btn btn-success btn_back submit-button btn-shadow ">
                                Go Back
                            </a>
                            <button type="button"
                                    class="btn btn-success btn-shadow"
                                    id="getQuoteButton">Get Quote</button>
                        </div>


                        <!--trip summary ends-->
                    </div>
                    <!--inner row ends-->


                    <br>


                </div>
                <!--inner-container ends-->
                <div class="col-xs-1 col-sm-2 col-md-2"></div>
            </div>
        </div>
    </div>
    </div>
    <!--3rd row ends-->

    <!--head-main container-fluid upper content ends-->
</div>
</div>
<!--riding child Modal -->
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Child Seat</h4>
            </div>
            <form id="childSeatFrom">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-8">
                            <label class="fntlbl">Car Seat Type</label>
                            <div id="typeChildSeat1" seq="Infant">Infant (rear facing)</div>
                        </div>
                        <div class="col-xs-4">
                            <label for="qntSeats1" class="fntlbl">How&nbsp;many?</label>
                            <select class="form-control" id="qntSeats1">
                                <option value="0" selected>0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                    </div>
                    <div id="childSeatDiv2" class="form-group ors-child-seat-row"
                         style=" margin-top:2%;margin-bottom:0px;">
                        <div class="row">
                            <div class="col-xs-8">
                                <div id="typeChildSeat2" seq="Toddler">Toddler (front facing)</div>
                            </div>
                            <div class="col-xs-4">
                                <select class="form-control" id="qntSeats2">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="childSeatDiv3" class="form-group ors-child-seat-row" style="margin-top:2%;">
                        <div class="row">
                            <div class="col-xs-8">
                                <div id="typeChildSeat3" seq="Booster">Booster (front facing)</div>
                            </div>
                            <div class="col-xs-4">
                                <select class="form-control" id="qntSeats3">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top:2%;">
                        <div class="col-xs-12 text-right">
                            <button type="button" class="btn btn-prim" id="updateChildSeat">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--riding child Modal ends -->
<div class="modal fade" id="specialPackageRequest" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Special Package Request</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="showSpecialPackageItem">
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-prim" id="updateSpecialPckBtn">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="goldWeddingPackage" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Special Package Item</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <ol id="specialPackageView">
                    </ol>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>

    var intlTelInputValid = 0;

    function validPhoneNumber(trigger) {
        $('.phoneValidation').html('');
        var isValidNumber = $("#passengerMobileNumber").intlTelInput("isValidNumber");
        if (!isValidNumber) {
            $('.phoneValidation').html('<span style="font-size: 12px;color: #ff4268;">Invalid Number. Please insert valid one.</span>');
            intlTelInputValid = 0;
        } else {
            intlTelInputValid = 1;
            var countrySet = $("#passengerMobileNumber").intlTelInput("getSelectedCountryData");
            $("#passengerMobileNumber").intlTelInput("destroy");
            $('#passengerMobileNumber').intlTelInput({
                utilsScript: 'build/js//utils.js',
                autoPlaceholder: true,
                initialCountry: countrySet.iso2,
                preferredCountries: ['us', 'fr', 'gb']
            });
        }

    }

    $(function () {
        $('#passengerMobileNumber').intlTelInput({
            utilsScript: 'build/js//utils.js',
            autoPlaceholder: true,
            autoFormat: true,
            formatOnDisplay: true,
            preferredCountries: ['us', 'fr', 'gb']
        });
    });
    $(document).ready(function () {


        var getChildValue = localStorage.getItem('isChildRide');

        if (getChildValue) {
            $('#riding_child_btn').show();
        } else {
            $('#riding_child_btn').hide();
        }


        $('#bckButton').on("click", function () {

            window.location.href = "select-vehicle.html";

        });

        $('#getQuoteButton').on("click", function () {


            var firstName = $('#passengerFirstName').val();
            if (firstName == '') {

                alert('Please fill first name');
                return false;

            }
            var lastName = $('#passengerLastName').val();
            if (lastName == '') {

                alert('Please fill last name');
                return false;

            }
            var passengerMobileNumber = $("#passengerMobileNumber").intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);
            if (passengerMobileNumber == '') {

                alert('Please fill mobile number');
                return false;

            }
            var passengerEmailName = $('#passengerEmailName').val();
            if (passengerEmailName == '') {

                alert('Please fill email.');
                return false;

            }
            if (intlTelInputValid == 0) {

                alert('Invalid Number. Please insert valid one.');
                return false;

            }
            var numberOfPassengerInput = $('#numberOfPassengerInput').val();
            var jurneyDetail = window.localStorage.getItem("rateSetInformation");
            // if(typeof(jurneyDetail)=="string")

            // {


            //     jurneyDetail=JSON.parse(jurneyDetail);
            // }


            var getLocalStorageValue = window.localStorage.getItem("limoanyWhereVerification");

            var bookingInfo = window.localStorage.getItem("bookingInfo");


            var jsonData = {
                "action": "getQuoteToBackOffice",
                "jurneyDetail": jurneyDetail,
                "firstName": firstName,
                "lastName": lastName,
                "passengerMobileNumber": $("#passengerMobileNumber").intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164),
                "passengerEmailName": passengerEmailName,
                "numberOfPassengerInput": numberOfPassengerInput,
                "bookingInfo": bookingInfo
            };
            if (typeof(getLocalStorageValue) == 'string') {
                getLocalStorageValue = JSON.parse(getLocalStorageValue);
            }
            var _ServerpathVehicle = _SERVICEPATHSERVICECLIENT;

            $.ajax({

                url: _ServerpathVehicle,

                type: 'POST',

                data: jsonData,

                success: function (response) {


                    var responseObj = response;
                    console.log(responseObj);
                    if (typeof(response) == "string") {

                        responseObj = JSON.parse(response);


                    }
                    if (responseObj.data.ResponseCode == 0) {
                        //alert("success");
                        localStorage.setItem('trip_confirmation', responseObj.data.TripInfo.TripConfirmationNumber);
                        localStorage.setItem('getQuete_exit', 'yes');
                        window.location.href = "thank_you.html";
                    }
                    else {

                        localStorage.removeItem('getQuete_exit');
                        alert(responseObj.data.ResponseText)
                    }


                }
            });


        });


        $('#riding_child_btn').on("click", function () {


            // $('#childSeatFrom').load();
            $('#typeChildSeat1').val('');
            $('#typeChildSeat2').val('');
            $('#typeChildSeat3').val('');


//            $('#qntSeats1').val(1);
//            $('#qntSeats2').val(1);
//            $('#qntSeats3').val(1);

//            $('#childSeatDiv2').hide();
//            $('#childSeatDiv3').hide();
            $('#myModal1').modal("show");
        });

        $('#updateChildSeat').on("click", function () {
            var getFirstSeat = $('#typeChildSeat1').attr("seq");
            var getSecondSeat = $('#typeChildSeat2').attr("seq");
            var getThirstSeat = $('#typeChildSeat3').attr("seq");
            var totalNumberOfSeat = 0;
            var concanateSeats = '';
            var getSeatFirstNumber = $('#qntSeats1').val();
            var getSeatSecondNumber = $('#qntSeats2').val();
            var getThirdSeatNumber = $('#qntSeats3').val();

            if (getSeatFirstNumber != '') {


                concanateSeats = getFirstSeat + "(" + getSeatFirstNumber + ")";
                totalNumberOfSeat += parseInt(getSeatFirstNumber);

            }
            if (getSeatSecondNumber != '') {

                concanateSeats += "," + getSecondSeat + "(" + getSeatSecondNumber + ")";
                totalNumberOfSeat += parseInt(getSeatSecondNumber);

            }
            if (getThirdSeatNumber != '') {

                concanateSeats += "," + getThirstSeat + "(" + getThirdSeatNumber + ")";


                totalNumberOfSeat += parseInt(getThirdSeatNumber);

            }


            if (totalNumberOfSeat != 0) {

                localStorage.setItem('total_child', totalNumberOfSeat);
                if (totalNumberOfSeat < 4) {


                    $('#myModal1').modal('hide');
                    $('#childValueStorhere').html(concanateSeats);
                    $('#riding_child_btn').html("Child's car seat selection");
                    paymentClass.getChildRateFunction(totalNumberOfSeat);

                }
                else {

                    alert("Total child seat must not exceed than 3 seat.");

                }

            }


        });


        $('#childSeatEdit').on("click", function () {
            // alert(12);
            $('#childSeatFrom')[0].reset();
            var getChildFieldValue = $('#childValueStorhere').html();
            // alert(getChildFieldValue);
            var getChildFieldValueArray = getChildFieldValue.split(',');
            $.each(getChildFieldValueArray, function (index, result) {
                var resultArray = result.split('(');
                resultArray[1] = resultArray[1].replace(')', '');
                $('#childSeatDiv' + (index + 1)).show();
                $('#typeChildSeat' + (index + 1)).val(resultArray[0]).show();
                $('#qntSeats' + (index + 1)).val(resultArray[1]).show();

            });
            // console.log(getChildFieldValueArray);


            $('#myModal1').modal('show');


        });


        $('#qntSeats1').change(function () {
            if ($('#qntSeats1').val() > 0) {
                $('#childSeatDiv2').show();
            }
        });

        $('#qntSeats2').change(function () {
            if ($('#qntSeats2').val() > 0) {
                $('#childSeatDiv3').show();
            }
        });


        $('input[type=checkbox][name=passenger]').change(function () {
            if ($(this).is(":checked")) {
                $('#airlinecode').hide();
            }
            else if ($(this).is(":not(:checked)")) {
                $('#airlinecode').show();
            }
            /* if (this.value == 'flightdetail') {
                 $('#airlinecode').hide();
             }
             else{
                 $('#airlinecode').show();
             }*/
        });


        $('#paymentMethod').change(function () {

            var option = $(this).find('option:selected').val();
            if (option == 2 || option == 3 || option == 4 || option == 5 || option == 6 || option == 7 || option == 8) {
                $('#payoption').show();
            }

            else {
                $('#payoption').hide();
            }
        });

        $('#addPackage').on('click', function () {
            $('#specialPackageRequest').modal("show");
        })
    });
</script>
<script src="PageJs/comman_all.js"></script>
<script src="PageJs/payment.js"></script>

</body>
</html>
